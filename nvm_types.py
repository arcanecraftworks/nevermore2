#===========================standard library imports==========================#
import sys
import calendar
#============================3rd party imports================================#
#================================local imports================================#
from cli_tools import *
#from cli_tools import prompt_loop, printDebug, menu, print_automatic, \
#yn_prompt_loop, prompt_loop_null, menu_exec, prompt_automatic, \
#printDictOfObject, printClass
from Selector import Selector, Selector_Simple
import database.utils as utils
from __builtin__ import NotImplementedError
from datetime import datetime
from NCalendar import MONTHS
from Nevermore import NCalendar
#from Nevermore.cli_tools import 
#==================================Constants==================================#
EQUAL_PRIORITY = True
JOKE_ERR = False
        
VIEW_LIMIT = 10
TESTING = False
COMM_ENABLED = False
NO_ROWS = 'No entries found for this filter.'
CAL_DISABLED = "NCalendar commands disabled. type 'menu' to access the"+\
               " adjustment menu"
TIME_ERR = 'Ben needs to fix this prompt, but something was formatted wrong,' +\
        'so try again.'
ACTUAL_TIME_ERR = 'Times have to be in either hh:mm:ss or hh:mm format.'
TIME_SUFFIX = '->'
DATE_LIST = [
             ['year'],
             ['month','year'],
             ['month','day','year'],
             ]

def makeCaption(s, colon=True):
    return sys.modules['entry'].makeCaption(s, colon=colon)
DECLARATIVE_META = "<class 'sqlalchemy.ext.declarative.api.DeclarativeMeta'>"
def typeMap(nTable):
    #assert False
    if str(nTable.__class__) == DECLARATIVE_META:
        nTable = utils.getNTable(class_=nTable)
    assert nTable.__class__ == utils.nvmTable
    #    printDebug(nTable.__class__)
    #    exit()
    #convert(nTable)
    
    c = Converter(nTable=nTable)
    
    res = dict()
    for coldict in c.promptlist:
        type_ = None
        main_key = None
        extra_data = dict()
        for key, val in coldict.iteritems():
            if key == 'type':
                type_ = val
            elif key == 'key':
                main_key = val
            else:
                extra_data[key] = val
        if len(extra_data) == 0:
            res[main_key] = type
        else:
            res[main_key] = coldict
    return res
class Name():
    def __init__(self,  nTable=None):
        self.session = utils.open_session()
        self.nTable = utils.nTables['Names']
    def prompt(self, prompt=None, caption=None, tab=0, **kw):
        
        if prompt is None and caption is not None:
            prompt = caption
        elif prompt is None and caption is None:
            prompt = 'Name: '
        
        return prompt_loop(prompt, '', self.verify, tab=tab)
    
    
        #return self.verify(s)
    def verify(self, name):
        ct = self.checkTable(name)
        if ct is None:
            return self.nTable.mClass(name=name)
        else:
            return ct
    def checkTable(self, name):
        q = self.session.query(self.nTable.mClass)
        return q.get(name)
class m2m(Selector):
    """class for selecting a list of multiple elements from a table 
    from across an M2M relationship.
    """
    def prompt(self, remote=None, prompt=None, new=False, tab=0, **kw):
        """Constructs a list of objects from the :param:remote class
        """
        self.tab = tab
        if prompt is None and 'caption' in kw:
            prompt = kw['caption']
        
        #printDebug(prompt, interrupt_override=True)
        printTabbedOverride(prompt, tab=tab)
        self.resetQuery(remote)
        self.nTable = remote
        choices = []
        while True:
            a = self.querySelect(msg=str(choices))
            
            if a.__class__ == str and a == 'Final':
                if choices == []:
                    return None
                else:
                    return choices
            elif a.__class__ == str and a == 'undo':
                try:
                    choices.pop()
                except IndexError:
                    printTabbedOverride('Nothing to undo')
            else:    
                choices.append(a)
        exit('m2m.prompt')
class string():
    ERR_MSG = 'field limit is {} characters, supplied string is {}'
    def __init__(self, limit=50, *args, **kw):
        self.limit = limit
    def prompt(self, prompt=None, limit=50, tab=0, **kw):
        assert prompt is not None
        
            
        
        try:
            s = prompt_loop_null(prompt, tab=tab)
        except:
            #TODO: aflkdklf
            printDebug(kw)
            exit()
        self.limit = limit
        return s
    def verify(self, s):
        if len(s) < self.limit:
            return s
        else:
            raise Exception(self.ERR_MSG.format(self.limit, len(s)))
class Integer():
    def __init__(self, *args, **kw):
        pass
    def prompt(self, prompt=None, limit=-1, tab=0, **kw):
        assert prompt is not None
        i = prompt_loop(prompt, '', None, tab=tab, input_type='int')
        if limit == -1 or i < limit:
            return i
class Text(Selector_Simple):
    pass
class Option(Selector_Simple):
    def prompt(self, prompt=None, tab=0, *arg, **kw):
        assert prompt is not None
        a = menu(kw['options'], title=prompt)
        #printDebug('"{}"'.format(a))
        if a == 'Other':
            return prompt_loop_null('specify->', tab=tab)
        else:
            return a
class Time(Selector_Simple):
    """Builds a time object from base components and checks
    for validity.
    """
    TAB = 1
    def prompt(self, prompt=None, orig=None, return_type=None, 
               echo=False, tab=0, *arg, **kw):
        assert prompt is not None
        #assert 'time' in kw
        del kw['type']
        self.tab = tab
        if not yn_prompt_loop('enter {}?'.format(orig)):
            return None
        if echo:
            print_automatic(prompt)
        
        kw2 = self.__buildkw__(**kw)
        self.res = dict()
        self.return_type = return_type
        
        
        order = ['time','date','epoch']
        
        for key in order:
            if kw2[key]:
                f = getattr(self, key)
                self.res[key] = f()
    
        self.__test__(**kw2)
        o = utils.nTables['Times'].mClass()
        o.date_time = self.__dtBuild__()
        o.epoch = self.res['epoch']
        
        return o
    def __test__(self, **kw2):
        type_class = self.return_type.__class__
        if type_class == utils.nTables['Times'].mClass.__class__:
            self.__dbObj__(**kw2)
        elif type_class == datetime().__class__:
            raise NotImplementedError
        elif type_class == datetime.time().__class__:
            raise NotImplementedError
        elif type_class == datetime.date().__class__:
            raise NotImplementedError
    def __dtBuild__(self):
        
        comb = dict()
        one = False
        two = False
        if 'time' in self.res and self.res['time'] is not None:
            for i, j in self.res['time'].iteritems():
                comb[i] = j
                one = True
        if 'date' in self.res and self.res['date'] is not None:
            for i, j in self.res['date'].iteritems():
                comb[i] = j
                two = True
        
        if one or two:
            return datetime(**comb)
        else:
            return None
    def time(self):
        if JOKE_ERR:
            err = TIME_ERR
        else:
            err = ACTUAL_TIME_ERR
        
        prompt = 'time{}'.format(TIME_SUFFIX)
        
        return prompt_loop(prompt, err, self.__time_regex__, tab=self.tab,
                           override=True, 
                           force_loop=True
                           )
    
    def __findKeys(self, options, len_args):
        order = None
        for i in options:
            if len(i) == len_args:
                return i
        return None
    def __time_regex__(self, a, *arg, **kw):
        orders = [['hour','minute','second'], ['hour','minute']]
        res = dict()
        args = a.split(':')
        len_a = len(args)
        if len_a < 2 or 3 < len_a:
            tryAgain()
        
        order = self.__findKeys(orders, len(args))
        
        i = 0
        for key in order:
            res[key] = int(args[i])
            i = i + 1
        
        return res
        
    def date(self):
        prompt = 'date{}'.format(TIME_SUFFIX)
        return prompt_loop(prompt, '', self.__date_2__, tab=self.tab)
    def __date_2__(self, el):
        arg0 = el.split(' ')
        if len(arg0) == 2 and arg0[1] == '-cal':
            self.__calLaunch__(arg0[0])
        return self.__regex__(el)
    def __calLaunch__(self, arg):
        
        m = self.__regex__(arg)
        
        self.year = m['year']
        self.month = m['month']
        
        while True:
            self.printMonth()
            CALL_ERR = "integer required, or type 'menu' for more options"
            a = prompt_loop('day->', CALL_ERR, self.daycheck)
            if a is not None:
                return a
    def printMonth(self):
        print(calendar.month(self.year, self.month))
    def daycheck(self, day=None, year=None, month=None):
        """Checks to make sure that the number of days that
        were typed in gives enough 
        """
        assert year is not None
        OVER_ERR = '{} does not have {} days in it.'
        #printDebug('getday={}'.format(day))
        try:
            n = self.findMax(year=year, month=month)
            text_month = MONTHS[month]
            #printDebug('{}={}'.format(text_month, n))
            day = int(day)
            if day <= n:
                #printDebug('Kirito')
                return day
            else:
                #printDebug('Asuna')
                s = OVER_ERR.format(text_month, day)
                tryAgain(s)
                #print_automatic(s, override=True)
                #self.calmenu()
                #return None
        except ValueError:
            printDebug('Silica')
            self.calcomm(day)
            return None
    def ordinal(self):
        return [
                'previous year',
                'previous month',
                'set month',
                'set year',
                'next month',
                'next year'
                ]
    def im(self):
        return {
                'previous year':self.__prevyear__,
                'previous month':self.__prevmonth__,
                'set month':self.__setmonth__,
                'set year':self.__setyear__,
                'next month':self.__nextmonth__,
                'next year':self.__nextyear__,
                }
    def __prevyear__(self):
        self.year = self.year - 1
    def __prevmonth__(self):
        month = self.month - 1
        if month > 12:
            self.month = month + 12
            self.year = self.year - 1
        else:
            self.month = month
    def __setmonth__(self):
        self.month = prompt_loop('month->','', self.__month2__)     
    def __month2__(self, i):
        
        #int version
        try:
            i = int(i)
            if 1 <= i and i <= 12:
                self.month = i
                return
            else:
                printDebug('There are only twelve months in a year. Try again.')
                assert False
        except ValueError:
            pass
            
        for d, di in MONTHS.iteritems():
            if i.capitalize() == di:
                #self.month = d
                return d
        printDebug('Month not recognized.')
        assert False
    def __setyear__(self):
        self.year = prompt_loop_null('year->', input_type=int)
    def __nextmonth__(self):    
        month = self.month + 1
        if month > 12:
            self.month = month - 12
            self.year = self.year + 1
        else:
            self.month = month
    def __nextyear__(self):
        self.year = self.year + 1
    def calcomm(self, n):
        if n == 'menu':
            self.calmenu()
        else:
            #TODO: finish menu
            if COMM_ENABLED:
                raise NotImplementedError
            else:
                printDebug(CAL_DISABLED, override=True)
                return None
    def calmenu(self):
        menu_exec(self.im(), ordinal=self.ordinal())
    def findMax(self, month=None, year=None):
        if month is None and 'month' in self.__dict__:
            month = self.month
        if year is None and 'year' in self.__dict__:
            year = self.month
        
        assert month is not None
        assert year is not None
        
        m = NCalendar.NCalendarFrame.__makeCal__(year=year, month=month)
        max_ = 0
        for i in m:
            for j in i:
                if j[0] > max_:
                    max_ = j[0]
        return max_
    def __regex__(self, el):
        REGEX = '/-'
        for i in REGEX:
            #printDebug('i={}'.format(i))
            arg1 = el.split(i)
            d = self.makeDay(arg1)
            if d is not None:
                #printDebug('d={}'.format(d))
                return d
            else:
                tryAgain(msg='Date must be in dd/mm/yyyy format.')
    def makeDay(self, arg1):
        #printDebug(arg1)
        #DAY_LIMITS = ['31','28','31','30','31','30',''
        
        i = len(arg1)-1
        
        list_ = DATE_LIST[i]
        res = dict()
        try: 
            for i in range(len(list_)):
                key = list_[i]
                arg = int(arg1[i])
                res[key] = int(arg)
            a = self.daycheck(**res)
            return res
        except ValueError:
            return None
    def epoch(self):
        nTable = utils.nTables['Times']
        select = Fk(nTable)
        remote = utils.nTables['Epochs'].mClass
        return select.prompt(remote=remote, prompt='epoch{}'.format(TIME_SUFFIX))
    def __dbObj__(self, time=True, date=True, epoch=True):
        pass
    def __buildkw__(self, time=True, date=True, epoch=True, **kw):
        #adds defaults into the provided dictionary
        return {'time':time,'date':date,'epoch':epoch}     
class Fk(Selector):
    def prompt(self, remote=None, prompt=None, tab=0, **kw):
        self.tab = tab
        if prompt is None and 'caption' in kw:
            prompt = kw['caption']
        
        echo = False
        
        select = prompt_loop_null(prompt, tab=tab)
        if select == '':
            return None
        b = False
        self.remote = remote
        args = self.prompt1(prompt, remote, pre_result=select, tab=tab)
    
        
        if args[0] == []:
            NULL_RETURN_ASK = 'Your selection did not return results. Create new?'
            b = yn_prompt_loop(NULL_RETURN_ASK)
        else:
            a = args[0]
            if a.__class__ == list:
                a = a[0]
            return a
        if b:
            sess = utils.open_session()
            o = self.newlaunch(name=select)
            sess.add(o)
            sess.commit()
            sess.close()
            return o
        else:
            return self.single_select(*args, echo=echo)
    def single_select(self, a, e_chk, echo=False):
        if a is None:
            return a
        
        len_a = len(a)
        len_e = len(e_chk)
        if len_a == 0:
            if echo:
                printDebug('Kirito')
            return self.querySelect()
        if len_a == 1:
            if echo:
                printDebug('Asuna')
            return a[0]
        elif len_e == 1 and EQUAL_PRIORITY:
            if echo:
                printDebug('Suguha')
            return e_chk[0] #self.__assoc(i, e_chk)
        elif len_e > 1:
            raise NotImplementedError
        elif len_a > 1:
            if echo:
                printDebug('Klein')
            return self.__menu_choice(a)
        else:
            printDebug('len_a={}'.format(len_a))
            printDebug('len_e={}'.format(len_e))
    def first(self, *arg, **kw):
        del kw['tab']
        return self.q.first(*arg, **kw)
    def menu_dicts(self, i=None):
        self.printq_options = {
                               'filter':self.__filter__,
                               'select first':self.first,
                               'menu select':self.menu_select1,
                               'create new':self.newlaunch,
                               'select None':self.null_select,
                               #'text select':self.text_select
                               }
        self.no_row_options = {
                               'reset one':self.__reset_one__,
                               'reset all':self.resetQuery,
                               'create new':self.newlaunch
                               }
    #def text_select(self):
    #    raise NotImplementedError

def headlessEntry(*args, **kw):
    return sys.modules['headless'].HeadlessEntry(*args, **kw)
class NumFk(Fk):
    def prompt(self, prompt=None, remote=None, **kw):
        print_automatic(prompt)
        if remote is None:
            printDebug('no remote supplied to NumFK prompt')
        
        a = utils.process(remote)
        return self.menu_select1(list=a)
class Converter():
    def __init__(self, nTable=None, mClass=None):
        self.promptlist = []    
        
        if nTable is not None:
            nTable.mClass.gui(self)
        if mClass is not None:
            mClass.gui(self)
    def nameField(self, key, caption=None):
        a = {'key':key, 'type':'Name'}
        if caption is not None:
            a['caption'] = caption
        self.promptlist.append(a)
    def optionField(self, key, options):
        prompt = makeCaption(key)
        
        #printDebug(key)
        #assert False
        a = {'key':key,'type':'Option','prompt':prompt,'options':options}
        self.promptlist.append(a)
    def textField(self, key):
        a = {'key':key,'type':'Text'}
        self.promptlist.append(a)
    def fkField(self, key, remote, caption=None, numerical=False):
        if numerical:
            type_ = 'NumFk'
        else:
            type_ = 'Fk'
        a = {'key':key,'type':type_,'remote':remote}
        if caption is not None:
            a['prompt']=caption
        self.promptlist.append(a)
    def timeField(self, key, return_type, **kw):
        a = {'key':key, 'type':'Time','orig':key,'return_type':return_type}
        a = self.loadDict(key, a, **kw)
        self.promptlist.append(a)
    def m2mField(self, key, remote):
        a = {'key':key,'type':'m2m','remote':remote}
        self.promptlist.append(a)
    def intField(self, key, caption=None, **kw):
        if caption is None:
            caption = makeCaption(key)
        
        kwarg = {'key':key,'type':'Integer','prompt':caption}
        a = self.loadDict(key, kwarg, **kw)
        self.promptlist.append(a)
    def simpleField(self, key, caption=None, **kw):
        kwarg = {'key':key,'type':'string'}
        if caption is not None:
            kwarg['prompt'] = makeCaption(caption)
        a = self.loadDict(key, kwarg, **kw)
        self.promptlist.append(a)
    def checkField(self, key, caption=None, **kw):
        kwarg = {'key':key,'type':'check','prompt':caption}
        a = self.loadDict(key, kwarg, **kw)
    def loadDict(self, key, kwarg, **kw):
        for i in kw:
            kwarg[i] = kw[i]
        return kwarg
    def get(self):
        
        #assert False
        #for i, di in self.kw.iteritems():
        #    printDebug('{}: {}'.format(i, di))
        #exit()
        return self.promptlist
def convert(nTable):
    mClass = nTable.mClass
    c = Converter()
    d = mClass.gui(c)
    return c.get()
    
    
        
        
        