import tkMessageBox as box

def onError(msg):
    return box.showerror("Error", msg)
def onWarn(msg):
    return box.showwarning("Warning", msg)
def onQuest(msg, title='Question'):
    return box.askquestion(title, msg)
def onInfo(msg, title="Information"):
    #assert False
    return box.showinfo(title, msg)
def onExit(parent):
    return parent.quit()