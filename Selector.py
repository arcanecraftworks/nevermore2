#===========================standard library imports==========================#
import sys
#============================3rd party imports================================#
#================================local imports================================#
from cli_tools import *
import database

#import database.cli_utils as cli_utils
#import database.utils as utils
import nvm_types
#==================================Constants==================================#
EQUAL_PRIORITY = True
VIEW_LIMIT = -1
TESTING = False
NO_ROWS = 'No entries found for this filter.'
DECLARATIVE_META = "<class 'sqlalchemy.ext.declarative.api.DeclarativeMeta'>"

class Selector_Simple():
    def __init__(self, *args, **kw):
        pass
    def prompt(self, prompt=None, tab=0, **kw):
        
        assert prompt is not None
        
        return prompt_loop_null(prompt, tab=tab)
        #return str(raw_input('{}: '.format(prompt)))
    def verify(self, a):
        return a



class Selector():
    ordinal_menu = None
    def menu_dicts(self):
        self.printq_options = {
                               'filter':self.__filter__,
                               'select first':self.first,
                               'menu select':self.menu_select1,
                               'create new':self.newlaunch,
                               'select None':self.null_select,
                               'finalize':self.finalize,
                               'undo':self.undo,
                               }
        self.no_row_options = {
                               'reset last filter':self.__reset_one__,
                               'reset all filters':self.resetQuery,
                               'create new':self.newlaunch
                               }
    
    def menu_select1(self, tab=0, list_=None):
        if list_ is None:
            list_ = self.q.all()
        return self.menu_select(list_, tab=tab)
    def finalize(self, *arg, **kw):
        return 'Final'
    def undo(self, *arg, **kw):
        return 'undo'
    def first(self, tab=None, **kw):
        return self.q.first(**kw)
    def __init__(self, host_nTable, query=None, **kw):
        self.q=query
        #assert host_nTable == utils.nvmTable
        self.host_nTable = host_nTable
        self.nTable = host_nTable
        self.checkNTable()
        self.filters = []
        self.tab=0
    def newlaunch(self, **kw):
        remote = database.utils.nTables[self.remote.__tablename__]
        #TODO: alkdjfkld
        kw['tab'] = self.tab + 1
        
        return sys.modules['headless'].HeadlessEntry(remote, **kw).write()
    def prompt(self, remote=None, prompt=None, new=False, tab=0, **kw):
        self.tab = tab
        if prompt is None and 'caption' in kw:
            prompt = kw['caption']
        self.remote = remote
        if new:
            return self.prompt2() 
        return self.prompt2(*self.prompt1(prompt, remote, tab=tab))
    def prompt1(self, prompt, remote, pre_result=None, tab=0):
        """Asks the operator for a string and compares that
        to the database.
        """
        assert prompt is not None
        if pre_result == None:
            self.select = prompt_loop_null(prompt, tab=tab)
        else:
            self.select = pre_result
            
        if self.select is None or self.select == 'None' or self.select == '':
            return None, []
        if self.select.capitalize() == 'New':
            return self.newlaunch(), []
        self.nTable = database.utils.getNTable(class_=remote)
        #self.resetQuery(remote=remote)
        a = database.utils.process(remote, query=self.q, for_each=self.__find__, 
                          allow_none=False)
        
        e_chk = self.__equalcheck(a)
        return a, e_chk
    def prompt2(self, a, e_chk):
        if a is None:
            return a
        len_a = len(a)
        len_e = len(e_chk)
        if len_a == 0:
            #find failed
            self.querySelect()
        elif len_a == 1:
            return a[0]
        elif len_e == 1 and EQUAL_PRIORITY:
            return e_chk #self.__assoc(i, e_chk)
        elif len_e > 1:
            raise NotImplementedError
        elif len_a > 1:
            return [self.__menu_choice(a)]
        else:
            printDebug('len_a={}'.format(len_a))
            printDebug('len_e={}'.format(len_e))
            raise NotImplementedError
    def resetQuery(self, remote=None, **kw):
        if remote is not None:
            self.remote = remote
        session = database.utils.open_session()
        self.q = session.query(self.remote)
        self.menu_dicts()
    def __menu_choice(self, a):
        print 'Found several entries for the given criteria.'+\
        ' Did you mean one of these?'
        FULL_TABLE = 'full table'
        BLANK = 'leave blank'
        
        b = self.menu_select(a, options=[FULL_TABLE, BLANK])
        if b == FULL_TABLE:
            return self.querySelect()
        elif b == BLANK:
            return []
        
        exit('__menu_choice')
    def menu_select(self, a, prompt='choice->>', options=[], tab=0):
        return menu(a, prompt, options=options, exit_=False, default_value=2,
                    tab=tab)
    def verify(self, a):
        #TODO stub
        return None
    def __equalcheck(self, a):
        b = []
        for i in a:
            if self.select.lower() == str(i).lower():
                b.append(i)
        #len_b = len(b)
        return b
    def querySelect(self, msg=None):
        r = None
        override = False
        while r is None:
            r = self.selectionLoop(override, msg=msg)
            if r == 'exit':
                override = True
        return r
    def selectionLoop(self, override=False, msg=None):
        rows = self.printQuery(override=override)   
        if rows == []:
            #printDebug('tab={}'.format(self.tab))
            printTabbedOverride(NO_ROWS, self.tab)
        if msg is not None:
            printTabbedOverride(msg, self.tab)
            
        temp = self.tab
        self.tab = self.tab + 1
        s = menu_exec(self.printq_options, ordinal=self.ordinal_menu, tab=self.tab)
        self.tab = temp
        return s
    def __reset_one__(self):
        self.filters.pop()
        self.resetQuery()
        for i in self.filters:
            self.q = database.cli_utils.filter_ver(None, pair=i)[0]
    def checkNTable(self):
        stri = str(self.nTable.__class__)
        
        if stri == DECLARATIVE_META:
            self.nTable = database.utils.getNTable(class_=self.nTable)
        
    def printQuery(self, override=True, q=None):
        if q  is None:
            q = self.q
            hide = override or (not AUTO_ECHO)
        
        #if 'nTable' in self.__dict__:
        #    nTable = self.nTable
        #else:
        #    nTable = self.host_nTable
        self.checkNTable()
            
        return database.cli_utils.printNTable(self.nTable, query=q, 
                                     limit=VIEW_LIMIT, hide=hide,
                                     tab=self.tab+1)
    def __filter__(self, tab):
        FILTER_PROMPT = 'filter->'
        FILTER_ERR = 'filter command not recognized. '+\
             'Please enter a command in the form "filter-> column=value"'
        #FILTER_DEF = 'name=Alfeim'
        if str(self.nTable) == DECLARATIVE_META:
            self.nTable = database.utils.getNTable(class_=self.nTable)
        
        #printDebug(self.nTable.__class__)
        
        typemap = nvm_types.typeMap(self.nTable)
        
        try:
            self.q, s = prompt_loop(FILTER_PROMPT, FILTER_ERR, database.cli_utils.filter_ver, 
                                 query=self.q,
                                 typemap=typemap, 
                                 nTable=self.nTable,
                                 tab=tab)
            #printDebug('Kirito')
            
            self.filters.append(s)
        except AttributeError:
            raise
    def null_select(self, *arg, **kw):
        return None
    def __find__(self, el):
        
        if self.select.lower() in str(el).lower():
            return el


    