#===========================standard library imports==========================#
import os
#from tkFileDialog import askdirectory
from datetime import date, datetime
#============================3rd party imports================================#
from sqlalchemy.exc import IntegrityError
#================================local imports================================#
import database
#import database.utils as utils
#import database.bootstrap as bootstrap
#from database.bootstrap import POLY_ON
from cli_tools import printDebug, yn_prompt_loop, menu
from nvm_types import Name
from MessageBox import onError
#==================================Constants==================================#
FILEROOT = os.path.dirname(__file__)
CHAR_REPLACE = {',':'%304','\n':'%582'}
UNI_REPLACE = {u'\u2014':'-',u'\u2019':"'"}
BACKUP_FILETYPE = 'csv'
SAVE_DIR = 'save'

OPEN_ERR = 'Could not open file {}'
DATE_CODE = '%DATE='
CLOUD_FILE = 'cloud_schema.txt'
FILENAME = 'filename: {}'
ACCESS_DENIED = 'Access denied to file {}.{}. Make sure that the files'+ \
                ' are not open in another program.'
KEY_ERR = 'Key error reading config on file {}!'
GUEST = 'guest'
CONFIG_FILETYPE = 'txt'
#=============================================================================#
def readFile(filename):
        try:
            findLevelNo = open(filename, "r")
            text = findLevelNo.read()
            return text
        except:
            onError(OPEN_ERR.format(filename))
def ensure_dir(f):
    if not os.path.exists(f):
        try:
            os.makedirs(f)
        except WindowsError:
            raise
def read_config(fileroot=None):
    if fileroot is None:
        fileroot = SAVE_DIR
    
    global dbms_index
    a = []
    
    ensure_dir(fileroot)
    
    for s in os.listdir(fileroot):
        filename = os.path.join(SAVE_DIR, s)
        if os.path.isfile(filename):
            a.append(configFile(filename))
    return a
def configFile(filename):
    defDict = dict()
    with open(filename, 'r') as findLevelNo:
        dbconfig = database.utils.DBConfig(fixed=True)
        for i in findLevelNo:
            try:
                sArray = i.split(': ')
                key = sArray[0]
                value = sArray[1].rstrip('\n').strip(' ')
                defDict[key] = value
            except IndexError:
                printDebug(FILENAME.format(filename), override=True)
                printDebug(i, override=True)
                raise
        
        if 'password' in defDict:
            pswd = defDict['password']
        
        if 'filename' in defDict:
            return defDict
        else:
            pswd = ''  
        
        try:
            dbconfig = database.utils.DBConfig(dicti=defDict, fixed=True, password=pswd)
            return defDict
        except KeyError:
            printDebug(KEY_ERR.format(filename), override=True)
        return dbconfig
def readCloudSchema():
    coll=[]
    with open(CLOUD_FILE, 'r') as findLevelNo:
        for row in findLevelNo:
            coll.append(row.rstrip('\n'))
def appendCloudSchema(s):
    with open(CLOUD_FILE, 'a') as findLevelNo:
        findLevelNo.write(s)
def write_config(dbconfig, auto=False, overwrite=True):
    if auto:
        s = 'autoconfig.conf'
    else:
        s = '{}.{}'.format(dbconfig.alias, CONFIG_FILETYPE)
    
    filename = os.path.join(SAVE_DIR, s)
    a = dbconfig.dbms
    #printDebug('dbms={} {}'.format(a.__class__, a))
    
    if overwrite or (not os.path.isfile(filename)):
        with open(filename, 'w') as file_:
            file_.write(repr(dbconfig))
def modeAdd_qualify(t):
    if t is None:
        return False
    if t.__class__ == str or t.__class__ == u''.__class__:
        raise TypeError('modeAdd tried to add a string.')
    if 'mk_del' in t.__dict__ and t.mk_del == True:
        return False
def makeName(t):
    uni_class = u''.__class__
    str_class = ''.__class__
    if t.__class__ == uni_class or t.__class__ == str_class:
        t.name = database.utils.nTables['Names'].mClass(name=t.name)   
def modeOne(session, nTable, t):
    prev = database.utils.findObj(nTable, t)
        
    if prev is not None:
        try:
            database.utils.merge(destination=prev, origin=t)
        except NameError:
            raise
    else:
        try:
            printDebug('Silica')
            makeName(t)
            
            #for i, j in t.__dict__.iteritems():
            #    printDebug('{}: {} {}'.format(i, j.__class__, j))
            #exit()
                
            session.add(t)
        except:
            printDebug('error in {} "{}"'.format(nTable, t))
def modeAdd(nTable, t, mode):
    #printDebug('mode={}'.format(mode))
    session = database.utils.open_session()
    if not modeAdd_qualify(t):
        return
    
    if mode == 0:
        #flush mode - database has already been cleared out.
        session.add(t)
    elif mode == 1:
        modeOne(session, nTable, t)
    elif mode == 2:
        #ignore mode
        prev = database.utils.findObj(nTable, t)
        
        if prev is None:
            session.add(t)
    elif mode == 3:
        #manual mode
        prev = database.utils.findObj(nTable, t)
        if prev is None:
            session.add(t)
        else:
            s = '{} {} already exists with primary key {}. Overwrite? [Y/N]'.format(prev.__class__, str(prev), t.id)
            if yn_prompt_loop(s):
                prev = t
def readInCSV(fileroot, nTable, mode=1, override=False):
    printDebug('reading in {}'.format(fileroot, interrupt_override=True))
    
    
    
    assert nTable.name is not None
    if nTable.name == 'nvMaps':
        return
    
    
    kw = dict()
    kw['mode'] = override
    kw['mode'] = mode
    #printDebug('ReadInCSV')
    
    session = database.utils.open_session()
    try:
        pt = database.utils.polyType(nTable)
    except AttributeError:
        raise
        #printDebug('AttributeError in {}'.format(nTable))
        #pt = None
    with open(getBackupFilePath(fileroot, nTable), 'r') as findLevelNo:
        if pt is None:
            readList(findLevelNo, nTable, session, **kw)
        else:
            kw['buffered'] = True
            list = readList(findLevelNo, nTable, session, **kw)
            
            printDebug(nTable)
            return BufferObject(nTable, pt, list)
class BufferObject():
    def __init__(self, nTable, pt, list):
        self.nTable = nTable
        self.pt = pt
        self.list = list
    def __str__(self):
        return str(self.nTable)
def BufferAdd(list):
    host = None
    guests = dict()
    for el in list:
        if GUEST in el.pt:
            info = el.pt[GUEST]
            el.nTable.name
            
            guests[el.nTable.name] = el
        if 'host' in el.pt:
            host = el
        session = database.utils.open_session()
    
    printDebug('guests={}'.format(len(guests)))
        
    for row in host.list:
        disp0 = host.nTable.mClass.__mapper_args__[database.bootstrap.POLY_ON]
        polyTable0 = str(getattr(row, disp0.name))
        
        polyClass = None
        for i in guests:
            #exit(i.__class__)
            a = guests[i]
            #printDebug(a)
            j = a.pt['ident']
            if polyTable0 == j:
                polyClass = str(a.nTable)    
        
        #polyClass = str(db.nTables[polyTable0])
        printDebug('{}: {}'.format(row.id, polyTable0))
        #printDebug('polyclass={}'.format(polyClass))
        if polyTable0 == 'None':
            #printDebug('Kirito')
            t = getRow(row.id, host)
            exec('t.{} = None'.format(disp0.name))
        else:
            t = getRow(row.id, guests[polyClass])
            u = getRow(row.id, host)
            database.utils.merge(destination=t, origin=u)
        assert t is not None
        session.add(t)
        session.commit()
        #printDebug('===========================')
def getRow(id_, buffObj):
    
    list_ = buffObj.list
    for i in list_:
        if i.id == id:
            return i
def readList(findLevelNo, nTable, session, mode=1, override=False, buffered=False):
    #printDebug('readList')
    headers = None
    list_ = []
    for line in findLevelNo:
        if headers is None:
            headers = line
        else:
            t = None
            try:
                t = readRow(nTable, headers, line.rstrip('\n'))
                list_.append(t)
            except KeyError:
                raise
            if not override and not buffered:
                modeAdd(nTable, t, mode)
            elif t is not None and not buffered:
                session.add(t)
        
        session.commit()
        
            
    len_list = len(list_)
    if len_list != 0:
        printDebug('{} - {}'.format(nTable, len_list))
    
    return list
def splitHeader(headers):
    
    columns = headers.strip('\n').split(',')
    col2 = []
    #printDebug(columns)
    for i in columns:
        col2.append(i.strip(' '))
    #printDebug(col2)
    return col2
def p2(el):
    printDebug('{}={}'.format(el.id, el.name))
def langCheck(Name):
    sess = database.utils.open_session()
    languages = database.utils.getClass('Languages')
    for i in sess.query(languages).filter_by(name=Name.name):
        return i
def cultureIDMap(kw):
    CIM_KEY = 'culture_id'
    NEW_KEY = 'language'
    ID_KEY = 'language_id'
    a = ['','Human','Dwarf','Elf','Gnome','Half-Elf','Half-Orc','Halfling']
    b = ['','Common','Dwarvish','Elvish','Gnome','Half-Elf','Orcish','Halfling']
    
    
    
    
    Language = database.utils.nTables['Languages'].mClass
    sess = database.utils.open_session
    nco = Name()
    cid = int(kw[CIM_KEY])
    del kw[CIM_KEY]
    
    if cid == 8:
        kw[NEW_KEY] = None
    else:
        #n = b[cid]
        n = nco.verify(b[cid])
        L = langCheck(n)
        if L is None:
            kw[NEW_KEY] = Language(name=n)
        else:
            kw[NEW_KEY] = L
            
        #kw[NEW_KEY] = Language(name=n) 
        
    return kw
def readRow(nTable, headers, line):
    #printDebug('ReadRow')
    elements = line.split(',')
    columns = splitHeader(headers)
    
    if len(elements) != len(columns):
        for i in range(len(columns) - len(elements)):
            elements.append(' ')
        
        
        if len(elements) !=len(columns):
            print 'mismatch!'
            print columns
            print elements
            exit()
    i = 0
    
    kw = dict()
    for e in elements:
        findLevelNo = e.rstrip('\n')
        #if findLevelNo == '0001-12-25':
        #    exit(findLevelNo.__class__)
        g = parse(findLevelNo)
        #if g == '0001-12-25':
        #    exit(g.__class__)
        if g is None:
            g = scrub(findLevelNo, comp=UNI_REPLACE)
            g = unscrub(g)
            
        
        col = stripDot(columns[i], 1)
        if col == 'name':
            kw[col] = database.utils.nTables['Names'].mClass(name=g)
        if col != 'mk_del':
            kw[col] = g
        
        i = i + 1
    
    exp = nTable.mClass
    
    if 'culture_id' in kw:
        kw = cultureIDMap(kw)
    
    try:
        e = exp(**kw)
    except:
        raise
    #printDebug('ReadRow End')
    #assert e.__class__ =
    return e
    
    
    #except TypeError:
    #    printDebug('Type Error on table {} - look to make sure that your input tables have the same column names as your db objects.'.format(tablename))
    #    for col in nTable.sTable.c:
    #        printDebug(col)
    #    printDebug('({}'.format(stri.rstrip(', ')))
    #printDebug(exp)
def scrub(s, comp=CHAR_REPLACE):
    if s is None:
        return s
    for i in comp:
        j = comp[i]
        s = s.replace(i, j)
    return s
def unscrub(s, comp=CHAR_REPLACE):
    if s is None:
        return s
    for i in comp:
        j = comp[i]
        s = s.replace(j, i)
    return s
def parse(s):
    
    #a = s.split(u'\u2014')
    a = s.split('-')
    z = None
    if len(a) == 2 and a[0] == '':
        return int(a[1]) * -1
    if len(a) == 3:
        try:
            year = int(a[0])
            month = int(a[1])
            
            b = a[2].split(' ')
            day = int(b[0])
            
            if len(b) > 1:
                t = b[1].split(':')
                hour = int(t[0])
                minute = int(t[1])
                second = int(t[2])
                z = datetime(year, month, day, hour, minute, second)
            else:
                z = date(year, month, day)
        except ValueError:
            pass
        except:
            raise
        return z
    if z is None:
        b = booleanScrub(s)
        if b is not None:
            return b
    return None
def booleanScrub(s):    
    ind = database.utils.int_boolean
    if s == 'False' or s == 'FALSE':
        if ind:
            return 0
        else:
            return False
    elif s == 'True' or s == 'TRUE':
        if ind:
            return 1
        else:
            return True
    return None
def getBackupFilePath(fileroot, tablename):
    return os.path.join(fileroot, '{}.{}'.format(tablename, BACKUP_FILETYPE))
def commaHeaders(coll):
    stri = ''
    for i in coll:
        stri += '{}, '.format(i.name)
    stri = stri.rstrip(', ')
    return stri
def commaDelimit(classObj, headers):
    stri = ''
    for el in headers:
        at = getattr(classObj, el.name)
        try:
            ins = str(at)
        except UnicodeEncodeError:
            ins = at
        cleaned = scrub(ins)
        stri += cleaned + ',' 
    return stri.rstrip(',')
def date_encode(d):
    return DATE_CODE + format(d)
def date_decode(d):
    d = d.lstrip(DATE_CODE)
    exit(d)
def backup():
    #fileRoot = os.path.join(SAVE_DIR, utils.dbconfig.alias)
    
    #fileRoot = askdirectory()
    fileRoot = os.path.join(SAVE_DIR,'kohlemeier')
    
    for t in database.utils.nTables:
        nTable = database.utils.nTables[t]
        try:
            writeOutCSV(fileRoot, nTable)
        except IOError:
            exit(ACCESS_DENIED.format(nTable.sTable.name, BACKUP_FILETYPE))
def restore():
    fileRoot = os.path.join(SAVE_DIR,'kohlemeier')
    buffers = []
    for t in database.utils.get_tables():
        restoreOne(fileRoot, t, buffers)
    BufferAdd(buffers)
    print 'Restore complete!'

def getStaticDirectory(mod_name):
    a = database.utils.nTables['nvMaps']
    for i in database.utils.process(a.mClass):
        if i.name == mod_name:
            return i.static_load_directory
def restoreModule(mod_name):
    if mod_name.__class__ == str:
        dir_ = getStaticDirectory(mod_name)
    else:
        dir_ = mod_name.static_load_directory
    
    fileDir = os.path.join(FILEROOT, 'database')
    fileroot = os.path.join(fileDir, dir_)
    buffers = []
    for file in os.listdir(fileroot):
        restoreOne(fileroot, file, buffers)
        
def restoreOne(fileRoot, t, buffers):
    printDebug('RestoreOne', interrupt_override=True)
    if t == str('nvMaps'):
        return
    
    try:
        nTable = database.utils.nTables[t.name]
    except AttributeError:
        n = t.split('.')[0]
        nTable = database.utils.nTables[n]
        #printDebug(t)
        #exit()
        
    try:
        if nTable.sTable is None:
            nTable.setTable(database.utils.getTableObj(t))
    except AssertionError:
        printDebug('nTable {} not properly initialized'.format(t))
    try:
        b = readInCSV(fileRoot, nTable)
        if b is not None:
            buffers.append(b)
    except IOError:
        raise
def restore_static(fileroot=SAVE_DIR, override=True):
    printDebug('futils.restore_static')
    session = database.utils.open_session()
    for mod in session.query(database.bootstrap.nvMaps):
        path = mod.static_load_directory
        if path is not None and path != '':
            fullpath = os.path.join(FILEROOT, 'database', path)
            for findLevelNo in os.listdir(fullpath):
                tablename = findLevelNo.split('.')[0]
                try:
                    readInCSV(fullpath, nTable=tablename, override=True)
                except IOError:
                    printDebug('unable to read from {}'.format(tablename), override=True)
                except IntegrityError:
                    if not override:
                        printDebug('integrity error on {}'.format(tablename), override=True)
def restore_config(directory=SAVE_DIR, mode=None):
    assert False
    #TODO: fix method
    global dbconfig
    fileroot = '{0}{1}'.format(directory, dbconfig.alias)
    restore_choices = 'flush mode (drop database and upload all data fresh from backup)','Update Mode (overwrites existing primary keys)','Ignore Mode (ignores existing primary keys and uploads new ones.','Manual Mode (allows you to specify action for each repeat row)'
    if mode is None:
        mode = menu(restore_choices)
    #if mode == 0:
    #    utils.pop2()
    
    for t in database.utils.nTables:
        nTable = database.utils.nTables[t]
        try:
            readInCSV(fileroot, nTable, mode)
        except IOError:
            print 'Error reading in {}. Skipping for now.'.format(nTable.name)
def writeOutCSV(fileroot, nTable):
    #rows = utils.rowStar(nTable, printTable=False)
    
    if not os.path.exists(fileroot):
        os.makedirs(fileroot)
    try:
        tablename = nTable.sTable.name
    except:
        exit(nTable)
    print tablename + '...'
    tableObj = nTable.sTable
    filename = os.path.join(fileroot, '{}.{}'.format(tablename, BACKUP_FILETYPE))
    with open(filename, 'w') as file_:
        headers = commaHeaders(tableObj.columns)
        file_.write(headers + '\n')
        database.utils.process(nTable, for_each=__for_each_row__, 
                      file_=file_, tableObj=tableObj)
        
        
        #for el in :
        
        
        
        #    row = commaDelimit(el, tableObj.columns)
        #    file_.write(row +'\n')
        print 'Done.'
def __for_each_row__(row, file_=None, tableObj=None):
    assert file_ is not None and tableObj is not None
    file_.write(commaDelimit(row, tableObj.columns))
def stripDot(s, i):
    try:
        return s.split('.')[i]
        #return t
    except IndexError:
        return s
def menu_print(i, el):
    print('[' + str(i) + '] ' + str(el))   
def print_array(a):
    for i in range(len(a)):
        menu_print(i, a[i])