#===========================standard library imports==========================#
from Tkinter import Frame, Label, Entry, StringVar, Tk, Toplevel
from Tkconstants import LEFT, END, E, GROOVE
from ttk import Button, OptionMenu

from datetime import datetime
#============================3rd party imports================================#
#================================local imports================================#
import database.utils as utils
from Nevermore import NCalendar
#from entry import fkSelect
from cli_tools import *
from _csv import list_dialects
DEF_WIDTH = 2
#==================================Constants==================================#




def makeOptionMenu(parent, text=None, *choices, **kw):
    command = kw.pop('command', True)
    return_obj = kw.pop('return_obj', False)
    
    
    Label(parent, text='Color by ').grid(row=0, column=0)
    v = StringVar(parent)
    om = OptionMenu(parent, v, *choices, command=command)
    om.grid(row=1, column=0)
    
    if return_obj:
        return v, om
    else:
        return v