#===========================standard library imports==========================#
#from Tkconstants import SW
from datetime import timedelta
#============================3rd party imports================================#
#================================local imports================================#
from cli_tools import *
import database
#==================================Constants==================================#
ORDER = ['seconds','minutes','hours','days','months','years','epochs']

DEPTH = 100
MAX_LEVEL = DEPTH
BORDER_X = 20
BORDER_Y = 100
TEXT_DEPTH = 12

CORNER_Y = BORDER_Y+DEPTH
MULTS = 1, 2, 5, 10, 25
#=============================================================================#
class TickElement():
    def __init__(self, tick_id, element, pixel_index):
        self.tick_id = tick_id
        self.element = element
        self.pixel_index = pixel_index
class GraphConfig():
    def __init__(self):
        self.ticklist = []
    def add(self, tick_id, key, pixel_index):
            a = TickElement(tick_id, key, pixel_index)
            self.ticklist.append(a)
    def search(self, e):
        for i in self.ticklist:
            if i.pixel_index == e:
                return i
        return None
class SceneList():
    def __init__(self, parent_frame, **kw):
        self.parent_frame = parent_frame
        self.tick_info = {}
        for key, val in kw.iteritems():
            self.__dict__[key] = val
    def addGraphics(self, s, x, obj, text=None):
        x = int(x)
        
        if '__string_ex' in obj.__dict__:
            text = obj.__string_ex()
        else:
            try:
                text = str(obj)
            except:
                printDebug(obj.__class__)
                exit()
        level_no = self.parent_frame.findLevelNo(x, text)
        
        if level_no is not None:
        
            level = DEPTH - (TEXT_DEPTH * level_no)
            self.parent_frame.tick(x, level=level, color=self.color, text=text)
        
            if s not in self.tick_info:
                self.tick_info[s] = GraphConfig()
            self.tick_info[s].add(s, obj, x)
        else:
            self.parent_frame.tick(x, level=DEPTH, color=self.color)
    def run_q(self, min_time, max_time):
        if self.hasList():
            self.scenes = self.list_
        else:
            self.scenes = self.scene_times(min_time, max_time)
    def hasList(self):
        a = 'list_' in self.__dict__
        return a and self.list_ is not None and self.list_ != []
    def trimlist(self, min_, max_):
        et = database.query.epics
        a = []
        for scene in self.list_:
            i = scene.time
            b = et.lesserOf(min_, i) is min_
            printDebug('{} < {}={}'.format(min_, i, b))
            if min_.isLessThan(i) and i.isLessThan(max_):
                a.append(scene)
        return a
    def disp_step(self, index, time_1, time_2):
        if 'scenes' not in self.__dict__:
            raise AttributeError('Query has not been run on {}'.format(self))
        
        
        return self.printTime(time_1, time_2, index)
    def makeTimeline(self, max, num_steps, delta, min_time, max_time):
        assert False
        printDebug('makeTimeline')
        total = self.total_times(delta)
        self.num_steps = num_steps
        max_scale, t = self.maxScale(total)
        
        self.step = float(max)/float(max_scale.val)
        prev = None
        
        printDebug('total={}'.format(total))
        for key, val in t.iteritems():
            printDebug('{}: {}'.format(key, val))
        
        
        exit('dev exit - makeTimeline')
        
        for i in range(max_scale.val):
            step2 = i * self.step
            time = self.getTime(max_scale, step2, min_time=min_time)
            
            if prev is not None:
                pass
                #TickElement2()
                #self.printTime(prev, time, t, step2, max_scale, min_time, max_time)
            prev = time
        printDebug('makeTimeline end')
        return total
    def getTime(self, max_scale, step, min_time):
        kw = dict()
        kw[max_scale.key] = step
        mid_delta = timedelta(**kw)
        return min_time.date_time + mid_delta
    def epochloop(self, el, id1=0, id2=0):
        if id1 <= el.id and el.id <= id2:
            return el    
    def scene_times(self, min_time, max_time):
        mine = min_time.epoch
        maxe = max_time.epoch            
        Times = database.utils.getClass('Times')
        kw = {'id1':mine.id, 'id2':maxe.id}
        a = []
        for epoch in database.utils.process('Epochs', for_each=self.epochloop, **kw):
            q = database.query.scenesByEpoch(epoch, existing=self.query)
            if epoch == mine:
                q.filter(Times.date_time >= min_time.date_time)
            if epoch == maxe:
                q.filter(Times.date_time <= max_time.date_time)
            
            a.extend(sorted(q.all(), key=self.sortkey))
        
        assert len(a) > 0
        return a
    def sortkey(self, scene):
        return scene.time.date_time
    def loop(self, el, min_time, max_time):
        raise NotImplementedError
    def checkScenes(self, prev, time):
        prev_time = prev.date_time
        time_time = time.date_time
        #printDebug('prev={} next={}'.format(prev_time, time_time))
        
        for scene in self.scenes:
            scene_time = scene.time.date_time
            if prev_time < scene_time and scene_time < time_time:
                return scene
            
        return None
    def printTime(self, prev, time, x):
        self.graphics = dict()
        m = self.checkScenes(prev, time)
        if m is not None:
            self.addGraphics('Scenes', x, m)
        return 1
    def __str__(self):
        return '<scenelist={}>'.format(self.choice)
    def __repr__(self):
        stri = ''
        for key, val in self.__dict__.iteritems():
            stri += '{}: {}\n'.format(key, val)
        return stri
            
            
            
            
            
            
    