#===========================standard library imports==========================#
import os
#============================3rd party imports================================#
from sqlalchemy_utils.functions.database import database_exists, create_database
#================================local imports================================#
import database.utils as utils
import file_utils as futils
from cli_tools import printDebug, printDictOfObject
from modselector import ModSelector
from MessageBox import onError, onQuest, onInfo
from tktools import top_window
from Nevermore import NCallback
from _ast import alias
#==================================Constants==================================#
FILENAME = 'filename'
PROJECT_ROOT = os.path.dirname(__file__)
ALLOW_AUTOLOAD = True
AUTO_FILENAME = 'autoconfig.conf'
AUTOLOAD_FILE = os.path.join(PROJECT_ROOT, AUTO_FILENAME)
AUTOLOAD_SEC = os.path.join(PROJECT_ROOT,'save', AUTO_FILENAME)
PASS_AUTH_MSG = 'password authentication failed'
SQL_ERR = 'relation "nvMaps" does not exist'
PERM_DENIED = 'permission denied for relation nvMaps'
PATH_WARN='Authentication Failed. Username or password incorrect'
OPERR_MSG = 'no such table: nvMaps'
LOGIN_EXCEPTION_OVERRIDE = False

SKIP_ONE = True
SKIP_TWO = True


def getAutoConfig():
    """Searches the file system for an autoconfig file that it can use to load 
    * a database.
    """
    a = os.path.exists(AUTOLOAD_FILE)
    b = os.path.exists(AUTOLOAD_SEC)
    if not ALLOW_AUTOLOAD:
        printDebug('Autoload Overriden')
        return None
    elif a:
        return futils.configFile(AUTOLOAD_FILE)
    elif b:
        return futils.configFile(AUTOLOAD_SEC)
    else:
        printDebug('manual load', interrupt_override=True)
        return None
def passthrough(a):
    return a
def autoload(parent=None, callback=passthrough):
        """
        """
        data = getAutoConfig()
        if data is None:
            return
        
        if FILENAME in data:
            if connect(parent=parent, callback=callback, filename=data[FILENAME]):
                #utils.initTables()
                return data['filename']
            else:
                printDebug('Asuna')
        else:
            __connect__(parent=parent, callback=callback, **data)
            #utils.initTables
            return data['alias']
        #return True
def __connect__(**data):
    #printDebug('connect 2')
    
    engine_url = utils.postgres.build(**data)
    
    
    if not database_exists(engine_url):
        
        #printDebug('creating new')
        createEmptyDatabase(engine_url, text=str(data['alias']))
    elif not SKIP_ONE:
        __connect_to_existing__(**data)
    if SKIP_ONE:
        __connect_to_existing__(**data)
def __connect_to_existing__(callback=passthrough, parent=None, **data):
    import database.bootstrap as bootstrap
    #parent = data.pop('parent', None)
    engine = utils.engine_config(**data)
    
    
    #session = utils.open_session()
    #printDebug('local engine={}'.format(engine))
    #session.configure(bind=engine)
    #exit(session.bind)
        
        
        
    lena = len(utils.open_session().query(bootstrap.nvMaps).all())
    if lena == 0:
        #printDebug('callback="{}"'.format(callback))
        ModSelector(top_window(), return_=NCallback(__open_connect__, parent, **data))
    elif parent is not None:
        __open_connect__(parent, **data)
    #printDebug('connect 2 open init')


def auto_import():
    import database.core07
    import database.dungeon_core02
    import database.bootstrap
    
def createEmptyDatabase(engine_url, text=None):
    NO_DATABASE = 'No database was found at {}. Do you want to create it?'
    if text is None:
        text=engine_url
    
    yn = onQuest(NO_DATABASE.format(text), 
                 title='No database!')
    if yn == 'yes':
        create_database(engine_url)
        utils.engine_config(url=engine_url)
        import database.bootstrap
        utils.create_all()
    else:
        exit(0)
        
        #onInfo('Reload Nevermore to access', 'Success! Database created!')
    #exit(0)
def __open_connect__(parent, **data):
    try:
        openInit(parent, **data)
        return True
    except Exception as e:
        raise
def __remove_quotes__(s):
    
    t = s.split('"')
    #printDebug(t)
    
    u = t[0] + t[2].strip()
    return u, t[1]
def connect(parent=None, master_config=None, filename=None):
        assert master_config is not None or filename is not None
        if master_config is None and filename != None:
            sqlite_file = filename
            sqliteLaunch(sqlite_file)
            return openInit(parent, alias=filename)
            
        elif master_config is not None:
            __connect__(**master_config)
        if master_config.__class__ == dict:
            alias = master_config['alias']
        else:
            alias = master_config.alias
        return openInit(parent, alias=alias)
def sqliteLaunch(sqlite_file, parent=None):
        
        kw = {'self.dbms':'SQLite3','filename':sqlite_file}
        
        #data = utils.S3Obj(sqlite_file)
        #printDictOfObject(data)
        
        if parent is not None:
            try:
                parent.cfg.alias.set(sqlite_file)
            except:
                parent.addTitle(sqlite_file)
        utils.engine_config(**kw)
        utils.int_boolean = True
def openInit(parent, cfg=None, alias=None, **data):
    #printDebug('openInit')
    try:
        if parent is not None:
            parent.initialize()
            parent.cfg = cfg
            if alias is not None:
                parent.addTitle(alias)
            return True
        else:
            return True
    except Exception as e:
            
        if not 'orig' in e.__dict__ or LOGIN_EXCEPTION_OVERRIDE:
            raise
        else:
            msg = str(e.orig)
        if PASS_AUTH_MSG in msg:
            raise
            onError(PATH_WARN)
            return False
        elif SQL_ERR in msg or OPERR_MSG in msg:
            parent.modSelLaunch()
            return True
        elif PERM_DENIED in msg:
            raise
            onError('Permission denied!')

