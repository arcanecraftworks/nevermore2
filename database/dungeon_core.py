assert False
import math
import os
from sqlalchemy import Sequence, Column, Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import relationship, backref
from random import randint

FILEROOT = os.path.dirname(__file__)

#import cli_tools as main
import core07 as core
import utils as utils
Base = core.Base

MOD_NAME = "Dungeonmasters' Mod"
READ_IN = ['powercenters', 'cityinfo', 'Classes','Species']
DESCRIPT_FILE = 'dd35descrip.txt'
DUNGEON_ROOT = os.path.join(FILEROOT, 'dungeonmaster')
DESCRIPT_PATH = os.path.join(DUNGEON_ROOT,DESCRIPT_FILE)
DUNGEON_STATIC_SAVE = os.path.join(DUNGEON_ROOT,'static tables')

GUARDS_DIVISOR = 100
MILITIA_DIVISOR = 20
officeholders = {60: 'warrior', 80: 'second-highest-level fighter', 100: 'highest-level-fighter'}
dd35_isolated_mix = {96: 'human',2: 'halfling', 1: 'elf'}
dd35_mixed = {79: 'human', 9: 'halfling', 5:'elf', 3: 'dwarf', 2: 'gnome', 1: 'half-elf', 1: 'half-orc'}
dd35_integrated_mix = {37: 'human', 20: 'halfling', 18: 'elf', 10: 'dwarf', 7:'gnome', 5: 'half-elf', 3: 'half-orc'}
poweralign = {35: 1, 39: 2, 41: 3, 61: 4, 63: 5, 64: 6, 90: 7,98: 8, 100: 9}
powercenters = {13:'Conventional',18: 'Nonstandard', 70: 'Magical'}
dd35_traits = open(DESCRIPT_PATH, 'r').read().split('\n')
def getTables():
    return Base.metadata.tables
# Tables 5-2 Random Town Generation && Power Centers  
class cityinfo(Base):
    __tablename__ = 'cityinfo'
    __tabletype__ = 'dd35'
    #===========================#
    id = Column(Integer, Sequence('dd35_5_2_seq'), primary_key=True)
    d_upper = Column(Integer)
    town_size = Column(String(20))
    pop_upper = Column(Integer)
    gp_limit = Column(Integer)
    power_mod = Column(Integer)
    power_mult = Column(Integer)
    level_mod = Column(Integer)
    def __repr__(self):
        return self.town_size
class char_class_assoc(Base):
    __tablename__ = 'char_class_assoc'
    __tabletype__ = 'dd35'
    __associationchild__ = 'Characters'
    #====================#
    id = Column(Integer, Sequence('cca_seq'), primary_key=True)
    class_id = Column(Integer, ForeignKey('Classes.id'))
    character_id = Column(Integer, ForeignKey('Characters.id'))
    child = relationship('Characters',backref='dd35_class')
class Classes(Base):
    __tablename__ = 'Classes'
    __tabletype__ = 'dd35'
    #============================#
    id = Column(Integer, Sequence('class_seq'), primary_key=True)
    name = Column(String(50))
    pc_class = Column(Integer)
    town_char_level_roll = Column(String(50))
    rels = {'members':char_class_assoc,}
    members = relationship("char_class_assoc",
                                 cascade="all, delete-orphan",
                                 passive_deletes=True)
    
    def __repr__(self):
        return self.name

debug = True
def prompt_loop(prompt, err_response, findLevelNo, input_type='str', override=False, session=None):
    #print_err('prompt_loop')
    command = '{}(raw_input("{} "))'.format(input_type, prompt)
    global debug
    if override and debug:
        #print_err('prompt overridden')
        i = eval(command)
        return findLevelNo(i)
    else:
        while True:
            try:
                #print_err('inner loop')
                i = eval(command)
                return findLevelNo(i)
            except:
                print err_response
def stub(i):
    return i
def rollstring(s):
    st = s.split('d')
    s0 = int(st[0])
    s1 = int(st[1])
    total = 0
    for i in range(s0):
        total += roll(s1, auto=True)
    return total
def roll(number, default=60, auto=True):
    if number == 100:
        prompt = 'd%->'
    else:
        prompt = 'd{}->'.format(number)
    
    if auto:
        return randint(1, number)
    elif debug:
        return default
    else:
        return prompt_loop(prompt,'Please enter an integer',stub)
def lookup(dicti, die_result=10):
    max = 0
    min = None
    max_val = None
    for d in dicti:
        #print '{}: {}'.format(d, dicti[d])
        if d > max:
            max = d
        if d < min and min != -1 or min is None:
            min = d
        if d == -1:
            max_val = dicti[d]
    stri = dicti[max]
    #print 'max = ' + str(max)
    #print 'min = ' + str(min)
    for i in range(max):
        switch = max-i+1
        if switch in dicti:
            stri = dicti[switch]
        #print '{} - {}'.format(switch, stri)
        if switch == die_result:
            return stri
def number(num):
    digit = num % 10
    #print 'digit=' + str(digit)
    if digit == 1 and num != 11:
        return '{}st'.format(num)
    if digit == 2 and num != 12:
        return '{}nd'.format(num)
    if digit == 3 and num != 13:
        return '{}rd'.format(num)
    else:
        return '{}th'.format(num)
def town_generate(interrupt='manual'):
    auto = False    
    d = roll(100, auto=auto)
    session = utils.open_session()
    #rows = utils.select_star('cityinfo', printTable=False)
    town = core.Cities()
    town_loc = core.locationClone(town)
    scene = core.Scenes()
    
    rows = []
    for row in session.query(cityinfo):
        rows.append(row)
    
    cur = 0
    prev = 0
    for i in range(len(rows)):
        row = rows[i]
        if row.d_upper <= d:
            cur = i
            prev = i-1
    
    if prev < 0:
        prev = 0
        pop_lower = 1
    else:
        prev_o = rows[prev]
        pop_lower = prev_o.pop_upper-1
    
    cur_o = rows[cur]
    print 'town size: ' + cur_o.town_size
    modifier = cur_o.power_mod
    multiplier = cur_o.power_mult
    print 'modifier: ' + str(modifier)
    
    #roll d% again to determine exact population
    e = roll(100, auto)
    
    pop_upper = cur_o.pop_upper
    diff = pop_upper - pop_lower
    ratio = diff / 100.0
    pop = int(math.ceil(pop_lower + (ratio * e)))
    print 'population: ' + str(pop)
    
    num = 0
    for i in range(multiplier):
        num += roll(20, default=10, auto=auto)
    print 'result = ' + str(num)
    
    powercenter = lookup(powercenters, num)
    print 'powercenter = ' + powercenter
    #TODO: link up with core alignment chart
    
    pca_roll = roll(100, auto=auto)
    print 'pca_roll = ' + str(pca_roll)
    pc_align = lookup(poweralign, pca_roll)
    
    nTableAlign = None
    nTableChar = None
    #nTableClass = None
    for t in utils.nTables:
        nt = utils.nTables[t]
        if nt.name == 'Alignments':
            nTableAlign = nt
        elif nt.name == 'Characters':
            nTableChar = nt
        #elif nt.name == 'Classes':
            #nTableClass = nt
    
    town = utils.getClass('Cities')()
    
    alignment = utils.object_query_at('Alignments', pc_align, session=session)
    exit('dungeon_core.town_generate')
    print 'Power Center Alignment = ' + str(alignment)
    
    numguards = int(math.floor(pop/GUARDS_DIVISOR))
    print 'Number of Guards: ' + str(numguards)
    numMilitia = int(math.floor(pop/MILITIA_DIVISOR))
    print 'Number of Militia: ' + str(numMilitia)
    
    characters = []
    npc_classes = []
    for dClass in session.query(Classes):
        highest_level = rollstring(dClass.town_char_level_roll)
        characters.extend(addClassCascade(dClass, highest_level, 1, session, town))
        if dClass.pc_class == 0:
        #if dClass.name == 'Commoner' or dClass.name == 'Warrior' or dClass.name == 'Expert' or dClass.name == 'Aristocrat' or dClass.name == 'Adept':
            npc_classes.append(dClass)
    
    
    remainder = dict()
    pop_remainder = pop - len(characters)
    num_commoners = int(math.floor(pop_remainder * 0.91))
    remainder['Commoner'] = num_commoners
    num_warriors = int(math.floor(pop_remainder * 0.05))
    remainder['Warrior'] = num_warriors
    num_experts = int(math.floor(pop_remainder * 0.03))
    remainder['Expert'] = num_experts
    last_remainder = pop_remainder - num_commoners - num_warriors - num_experts
    num_aristocrats = int(math.floor(last_remainder * 0.5))
    remainder['Aristocrat'] = num_aristocrats
    num_adepts = last_remainder - num_aristocrats
    remainder['Adept'] = num_adepts
    
    for dClass in npc_classes:
        multiplier = remainder[dClass.name]
        addClass(dClass, remainder[dClass.name], 1, session, town)
    
    

    exit('dungeon_core test terminated successfully.')
    session.add_all(characters)
    session.commit()
    session.close()
def addClass(dClass, num, level, session, town):
    characters = []
    print '{}x {} level {}'.format(num, number(level), dClass.name)
    for m in range(num):
        n = core.Characters(first_name='NPC',char_type='NPC', height=level, misc=dd35_traits[randint(0, len(dd35_traits)-1)])
        utils.assoc(dClass, 'members',n)
        
        characters.append(n)
    return characters
def addClassCascade(dClass, rollResult, multiplier, session, town):
    characters = []
    if rollResult == 0:
        return []
    if dClass.pc_class == 1 or (dClass.pc_class == 0 and rollResult != 1):    
        characters.extend(addClass(dClass, multiplier, rollResult, session, town))
        addClassCascade(dClass, int(math.floor(rollResult/2)), multiplier * 2, session, town)
    return characters
    
        
        
        
        
        