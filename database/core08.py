assert False
#===========================standard library imports==========================#
import sys
#============================3rd party imports================================#
from sqlalchemy import Sequence, Column, Integer, String, \
ForeignKey, DateTime, Date, Text, Boolean
from sqlalchemy.orm import relationship, backref, relation
from datetime import date
#================================local imports================================#
import bootstrap
#==================================Constants==================================#
SCOPE_DEFAULT = True

Base = bootstrap.Base
#=============================================================================#
#import utils
#import dungeon_core
READ_IN = ['Alignments']
MOD_NAME = 'core'
DB_VERSION = '0.0.8'
MEASURE_SYS = 'in'
#class db_options(Base):
#    __tablename__ = 'db_options'
#    __tabletype__ = 'sys'
#
#    key = Column(String(50), primary_key=True)
#    val = Column(Text)
class Series(Base):
    __tablename__ = 'Series'
    __tabletype__ = 'front'
    name_singular = 'Series'
    filter_out = ['id']
    #======================#
    id = Column(Integer, Sequence('ser_id_seq'), primary_key=True)
    title = Column(String(50))
    summary = Column(Text)
    universe_id = Column(Integer, ForeignKey('Universes.id'))
    universe = relationship("Universes",backref=backref('stories', 
                                                        order_by=id))
    def __repr__(self):
        return self.title
    @staticmethod
    def gui(parent):
        parent.simpleField('title')
        parent.textField('summary')
        parent.fkField('universe', Universes) 
class Stories(Base):
    __tablename__ = 'Stories'
    __tabletype__ = 'front'
    name_singular = 'Story'
    display_names = {'series_number':'Part no:', }
    filter_out = ['id']
    #=====================#
    id = Column(Integer, Sequence('story_id_seq'), primary_key=True)
    series_number = Column(Integer)
    title = Column(String(50))
    tagline = Column(String(50))
    jacket_summary = Column(Text)
    synopsis = Column(Text)
    series_id = Column(Integer, ForeignKey('Series.id'))
    series = relationship("Series",backref=backref('stories', order_by=id))    
    def __repr__(self):
        return self.title
    @staticmethod
    def gui(parent):
        parent.fkField('series', Series)
        parent.intField('series_number', caption='Part no:')
        parent.simpleField('title')
        parent.simpleField('tagline')
        parent.textField('jacket_summary')
        parent.textField('synopsis')
class Chapters(Base):
    __tablename__ = 'Chapters'
    __tabletype__ = 'front'
    name_singular = 'Chapter'
    filter_out = ['id']
    sort = ['chapter_number']
    #=======================#
    id = Column(Integer, Sequence('chap_id_seq'), primary_key=True)
    story_id = Column(Integer, ForeignKey('Stories.id'))
    story = relationship("Stories",backref=backref('chapters'))
    chapter_number = Column(Integer)
    title = Column(String(50))
    summary = Column(Text)
    def __repr__(self):
        if self.chapter_number == 0:
            return 'Prologue: ' + self.title
        elif self.chapter_number == -1:
            return 'Epilogue: ' + self.title
        else:
            return 'Chapter ' + str(self.chapter_number) + ': ' + self.title
    @staticmethod
    def gui(parent):
        parent.fkField('story', Stories)
        parent.intField('chapter_number')
        parent.simpleField('title')
        parent.textField('summary') 
class scene_char_assoc(Base):
    __tablename__='scene_char_assoc'
    __tabletype__='back'
    __assocationchild__ = 'Characters'
    #===================#
    scene_id = Column(Integer, ForeignKey('Scenes.id'), primary_key=True)
    character_id = Column(Integer, ForeignKey('Characters.id'), primary_key=True)
    child = relationship('Characters')
    def __repr__(self):
        return str(self.child)
class Scenes(Base):
    __tablename__ = 'Scenes'
    __tabletype__ = 'front'
    name_singular = 'Scene'
    rels = {'characters':scene_char_assoc}
    display_names = {'sc_type':'Scene Type: '}
    #sort = ['scene_number','chapter_id']
    sort = ['chapter_id','scene_number','time_id']
    filter_out = ['id']
    #======================#
    id = Column(Integer, Sequence('scene_id_seq'), primary_key=True)
    title = Column(String(50))
    chapter_id = Column(Integer, ForeignKey('Chapters.id'))
    chapter = relationship("Chapters",backref=backref('scenes'))
    scene_number = Column(Integer)
    plotline_id = Column(Integer, ForeignKey('Plotlines.id'))
    plotline = relationship("Plotlines", backref=backref('plotlines'))
    #sc_type = Column(String(50))
    status = Column(String(50))
    status_id = Column(Integer, ForeignKey('scene_status.id'))
    status = relationship("scene_status",backref=backref('scenes'))
    time_id = Column(Integer, ForeignKey('Times.id'))
    time = relationship("Times")
    summary = Column(String(500))
    location_id = Column(Integer, ForeignKey('Locations.id'))
    location = relationship("Locations",backref=backref('scenes'))
    characters = relationship("scene_char_assoc", cascade="all, delete-orphan", backref="scenes")
    def __repr__(self):
        return self.title
    @staticmethod
    def gui(parent):
        #parent.simpleField('Title: ','title')
        parent.simpleField('title')
        parent.fkField('chapter', Chapters)
        parent.intField('scene_number')
        parent.fkField('plotline', Plotlines)
        #parent.simpleField('sc_type', caption='Scene Type: ')
        parent.fkField('status', scene_status)
        parent.timeField('time', return_type=Times)
        parent.textField('summary')
        parent.fkField('location', Locations)
        parent.m2mField('characters', Characters)
class scene_status(Base):
    __tablename__ = 'scene_status'
    __tabletype__ = 'back'
    #=======================#
    id = Column(Integer, Sequence('char_id_seq'), primary_key=True)
    name = Column(String(50))
    description = Column(Text)
    def __repr__(self):
        return self.name
    @staticmethod
    def gui(parent):
        parent.simpleField('name')
        parent.textField('description')
class char_culture_assoc(Base):
    __tablename__ = 'char_culture_assoc'
    __tabletype__ = 'back'
    __assocationchild__ = 'Cultures'
    #====================#
    character_id = Column(Integer, ForeignKey('Characters.id'), primary_key=True)
    culture_id = Column(Integer, ForeignKey('Cultures.id'), primary_key=True)
    child = relationship('Cultures', backref=backref('characters'))
    def __repr__(self):
        return str(self.child)
class char_species_assoc(Base):
    __tablename__ = 'char_species_assoc'
    __tabletype__ = 'back'
    __assocationchild__ = 'Species'
    #=====================#
    character_id = Column(Integer, ForeignKey('Characters.id'), primary_key=True)
    parent = relationship('Characters', backref=backref('Species'))
    species_id = Column(Integer, ForeignKey('Species.id'), primary_key=True)
    child = relationship('Species', backref=backref('character_examples'))
    def __repr__(self):
        return str(self.child)
class char_lang_assoc(Base):
    __tablename__ = 'char_lang_assoc'
    __tabletype__ = 'back'
    __associationchild__ = 'language'
    #=====================#
    character_id = Column(Integer, ForeignKey('Characters.id'), primary_key=True)
    language_id = Column(Integer, ForeignKey('Languages.id'), primary_key=True)
    child = relationship('Languages', backref=backref('spoken_by'))
    def __repr__(self):
        return str(self.child)
class Character_Associations(Base):
    __tablename__ = 'Character_Associations'
    __tabletype__ = 'front_'
    name_singular = 'Relationship'
    __associationchild__ = 'Character'
    #============================#
    id = Column(Integer, Sequence('rel_id_seq'), primary_key=True)
    charOne = Column(Integer, ForeignKey('Characters.id'))
    char_parent = relationship('Characters', foreign_keys=charOne)
    charTwo = Column(Integer, ForeignKey('Characters.id'))
    child = relationship('Characters', foreign_keys=charTwo)
    type = Column(String(50))
    description = Column(Text)
    @staticmethod
    def gui(parent):
        #TODO: stub
        pass
class Characters(Base):
    __tablename__='Characters'
    __tabletype__='front'
    name_singular = 'Character'
    filter_out = ['id','occupation','alignment','universe',
                  'education','hometown_id','hair_color',
                  'eye_color','universe','dislikes',
                  'habits','financial','backstory','misc'
                  'birthdate_id','deathdate_id']
    #============================#
    id = Column(Integer, Sequence('char_id_seq'), primary_key=True)
    
    first_name_id = Column(String(50), ForeignKey('Names.name'))
    first_name = relationship('Names', foreign_keys=first_name_id, 
                              backref=backref('first_names'), lazy='joined')
    middle_name_id = Column(String(50), ForeignKey('Names.name'))
    middle_name = relationship('Names', foreign_keys=middle_name_id, 
                               backref=backref('middle_names'))
    last_name_id = Column(String(50), ForeignKey('Names.name'))
    last_name = relationship('Names', foreign_keys=last_name_id, 
                             backref=backref('last_names'))
    
    #char_type = Column(String(50))
    significance = Column(String(50))
    description = Column(Text)
    gender = Column(String(20))
    alignment_id = Column(Integer, ForeignKey('Alignments.id'))
    alignment = relationship('Alignments', 
                             backref=backref("Character_examples"))
    hometown_id = Column(Integer, ForeignKey('Locations.id'))
    hometown = relationship('Locations', 
                            backref=backref('characters'), lazy='joined')
    birthdate_id = Column(Integer, ForeignKey('Times.id'))
    birthdate = relationship("Times",
                              foreign_keys=birthdate_id, lazy='joined')
    deathdate_id = Column(Integer, ForeignKey('Times.id'))
    deathdate = relationship("Times", foreign_keys=deathdate_id, 
                             lazy='joined')
    height = Column(Integer)
    hair_color = Column(String(50))
    eye_color = Column(String(50))
    occupation = Column(String(50))
    likes = Column(Text)
    dislikes = Column(Text)
    hopes = Column(Text)
    fears = Column(Text)
    personality = Column(Text)
    habits = Column(Text) 
    financial = Column(Text)
    education = Column(Text)
    hobbies = Column(Text)
    backstory = Column(Text)
    misc = Column(Text)
    
    
    universe_id = Column(Integer, ForeignKey('Universes.id'))
    universe = relationship("Universes", foreign_keys=universe_id)
    religion_id = Column(Integer, ForeignKey('Religions.id'))
    religion = relationship("Religions", backref=backref('followers'))

    rels = {'cultures':char_culture_assoc, 'species':char_species_assoc, 
            'languages':char_lang_assoc, 'relationships':Character_Associations}
    cultures = relationship('char_culture_assoc', cascade="all, delete-orphan", 
                            backref=backref("characters"), lazy='joined')
    species = relationship("char_species_assoc", cascade="all, delete-orphan", 
                           backref=backref("characters"), lazy='joined')
    languages = relationship('char_lang_assoc', cascade="all, delete-orphan", 
                             backref=backref('speakers'))
    relationships = relationship("Character_Associations",
                                 cascade="all, delete-orphan",
                                 passive_deletes=True,
                                 foreign_keys=Character_Associations.charOne,
                                 backref=backref("relationships_backref"))
    def __repr__(self):
        stri = ''
        if self.first_name is not None:
            stri += str(self.first_name) + ' '
        if self.last_name is not None:
            stri += str(self.last_name) + ' '
        if stri == '':
            stri = 'No one'
        return stri.rstrip(' ')
    @staticmethod
    def gui(parent):
        #TODO: picture
        #TODO: name field
        parent.nameField('first_name', caption='First Name: ')
        parent.nameField('middle_name', caption='Middle Name: ')
        parent.nameField('last_name', caption='Last Name:')
        
        #char_type = Column(String(50))
        sig_opt = ['Main','Secondary','Teritiary']
        gender_opt = ['None','Male','Female','Androgynous']
        #TODO: editable database entry
        parent.optionField('significance', sig_opt)
        parent.textField('description')
        parent.optionField('gender',gender_opt)
        parent.fkField('alignment', Alignments)
        parent.fkField('hometown', Locations)
        parent.timeField('birthdate', return_type=Times)
        parent.timeField('deathdate', return_type=Times)
        parent.intField('height', caption='Height ({})'.format(MEASURE_SYS))
        parent.simpleField('hair_color')
        parent.simpleField('eye_color')
        parent.simpleField('occupation')
        parent.fkField('universe', Universes)
        parent.fkField('religion', Religions)
        parent.m2mField('cultures', Cultures)
        parent.m2mField('species', Species)
        parent.m2mField('languages', Languages)
        
        parent.textField('likes')
        parent.textField('dislikes')
        parent.textField('hopes')
        parent.textField('fears')
        parent.textField('personality')
        parent.textField('habits')
        #parent.textField('financial')
        parent.textField('education')
        parent.textField('hobbies')
        parent.textField('backstory')
        parent.textField('misc')
class Plotlines(Base):
    __tablename__ = 'Plotlines'
    __tabletype__ = 'front'
    name_singular = 'Plotline'
    filter_out = ['id']
    #=========================#
    id = Column(Integer, Sequence('plot_id_seq'), primary_key=True)
    pov_id = Column(Integer, ForeignKey('Characters.id'))
    pov = relationship("Characters")
    name = Column(String(50))
    narration = Column(String(50))
    omniscience = Column(String(50))
    plot_type = Column(String(50))
    @staticmethod
    def gui(parent):
        parent.fkField('pov', Characters, caption='POV: ')
        parent.simpleField('name')
        #TODO: database tables
        narr_opt = ['third','first','second']
        parent.optionField('narration', narr_opt, caption='Person: ') 
        omni_opt = ['Limited','Semi-Limited','Omniscient']
        parent.optionField('omniscience', omni_opt)
        parent.simpleField('plot_type')
    def __repr__(self):
        return str(self.name)
class Populations(Base):
    __tablename__ = 'Populations'
    __tabletype__ = 'front'
    name_singular = 'Population'
    __assocationchild__ = 'Cultures'
    rels = []
    filter_out = ['id']
    #==========================#
    id = Column(Integer, Sequence('pop_seq'), primary_key=True)
    location_id = Column(Integer, ForeignKey('Locations.id'))
    location = relationship('Locations', backref=backref('populations'))
    culture_id = Column(Integer, ForeignKey('Cultures.id'))
    culture = relationship('Cultures', backref=backref('settlements'))
    species_id = Column(Integer, ForeignKey('Species.id'))
    species = relationship('Species')
    size = Column(Integer)
    @staticmethod
    def gui(parent):
        parent.fkField('location',Locations)
        parent.fkField('culture',Cultures)
        parent.fkField('species', Species)
        parent.intField('size', caption='Size: ')
    def __repr__(self):
        if self.culture_id is not None:
            tmp = str(self.culture)
        else:
            tmp = str(self.species)
        return str('{} {} in {}'.format(self.size, tmp, str(self.location)))
class loc_assoc(Base):
    __tablename__ = 'loc_assoc'
    __tabletype__ = 'back'
    __associationchild__ = 'Locations'
    #=====================#
    location_parent = Column(Integer, ForeignKey('Locations.id', ondelete='CASCADE'), primary_key=True)
    location_child = Column(Integer, ForeignKey('Locations.id', ondelete='CASCADE'), primary_key=True)
    child = relationship('Locations', foreign_keys=location_child, backref=backref('contains'))
    
    def __repr__(self):
        return str(self.child)
class Locations(Base):
    __tablename__ = 'Locations'
    __tabletype__ = 'front'
    name_singular = 'Location'
    rels = {'lies_within':loc_assoc}
    filter_out = ['id','mk_del']
    #========================#
    id = Column(Integer, Sequence('loc_id_seq'), primary_key=True)
    name_id = Column(String(50), ForeignKey('Names.name'))
    name = relationship('Names', foreign_keys=name_id, lazy='joined')
    description = Column(Text)
    #TODO: expand terrain
    terrain = Column(String(50))
    lies_within = relationship("loc_assoc",
                               cascade="all, delete-orphan",
                               passive_deletes=True,
                               foreign_keys=loc_assoc.location_parent,
                               backref=backref("contains"))
    type = Column('type', String(50))
    mk_del = Column(Boolean, nullable=False, default=False)
    __mapper_args__ = {'polymorphic_on': type}
    def __repr__(self):
        return str(self.name)
    @staticmethod
    def gui(parent):
        #TODO: nameFields
        parent.nameField('name')
        parent.textField('description')
        parent.simpleField('terrain')
        parent.m2mField('lies_within', Locations)
        #parent.m2mField('contains', Locations)
class item_assoc(Base):
    __tablename__ = 'item_assoc'
    __tabletype__ = 'back'
    __associationchild__ = 'Characters'
    #====================#
    id = Column(Integer, Sequence('ia_seq'), primary_key=True)
    item_id = Column(Integer, ForeignKey('Items.id'))
    character_id = Column(Integer, ForeignKey('Characters.id'))
    child = relationship('Characters',backref='items')
    date = Column(Integer, ForeignKey('Times.id'))
    def __repr__(self):
        return str(self.child)
class Items(Base):
    __tablename__ = 'Items'
    __tabletype__ = 'front'
    name_singular = 'Item'
    filter_out = ['id']
    #======================#
    id = Column(Integer, Sequence('item_id_seq'),primary_key=True)
    name_id = Column(String(50), ForeignKey('Names.name'))
    name = relationship('Names', foreign_keys=name_id, lazy='joined')
    classification = Column(Text)
    description = Column(Text)
    properties = Column(Text)
    creator_id = Column(Integer,ForeignKey("Characters.id"))
    creator = relationship("Characters", foreign_keys=creator_id)
    owners_id = Column(Integer, ForeignKey("Characters.id"))
    owners = relationship('item_assoc', cascade="all, delete-orphan", backref=backref("items"))
    location_id = Column(Integer, ForeignKey("Locations.id"))
    location = relationship("Locations", backref=backref("items"))
    def __repr__(self):
        return self.name
    @staticmethod
    def gui(parent):
        parent.nameField('name')
        parent.simpleField('classfication')
        #TODO: classification into fk to it's own table
        parent.textField('description')
        parent.textField('properties')
        parent.fkField('creator', Characters)
        #TODO: make gui for item ownership
        parent.fkField('owner', Characters)
        parent.fkField('location',Locations)
class Universes(Locations):
    __tablename__ = 'Universes'
    __tabletype__ = 'front'
    filter_out = ['id','type']
    id = Column(Integer, ForeignKey('Locations.id', ondelete='CASCADE'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'Universe', 'inherit_condition': (id==Locations.id)}
    name_singular = 'Universe'
    _idref = relation(Locations, foreign_keys=id, uselist=False, primaryjoin=id==Locations.id)
    def __repr__(self):
        return str(self.name)
    @staticmethod
    def gui(parent):
        Locations.gui(parent)
class Worlds(Locations):
    __tablename__ = 'Worlds'
    __tabletype__ = 'front'
    #filter_out = ['id']
    id = Column(Integer, ForeignKey('Locations.id', ondelete='CASCADE'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'World', 'inherit_condition': (id==Locations.id)}
    name_singular = 'World'
    filter_out = ['id','type']
    _idref = relation(Locations, foreign_keys=id, primaryjoin=id==Locations.id)
    def __repr__(self):
        return str(self.name)
    @staticmethod
    def gui(parent):
        Locations.gui(parent)
class States(Locations):
    __tablename__ = 'States'
    __tabletype__ = 'front'
    filter_out = ['id','type']
    id = Column(None, ForeignKey('Locations.id', ondelete='CASCADE'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'State', 'inherit_condition': (id==Locations.id)}
    name_singular = 'State'
    _idref = relation(Locations, foreign_keys=id, primaryjoin=id==Locations.id)
    #======================#
    ruled_by_id = Column(Integer, ForeignKey("Locations.id"))
    ruled_by = relationship("States", remote_side=Locations.id, foreign_keys=ruled_by_id, backref=backref("subdomains"))
    capital_id = Column(Integer, ForeignKey("Locations.id"))
    capital = relationship("Towns", foreign_keys=capital_id, backref=backref("capital_of"))
    def __repr__(self):
        return str(self.name)
    @staticmethod
    def gui(parent):
        Locations.gui(parent)
        parent.fkField('ruled_by', States)
        parent.fkField('capital', Towns)
class Towns(Locations):
    __tablename__ = 'Towns'
    __tabletype__ = 'front'
    filter_out = ['id','type','terrain']
    id = Column(Integer, ForeignKey('Locations.id', ondelete='CASCADE'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'Town', 'inherit_condition': (id==Locations.id)}
    name_singular = 'Town'
    _idref = relation(Locations, foreign_keys=id, primaryjoin=id==Locations.id)
    power_center = Column(String(50))
    pc_align_id = Column(Integer, ForeignKey('Alignments.id'))
    pc_align = relationship('Alignments', foreign_keys=pc_align_id)
    def __repr__(self):
        return str(self.name)
    @staticmethod
    def gui(parent):
        Locations.gui(parent)
        center_opt = ['Conventional','Nonstandard','Magical']
        parent.optionField('power_center', center_opt)
        parent.fkField('pc_align', Alignments, caption='Alignment')
class Cities(Locations):
    __tablename__ = 'Cities'
    __tabletype__ = 'front'
    filter_out = ['id','type','terrain']
    id = Column(None, ForeignKey('Locations.id', ondelete='CASCADE'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity':'City', 'inherit_condition': (id==Locations.id)}
    name_singular = 'City'
    _idref = relation(Locations, foreign_keys=id, primaryjoin=id==Locations.id)
    power_center = Column(String(50))
    pc_align_id = Column(Integer, ForeignKey('Alignments.id'))
    pc_align = relationship('Alignments', foreign_keys=pc_align_id)
    def __repr__(self):
        return str(self.name)
    @staticmethod
    def gui(parent):
        Locations.gui(parent)
class cult_inherit(Base):
    __tablename__ = 'cult_inherit'
    __tabletype__ = 'back'
    __associationchild__ = 'Cultures'
    #=======================#
    culture_parent = Column(Integer, ForeignKey('Cultures.id'), primary_key=True)
    culture_child = Column(Integer, ForeignKey('Cultures.id'), primary_key=True)
    child = relationship('Cultures', foreign_keys=culture_child, backref=backref('parents'))
    def __repr__(self):
        return str(self.child)
class cult_relig(Base):
    __tablename__ = 'cult_relig'
    __tabletype__ = 'back'
    __associationchild__ = 'Religions'
    #======================#
    culture_id = Column(Integer, ForeignKey('Cultures.id'), primary_key=True)
    religion_id = Column(Integer, ForeignKey('Religions.id'), primary_key=True)
    child = relationship('Religions', backref=backref('cultures'))
    def __repr__(self):
        return str(self.child)
class Cultures(Base):
    __tablename__ = 'Cultures'
    __tabletype__ = 'front'
    name_singular = 'Culture'
    filter_out = ['id']
    #=========================#
    id = Column(Integer, Sequence('cult_id_seq'), primary_key=True)
    name_id = Column(String(50), ForeignKey('Names.name'))
    name = relationship('Names', foreign_keys=name_id, lazy='joined')
    trad_dress_male = Column(Text)
    trad_dress_female = Column(Text)
    trad_dress_child = Column(Text)
    description = Column(Text)
    rels = {'latest_techs':'tech_culture_assoc', 'derived_from':cult_inherit, 'religions':cult_relig, 'subcultures':cult_inherit}
    latest_techs = relationship('techs_culture_assoc', cascade="all, delete-orphan")
    derived_from = relationship('cult_inherit',
                                cascade="all, delete-orphan",
                                passive_deletes=True,
                                foreign_keys=cult_inherit.culture_parent,
                                backref=backref("subcultures"))
    religions = relationship('cult_relig', cascade="all, delete-orphan", backref=backref("adopted_by_cultures"))
    religions = relationship('cult_relig', cascade="all, delete-orphan")
    def __repr__(self):
        return str(self.name)
    @staticmethod
    def gui(parent):
        parent.nameField('name')
        parent.textField('description')
        parent.textField('trad_dress_male', caption='Traditional Male Dress: ')
        parent.textField('trad_dress_female', caption='Traditional Female Dress: ')
        parent.m2mField('religions', Religions)
        parent.m2mField('derived_from', Cultures)
        #parent.m2mField('subcultures', Cultures)
class lang_inherit(Base):
    __tablename__ = 'lang_inherit'
    __tabletype__ = 'back'
    __associationchild__ = 'language'
    #====================#
    language_parent = Column(Integer, ForeignKey('Languages.id'), primary_key=True)
    language_child = Column(Integer, ForeignKey('Languages.id'), primary_key=True)
    child = relationship('Languages', foreign_keys=language_child, backref=backref('parents'))
    def __repr__(self):
        return str(self.child)
class Languages(Base):
    __tablename__ = 'Languages'
    __tabletype__ = 'front'
    name_singular = 'Language'
    filter_out = ['id']
    #========================#
    id = Column(Integer, Sequence('cult_id_seq'), primary_key=True)
    name = Column(String(50))
    description = Column(Text)
    rels = {'derived_from':lang_inherit}
    derived_from = relationship('lang_inherit',
                                cascade="all, delete-orphan",
                                passive_deletes=True,
                                foreign_keys=lang_inherit.language_parent,
                                backref=backref("root_for"))
    def __repr__(self):
        return self.name
    @staticmethod
    def gui(parent):
        parent.simpleField('name')
        parent.textField('description')
        parent.m2mField('derived_from', Languages)
class spec_inherit(Base):
    __tablename__ = 'spec_inherit'
    __tabletype__ = 'back'
    __associationchild__ = 'Species'
    #===========================#
    species_parent = Column(Integer, ForeignKey('Species.id'), primary_key=True)
    species_child = Column(Integer, ForeignKey('Species.id'), primary_key=True)
    child = relationship('Species', foreign_keys=species_child, backref=backref('parents'))
    def __repr__(self):
        return str(self.child)
class Species(Base):
    __tablename__ = 'Species'
    __tabletype__ = 'front'
    name_singular = 'Species'
    filter_out = ['id']
    #=========================#
    id = Column(Integer, Sequence('spec_id_seq'), primary_key=True)
    name = Column(String(50))
    name_plural = Column(String(50))
    origin = Column(Text)
    description = Column(Text)
    consciousness = Column(Text)
    abilities = Column(Text)
    size_class = Column(String)
    rels = {'descended_from':spec_inherit}
    descended_from = relationship('spec_inherit',
                                  cascade="all, delete-orphan",
                                  passive_deletes=True,
                                  foreign_keys=spec_inherit.species_parent,
                                  backref=backref("subspecies"))
    def __repr__(self):
        return self.name
    @staticmethod
    def gui(parent):
        parent.simpleField('name')
        parent.textField('description')
        parent.m2mField('descended_from', Species)
        #parent.m2mField('subspecies', Species)
class relig_inherit(Base):
    __tablename__ = 'relig_inherit'
    __tabletype__ = 'back'
    __associationchild__ = 'Religion'
    #=======================#
    religion_parent = Column(Integer, ForeignKey('Religions.id'), primary_key=True)
    religion_child = Column(Integer, ForeignKey('Religions.id'), primary_key=True)
    child = relationship('Religions', foreign_keys=religion_child, backref=backref('parents'))
    def __repr__(self):
        return str(self.child)
class relig_holiday_assoc(Base):
    __tablename__ = 'relig_holiday_assoc'
    __tabletype__ = 'back'
    __associationchild__ = 'Holidays'
    #======================#
    religion = Column(Integer, ForeignKey('Religions.id'), primary_key=True)
    holiday = Column(Integer, ForeignKey('Holidays.id'), primary_key=True)
    child = relationship('Holidays', backref=backref('religions'))
    def __repr__(self):
        return str(self.child)
class Religions(Base):
    __tablename__ = 'Religions'
    __tabletype__ = 'front'
    name_singular = 'Religion'
    filter_out = ['id']
    #=======================#
    id = Column(Integer, Sequence('species_id_seq'), primary_key=True)
    name = Column(String(50))
    description = Column(Text)
    beliefs = Column(Text)
    practices = Column(Text)
    rels = {'split_from':relig_inherit, 'holidays':relig_holiday_assoc}
    split_from = relationship('relig_inherit',
                              cascade="all, delete-orphan",
                              passive_deletes=True,
                              foreign_keys=relig_inherit.religion_parent,
                              backref=backref("split_into"))
    holidays_id = Column(Integer, ForeignKey('Holidays.id'))
    holidays = relationship('relig_holiday_assoc', cascade="all, delete-orphan", backref=backref('celebrated_by'))
    def __repr__(self):
        return self.name
    @staticmethod
    def gui(parent):
        parent.simpleField('name')
        parent.textField('description')
        parent.textField('beliefs')
        parent.textField('practices')
        parent.m2mField('split_from', Religions)
        parent.m2mField('holidays', Holidays)
class Holidays(Base):
    __tablename__ = 'Holidays'
    __tabletype__ = 'front'
    name_singular = 'Holiday'
    #================================#
    id = Column(Integer, Sequence('holiday_id_seq'),primary_key=True)
    name = Column(String(50))
    customs = Column(Text)
    meaning = Column(Text)
    fixed_day = Column(Date)
    moving_day = Column(Text)
    description = Column(Text)
    def __repr__(self):
        return self.name
    @staticmethod
    def gui(parent):
        parent.simpleField('name')
        parent.textField('customs')
        parent.textField('meaning')
        #parent.dateField('fixed_day', caption='Fixed Day')
        parent.timeField('fixed_day', 
                         caption='Day', 
                         time=False, 
                         year=False, 
                         epoch=False,
                         return_type = date)
        #parent.timeField('moving_day', caption='Moving Day (experimental):')
        parent.textField('description')
class Times(Base):
    __tablename__ = 'Times'
    __tabletype__ = 'back'
    name_singular = 'Date'
    #========================#
    id = Column(Integer, Sequence('date_epoch_seq'), primary_key=True)
    date_time = Column(DateTime)
    epoch_id = Column(Integer, ForeignKey('Epochs.id'))
    epoch = relationship('Epochs')
    def __repr__(self):
        
        proc = sys.modules['nTime'].printTime(self.date_time)
        
        e = str(self.epoch)
        if e == 'None':
            e = ''
        return '{} {}'.format(proc, e)
class Epochs(Base):
    __tablename__ = 'Epochs'
    __tabletype__ = 'front'
    name_singular = 'Epoch'
    #======================#
    filter_out = ['id']
    id = Column(Integer, Sequence('epoch_id_seq'), primary_key=True)
    name = Column(String(50))
    name_backward = Column(String(50))
    #TODO: Abbrev
    max_year = Column(Integer)
    goes_forward = Column(Boolean)
    after_id = Column(Integer, ForeignKey('Epochs.id'))
    after = relationship('Epochs')
    #rels = {'after':relig_inherit}
    def __repr__(self):
        return self.name
    @staticmethod
    def gui(parent):
        parent.simpleField('name')
        parent.simpleField('name_backward', caption='Name (backward): ')
        parent.intField('max_year', caption='Maximum Year')
        parent.checkField('goes_forward', caption='Years Ascending', default=True)
        #parent.fkField('after', Epochs, caption='Preceding Epoch')
class Alignments(Base):
    __tablename__ = 'Alignments'
    __tabletype__ = 'static'
    name_singular = 'Alignment'
    filter_out = ['id']
    #==========================#
    id = Column(Integer, Sequence('align_id'), primary_key=True)
    name = Column(String(50))
    def __repr__(self):
        return self.name
    def gui(self, parent):
        raise NotImplementedError
class tech_prereqs(Base):
    __tablename__ = 'tech_prereqs'
    __tabletype__ = 'back'
    __associationchild__ = 'Technology'
    #========================#
    tech_parent = Column(Integer, ForeignKey('Technologies.id'), primary_key=True)
    tech_child = Column(Integer, ForeignKey('Technologies.id'), primary_key=True)
    child = relationship('Technologies', foreign_keys=tech_child, backref=backref('prereqs'))
    def __repr__(self):
        return str(self.child)
class techs_culture_assoc(Base):
    __tablename__ = 'techs_culture_assoc'
    __tabletype__ = 'back'
    #==========================#
    culture = Column(Integer, ForeignKey('Cultures.id'), primary_key=True)
    technology_id = Column(Integer, ForeignKey('Technologies.id'), primary_key=True)
    completion_date_id = Column(Integer, ForeignKey('Times.id'))
    completion_date = relationship('Times')
    child = relationship('Technologies')
    def __repr__(self):
        return str(self.child)
class Technologies(Base):
    __tablename__ = 'Technologies'
    __tabletype__ = 'front'
    name_singular = 'Technology'
    filter_out = ['id']
    #============================#
    id = Column(Integer, Sequence('align_id'), primary_key=True)
    name = Column(String(50))
    description = Column(Text)
    magical_override = Column(Integer)
    rels = {'prerequisites':tech_prereqs}
    prerequisites = relationship('tech_prereqs',
                                cascade="all, delete-orphan",
                                passive_deletes=True,
                                foreign_keys=tech_prereqs.tech_parent,
                                backref=backref('unlocks'))
    def __repr__(self):
        return str(self.name)
    @staticmethod
    def gui(parent):
        parent.simpleField('name')
        parent.textField('description')
        parent.checkField('magical_override', caption='Magic?: ',default=False)
        parent.m2mField('preqrequisites', Technologies)
class Names(Base):
    __tablename__ = 'Names'
    __tabletype__ = 'front'
    name_singular = 'Name'
    allow_edit = True
    NAME_PLURAL = 'Plural Name'
    display_names = {'name_plural':NAME_PLURAL}
    #==========================#
    
    name = Column(String(50), primary_key=True)
    name_plural = Column(String(50))
    meaning = Column(String(50))
    language_id = Column(Integer, ForeignKey('Languages.id'))
    language = relationship('Languages', backref='names')
    gender = Column(String(50))
    
    def __repr__(self):
        if str(self.name) == 'None':
            return ''
        else:
            return str(self.name)
    def __str__(self):
        return self.__repr__()
    @staticmethod
    def gui(parent):
        parent.simpleField('name')
        parent.simpleField('name_plural', caption=Names.NAME_PLURAL + ':')
        parent.fkField('language', Languages)
        parent.simpleField('meaning')
        gen_opt = ['Male','Female','Neutral','Object','Other']
        parent.optionField('gender', gen_opt)
def makeName(name):
    return Names(name=name)
    #language = Column(Integer, ForeignKey('Languages'))
def Drafts(Base):
    __tablename__ = 'Drafts'
    __tabletype__ = 'front'
    name_singular = 'Draft'
    #==========================#
    id = Column(Integer, Sequence('align_id'), primary_key=True)
    name = Column(String(50))
    description = Column(Text)
    
    