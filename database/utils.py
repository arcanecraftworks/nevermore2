#===========================standard library imports==========================#
import sys
import os
from functools import wraps
#============================3rd party imports================================#
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from sqlalchemy.exc import UnboundExecutionError
#================================local imports================================#
import bootstrap
from blockdiag.imagedraw.simplesvg import path
#from cli_tools import printDictOfObject
NT_OVERRIDE = False
if os.name != 'posix' and not NT_OVERRIDE:
    import postgres.postgres as postgres
import sqlite3.sqlite as sqlite3
from db_dict import DBConfig
#==================================Constants==================================#
SCOPE_DEFAULT = True
#=============================================================================

STAR_EXC = 'star queries are not supported for printing. Please specify columns'
        
project_home = None
SAVE_DIR = 'save'
DEFAULT_CORE = 'core08'
CORE_DIRECTORY = os.path.join('core03', 'static tables')
AUTOFLUSH_DEFAULT = False
Base = bootstrap.Base
engine = None
debug = False
SCOPED_SESSION = None
FILTERED_ATTRIB = 'filter_out'
nTables = dict()
mods = dict()
nvmt = dict()
coremap = DEFAULT_CORE
dbm_systems_future = 'SQLite3', 'MySQL','MS SQL Server', 'ODBC'

dbm_systems = [
               'SQLite3 in memory (non-persistent)',
               'PostgreSQL',
               'SQLite3 (persistent)'
               ]
db_choices = ['SQLite3 in memory (non-persistent)',
              'MySQL',
              'PostgreSQL',
              'MS SQL Server',
              'ODBC']
db_ports = {1: 3306, 2:5432}
mem_header = 'sqlite:///:memory:'
mysql_header = 'mysql+mysqldb://'
mysql_create_command = 'create '

sqlite_header = 'sqlite:///'
temp_message = 'None' 

ZED = 'Zed Testing Database'
CLOUD_HOST = 'nevermore-cloud.cdcoffsjzxrc.us-east-1.rds.amazonaws.com'

nvmcloud = {'alias':'Nevermore Cloud','dbms':'PostgreSQL','port':'5432',
            'host':CLOUD_HOST,'offlinemode':'True'}

zed = {'alias':ZED,'dbms':'SQLite in Memory','port':'0',
       'host':':memory:','offlinemode':'True'}

testdb = {'alias':'local'}
coldbs = []
dbconfig = None
CLASS_ERR = "Unable to located class '{}'" 
DB_STRING = 'database.{}'
rollback_override = True
int_boolean = False
INHERIT = 'inherit_condition'
POLY_ON = 'polymorphic_on'
POLY_IDENT = 'polymorphic_identity'
DBMS_DEFAULT = 'PostgreSQL'
TAB = '    '
def stub(*arg, **kw):
    pass
class nvmTable():
    def getFullName(self):
        return '{}.{}'.format(self.mod_path, self.classname)
    def getNameSingular(self):
        if 'name_singular' in self.mClass.__dict__:
            return self.mClass.name_singular
        else:
            return self.name
    def __init__(self, classname=None, mod_path=None, sTable=None, **kw):
        self.classname = classname
        self.sTable = sTable
        self.mod_path = mod_path
        self.fullname = '{}.{}'.format(self.mod_path, self.classname)
        
        for k in kw:
            self.__dict__[k] = kw[k]
        if self.sTable is not None:
            self.name = self.sTable.name
    def __str__(self):
        if 'name' in self.__dict__ and self.name is not None:
            return self.name
        elif self.sTable is not None:
            self.name = self.sTable.name
            return self.name
        else:
            raise AttributeError
    def setTable(self, t):
        assert t is not None
        self.sTable = t
        self.name = self.sTable.name 
    def __repr__(self):
        stri = ''
        for i in self.__dict__:
            stri += '{}: {}\n'.format(i, self.__dict__[i])
        return stri
class S3Obj():
    def __init__(self, filename):
        assert False
        self.dbms = 'SQLite3'
        self.filename = filename
coldbs.append(DBConfig(fixed=True, **nvmcloud))
coldbs.append(DBConfig(fixed=True, **zed))
def delete(session, obj):
    if 'mk_del' in obj.__dict__:
        obj.mk_del = True
    else:
        session.delete(obj)
        session.commit()
def with_session(session):
    def sess_decorator(fn):
        @wraps(fn)
        def go(*args, **kw):
            session.begin(subtransactions=True)
            try:
                kw['session'] = session
                ret = fn(*args, **kw)
                session.commit()
                return ret
            except UnboundExecutionError:
                session.rollback() 
                session.bind(engine)
                go(*args, trip=True)
            except:
                if rollback_override:
                    raise
                else:
                    ow = sys.modules['MessageBox'].onWarn
                    ow('Session Rolled Back')
                    session.rollback()
        return go
    return sess_decorator
def assoc(parent, rel, child, relObj=None):
    assert child is not None
    #from sqlalchemy.exc import IntegrityError
    if relObj is None:
        a = parent.rels[rel]()
    else:
        a = relObj
    
    if str(a.__class__) == str(child.__class__):
        return
    
    try:
        a.child = child
        a.parent = parent
        b = getattr(parent, rel)
        b.append(a)
    except:
        raise
    #except KeyError:
    #    raise
    #except IntegrityError:
    #    printDebug('IntegrityError')
    #    a.child = child
    #    a.parent = parent
    #    b = getattr(parent, rel)
    #    b.append(a)
#def assoc_getObj(parent, rel, child):
#    a = parent.rels[rel]()
#    a.parent = parent
#    a.child = child
#    return a

    
def initTables(new=False):
    """Builds nTable dict for use by the rest of the program."""
    printDebug('initTables', interrupt_override=True)
    mods = loadMods()
    create_all()
    global nTables
    for t in Base.metadata.sorted_tables:
        if t.name == 'nvMaps':
            nTables[t.name] = nvmTable(sTable=t, classname='nvMaps', 
                                       mod_path='bootstrap')
        else:
            tablename = t.name
            classname = getClassName(tablename)
            nTables[t.name] = nvmTable(sTable=t, classname=classname)
    buildNTables(mods)
    #restoreStatic()
def tcLoop(el, **kw):
    printDebug('{}, {}'.format(el, kw))
    for key, val in kw.iteritems:
        if getattr(el, key) == val:
            return el
def tableCheck(nTable, **kw):
    a = process(nTable.mClass, for_each=tcLoop)
    printDebug('tableCheck {}={}'.format(nTable, a))
    if a is not []:
        return True
    else:
        return False
def pnt(el):
    if el.static_load_directory != '':
        sys.modules['file_utils'].restoreModule(el)
        el.static_load_directory = ''
def restoreStatic():

    process('nvMaps', for_each=pnt)
def printDebug(s, **kw):
    sys.modules['cli_tools'].printDebug(s, **kw)
def yn_prompt_loop(*arg, **kw):
    return sys.modules['cli_tools'].yn_prompt_loop(*arg, **kw)
def setProjectHome(s):
    global project_home
    project_home = s
def reflect():
    Base.metadata.reflect()
def create(tables):
    global engine
    Base.metadata.create_all(tables)
def create_all():
    global engine
    Base.metadata.create_all(engine)
def get_tables():
    return Base.metadata.sorted_tables
def get_nvmTables():
    assert False
    return nTables
def get_nvmTable(classname=None, tablename=None):
    if tablename is not None:
        return nTables[tablename]
    elif classname is not None:
        for t in nTables:
            if nTables[t].classname == classname:
                return nTables[t]
    else:
        return None
def getTableObjs():
    return Base.metadata.sorted_tables()
def open_session(autoflush=AUTOFLUSH_DEFAULT, use_scoped=SCOPE_DEFAULT):
#def open_session():
    global engine
    #printDebug('open_session engine={}'.format(engine))
    
    #assert engine is not None
    global SCOPED_SESSION
    #printDebug(SCOPED_SESSION)
    if use_scoped and SCOPED_SESSION is None:
        #printDebug('silica')
        session_fact = sessionmaker(bind=engine, autoflush=autoflush)
        Session = scoped_session(session_fact)
        SCOPED_SESSION = Session
        session = Session()
        return session
    elif SCOPED_SESSION is not None:
        #printDebug('Rain')
        #printDebug(SCOPED_SESSION)
        #SCOPED_SESSION.configure(bind=engine)
        session = SCOPED_SESSION()
        #printDebug('Domon')
        
        return session
    else:
        Session = sessionmaker(bind=engine, autoflush=autoflush)
    session = Session()
    return session
def engine_config(dbms=DBMS_DEFAULT, filename=None, comm_echo=False, **kw):
    #printDebug('engine_config')
    global engine
    
    url = kw.pop('url', None)
    if url is not None:
        #printDebug('Kirito')
        engine = create_engine(url, echo=comm_echo)
        #session = open_session()
        return engine

    #printDebug('Asuna')
    assert dbms is not ''
    
    if dbms == 0 or dbms == '0':
        engine_url = sqlite3.header.format(':memory:')
    elif dbms == 'SQLite3' or filename is not None:
        engine_url = sqlite3.header.format(filename)
    elif dbms == 'PostgreSQL':
        engine_url = postgres.build(**kw)
    else:
        exit('unassigned={}'.format(dbms))
    
    engine = create_engine(engine_url, echo=comm_echo)
    #session = open_session()
    
    return engine
def reset():
    session = SCOPED_SESSION
    session.rollback()
    session.close()
    engine
def getClassName(tablename):  
    for m, map_mod in mods.iteritems():
        for i, follow in map_mod.__dict__.iteritems():
            fc = follow.__class__
            bc = Base.__class__
            if i == tablename and follow.__tablename__ == tablename:
                return i
            elif fc == bc and follow != bootstrap.Base and follow.__tablename__ == tablename:
                return i
    
    #for m in mods:
    #    map_mod = mods[m]
    #    for i in map_mod.__dict__:
    #        follow =  map_mod.__dict__[i]
    #        fc = follow.__class__
    #        bc = Base.__class__
    #        #ft = follow.__tablename__
    #        if i == tablename and follow.__tablename__ == tablename:
    #            return i
#=============================================================================#
    #        elif fc == bc and follow != bootstrap.Base and follow.__tablename__ == tablename:
    #            return i
def nImport(mod_path, mod_name):
    #mods = {}
    fullpath = 'database.{}'.format(mod_path)
    print 'loading {}...'.format(mod_path)
    if not fullpath in sys.modules:
        __import__(fullpath)
    mod = sys.modules[fullpath]
    #printDebug(m)
    
    if mod_name == 'core':
        global coremap
        coremap = str(mod_path)
    return mod
        
    
    #printDebug('nImport mod_path={} mod_name={}'.format(mod_path, mod_name))
    #assert mod_path is not None
    #fullpath = 'database.{}'.format(mod_path)
    #global mods
    #if mod_path in mods:
    #    printDebug('Asuna')
    #    return mods[mod_path]
    #elif fullpath in sys.modules:
    #    printDebug('Kirito')
    #    mods[mod_path] = sys.modules[fullpath]
    #    
    #    printDebug('fullpath={} mod_path={}'.format(fullpath, mod_path))
    #    
    #    
    #    
    #    return sys.modules[fullpath]
    #
    #else:
    #    #printDebug('Suguha')
    #    print 'loading {}...'.format(mod_path)
    #    mod = __import__('database.' + mod_path)
    #    if mod_name == 'core':
    #        mods[mod_name] = mod
    #        global coremap
    #        coremap = str(mod_path)
    #    else:
    #        #printDebug()
    #        mods[mod_path] = mod
    #    printDebug('mod_path={} fullpath={} mod={}'.format(mod_path, fullpath, mod))
    #exit('nImport')
def loadMods():
    mods = {}
    session = open_session()
    #session.bind = engine
    q = session.query(bootstrap.nvMaps) 
    for mod in q:
        #printDebug('{} {}'.format(mod.__class__, mod))
        mods[mod.maps_file] = nImport(mod.maps_file, mod.name)
    return mods
def getClass(classname):
    try:
        o = nTables[classname]
        assert o is not None
        return o.mClass
    except KeyError:
        create_all()
        initTables()
        #try:
        o = nTables[classname]
        #except KeyError:
        #    
        #    printDebug(CLASS_ERR.format(classname), override=True)
        #    assert False
    mod_path = o.mod_path
    
    long_path = DB_STRING.format(mod_path)
    if classname == 'Towns' or 'nvMaps':
        s = DB_STRING.format(DEFAULT_CORE)
        att = sys.modules[s]
        return getattr(att, classname)
    else:
        return getattr(sys.modules[long_path], classname)
def buildNTables(mods):
    #printDebug('getModules')

    #===BOOTSTRAP INIT======#
    bootClass = sys.modules['database.bootstrap'].nvMaps
    assert bootClass is not None
    nTables['nvMaps'].mClass = bootClass
    nTables['nvMaps'].mod_path = 'bootstrap'
    
    #global mods
    global tables
    #printDebug('mods={}'.format(mods))
    
    for path, mod_obj in mods.iteritems():
        __for_each_mod__(path, mod_obj)
    #exit('build')
    
    #for mod_path in mods:
    #    pMod = mods[mod_path]
    #    printDebug('pMod={}'.format(pMod))
    #    if mod_path != 'core':
    #        impMod = getattr(pMod, str(mod_path))
    #    else:
    #        impMod = getattr(pMod, str(coremap))
    #    printDebug('impMod={}'.format(impMod))
    #    
    #    
    #    for classname, mClass in impMod.__dict__.iteritems():
    #        #stri = str(i)
    #        #classname = str(i)
            #mClass = getattr(impMod, classname)
            #if mClass.__class__ == Base.__class__ and mClass != Base:
    #        if classQualify(mClass):
    #            #printDebug('mClass={}'.format(mClass))
    #            table = mClass.__tablename__
    #            nTables[table].classname = classname
    #            if mod_path != 'core':
    #                nTables[table].mod_path = mod_path
    #            if table == 'Towns':
    #                nTables[table].mod_path = coremap
    #            else:
    #                nTables[table].mod_path = coremap
    #            nTables[table].mod_path
    #            nTables[table].mClass = mClass
def __for_each_mod__(path, mod_obj):
    #printDebug('{}: {}'.format(path, mod_obj))
    for classname, mClass in mod_obj.__dict__.iteritems():
        if classQualify(mClass):
            table = mClass.__tablename__
            nTables[table].classname = classname
            nTables[table].modpath = path
            nTables[table].mClass = mClass
def classQualify(mClass):
    #a = True
    #try:
    #    a = '__class__' in mClass.__dict__
    #except AttributeError:
    #    return False
    #return a and mClass.__class__ == Base.__class__ and mClass != Base
    return mClass.__class__ == Base.__class__ and mClass != Base

def print_schema(cols=True):
    for t in Base.metadata.sorted_tables:
        print t.name
        if cols:
            for c in t.c:
                print TAB + c.name
def tablename_from_classname(classname):
    try:
        getTableName(classname)
    except:
        return None
def getTableObj(s):
    a = Base.metadata.sorted_tables
    for el in a:
        if el.name == s:
            return el
def find_columns(nTable):
    return nTable.sTable.c
def getNTable(classname=None, class_=None):
    assert classname is not None or class_ is not None
    
    if classname is not None:
        
        for tablename, nTable in nTables.iteritems():
            tn = str(tablename)
            
            #printDebug('{} "{}"={} "{}"'.format(tn.__class__, tn, classname.__class__, classname))
            
            if classname == tn:
                return nTable
            if classname == nTable.classname:
                return nTable
        #for table in nTables:
        #    if nTables[table].classname == classname:
        #        return nTables[table]
    elif class_ is not None:
        for nTable in nTables.values():
            if class_ == nTable.mClass:
                return nTable
    printDebug('class {} {} not found'.format(class_, classname))
    assert False
#def rowStar(nTable, rowlimit=-1, printTable=False):
#    session = open_session()
#    rows = []
#    for row in session.query(getClass(nTable.classname)):
#        rows.append(row)
#        rowlimit = rowlimit -1
#        if rowlimit == 0:
#            return rows
#    return rows
def selectObj(nTable, query=None, rowlimit=-1, printTable=False, session=None):
    raise NotImplementedError
    #assert False
    #mClass = None
    #res = None
    #if nTable.mClass is None:
    #    mClass = getClass(nTable.classname)
    #else:
    #    mClass = nTable.mClass
    #if query is None:
    #return rowStar(nTable, rowlimit=rowlimit, prinTable=printTable)
    #return res
def getTableName(classname):
    nTable = getNTable(classname)
    return nTable.name
def findObj(nTable, t):
    
    session = open_session()
    pk0 = nTable.sTable.primary_key.columns
    pk = []
    for p in pk0:
        pk.append(getattr(t, p.name))
    try:
        return session.query(nTable.mClass).get(pk)
    except:
        printDebug(nTable)
        mC = nTable.mClass
        if '__mapper_args__' in mC.__dict__ and 'polymorphic_identity' in mC.__mapper_args__:
            printDebug(nTable.mClass.__mapper_args__['polymorphic_identity'])
def polyType(nTable):
    mClass = nTable.mClass
    di = mClass.__dict__
    
    if '__mapper_args__' in di:
        arg = mClass.__mapper_args__
        if INHERIT in arg and POLY_ON in arg:
            assert False
        elif INHERIT in arg:
            host = arg[INHERIT]
            poly_id = arg[POLY_IDENT]
            return {'guest':host, 'ident':poly_id}
        elif POLY_ON in arg:
            guest = arg[POLY_ON]
            return {'host':guest}
        else:
            return None
    return None   
def merge(destination=None, origin=None):
    if destination == None or origin == None:
        assert False
    
    d = origin.__dict__
    for i, di in d.iteritems():
        #di = d[i]
        if i is not '_sa_instance_state':
            at = getattr(origin, i)
            exec('destination.{} = at'.format(i))      
#def listColumn(classname, attname, mClass=None, session=None):
#    assert False
#    if session is None:
#        session = open_session()
#    if mClass is None:
#        mClass = getClass(classname)
#    return process(classname, session, mClass=mClass, query=session.query(getattr(mClass, attname)))


def get(mClass, id):
    mClass = parseString(mClass)
    sess = open_session()
    return sess.query(mClass).get(id)
def parseString(mClass):
    if mClass.__class__ == ''.__class__:
        return nTables[mClass].mClass
    else:
        return mClass
def process(mClass, query=None, for_each=None, 
            limit=100, column=None, allow_none=True, 
            id=None, **kw):
    
    if 'override' in kw:
        assert False
    mClass = parseString(mClass)
    session = open_session()
    if query is None:
        query = session.query(mClass)
    
    query = makeSort(query, column=column)
    query = query.limit(limit)
    
    if id is not None:
        return query.get(id)
    
    rows = []
    for r in query:
        b = 'mk_del' in r.__dict__ and r.mk_del==True
        if not b and for_each is None:
            rows.append(r)
        elif for_each is not None:
            el = for_each(r, **kw)
            if (el is None and allow_none) or el is not None:
                rows.append(el)
                
    return rows    
def passColumn(col, nTable):
    mClass = nTable.mClass
    if FILTERED_ATTRIB in mClass.__dict__:
        if col.name in getattr(mClass, FILTERED_ATTRIB):
            return False
        else:
            return True
    else:
        return True
def textNumeral(n):
    str_num = str(n)
    len_name = len(str_num)
    last_digit = str_num[len_name-1]
    
    if 12 < n and n < 20:
        numeral = 'th'
    elif last_digit == '1':
        numeral = 'st'
    elif last_digit == '2':
        numeral = 'nd'
    elif last_digit == '3':
        numeral = 'rd'
    else:
        numeral = 'th'
    
    return str_num + numeral
    
def makeSort(query, column=None):
    """Takes the sort info from the mClass definition
    and attempts to make a sorted query of it.
    """
    mClass = query._primary_entity.expr
    
    if column is None and 'sort' in mClass.__dict__:
        sortlist = mClass.sort
    elif column is None:
        sortlist = [column]
    else:
        return query
    for i in sortlist:
        if i is not None:
            col = getattr(mClass, i)
            query = query.order_by(col)
    
    
    return query
def __get_cols__(nTable, x_nTable):
    """gets a list of columns adjusted for a polymorphic host/guest
    schema"""
    
    columns=nTable.sTable.columns
        
    columns2 = []
        
    for col in columns:
        blank_col = True
        blank_col2 = True
        if x_nTable is not None:
            blank_col = not passColumn(col, x_nTable)
        else:
            blank_col = False
        blank_col2 = not passColumn(col, nTable)
        if not (blank_col or blank_col2):
            columns2.append(col)
    return columns2
def parseColumns(nTable=None, x_nTable=None):
        """finds the list of headers for the given nTable
        """

        assert nTable is not None or x_nTable is not None
        
        columns = __get_cols__(nTable, x_nTable)
        #printDebug(columns)
        
        mClass = nTable.mClass
        if 'display_names' in mClass.__dict__:
            dn = nTable.mClass.display_names
        else:
            dn = dict()
        headers = []
        for i in columns:
            key = i.name
            ifk = i.foreign_keys
            
            for o in i.foreign_keys:
                remote_table = o.column.table
                name = remote_table.name
                mClass = nTables[name].mClass
                
                di = mClass.__dict__

            
            if key in dn:
                headers.append([key, dn[key]])
            elif len(ifk) != 0 and 'name_singular' in di:
                headers.append([key, mClass.name_singular])   
            else:
                capt = makeCaption(key, use_colon=False)
                headers.append([key, capt])
        #printDebug(headers[0][1])
        return headers
def searchVal(nTable, row_obj, at_name):
    """Takes a foreign key entry in a table and returns the actual object
    instead of just it's id.
    """
    
    attrib = getattr(row_obj, at_name)
    fk = nTable.sTable.foreign_keys
    for i in fk:
        local_name = i.parent.description
        if local_name == at_name:
            pk = i.column.table.primary_key.columns
                        
            for i in pk:
                remote_column = i
            remote_name = remote_column.key
            remote_table = remote_column.table
            remote_class = nTables[remote_table.name].mClass
            value = getattr(row_obj, local_name)
            res = findVal(nTable.mClass, row_obj, remote_class, 
                                   value, remote_name)     
            if res is not None:
                return res
    return attrib
def findVal(host_class, row_obj, remote_class, value, remote_name):
    di = host_class.__dict__
                            
    for i, e in di.iteritems():
        findLevelNo = nTables['nvMaps'].mClass.name
        if e.__class__ == findLevelNo.__class__:
            a = getattr(row_obj, i)
                
            if a.__class__ == remote_class:
                b = getattr(a, remote_name)    
                if b == value:
                    return a
def staticForeignKeyName(nTable, id_name):
    mClass = nTable.mClass
    if 'foreign_key_map' in mClass.__dict__:
        return mClass.foreign_key_map[id_name]
    else:
        return id_name.rstrip('_id')
    #fk = nTable.sTable.foreign_keys
    #for i in fk:
    #    local_name = i.parent.description
    #    if local_name == id_name:
    #        pk = i.column.table.primary_key.columns
    #        
    #        for i in pk:
    #            remote_column = i
    #        printDebug('remote_column={}'.format(remote_column))
    #        remote_name = remote_column.key
    #        printDebug('remote_name={}'.format(remote_name))
    #        remote_table = remote_column.table
    #        printDebug('remote_table={}'.format(remote_table))
    #        remote_class = nTables[remote_table.name].mClass
    #        #exit()
    #        
    #        d = remote_class._sa_class_manager.local_attrs['first_names'].__dict__
    #        
    #        for i, di in d.iteritems():
    #            printDebug('    {}: {}'.format(i, di))
    #        
    #        
    #        exit()


def makeCaption(s, use_colon=True):
    a = s.split('_')
    b = ''
    for i in a:
        b = b + i.capitalize() + ' '
    c = b.strip(' ')
    if use_colon:
        return '{}: '.format(c)
    else:
        return c

if __name__ == '__main__':
    print 'utils check complete'