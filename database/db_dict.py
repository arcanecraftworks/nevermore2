#===========================standard library imports==========================#
from Tkinter import StringVar
#============================3rd party imports================================#
#================================local imports================================#
import utils
#==================================Constants==================================#
#=============================================================================#

#dbm_systems = [
#               'SQLite3 in memory (non-persistent)',
#               'PostgreSQL',
#               'SQLite3 (persistent)'
#               ]
#
#ATTRIB = [
#          'username'
#          'dbms'
#          'port'
#          'hostname'
#          'alias'
#          'schema'
#          'password'
#          ''
#          ]
DEF_SET = ['dbms','port','host','schema','username',
           'offlinemode','alias','password','filename']




class DBConfig():
    def __init__(self, **kw):
        self.id = 0
        dicti = kw.pop('dicti', None)
        if dicti is not None:
            self.set(**dicti)
        self.set(**kw)  
    def set(self, **kw):
        for key, val in kw.iteritems():
            self.__dict__[key] = val
    def __str__(self):
        if 'dbms' in self.__dict__:
            db = self.dbms
        else:
            db = ''
        if 'filename' in self.__dict__ and self.filename != '':
            s = self.filename
        elif 'alias' in self.__dict__:
            s = self.alias        
        else:
            try:
                s = '{0}@{1} ({2})'.format(self.schema, self.host, db)
            except:
                s = '<>'
        if self.id == 0 or self.id == '0':
            return s
        else:
            return '{}_{}'.format(s, self.id)
    def __repr__(self):
        stri = ''
        
        for i in self.__dict__:
            stri += '{}: {}\n'.format(i, self.__dict__[i])
        return stri
class StringVarConfig(DBConfig):
    def __init__(self, string_root, **kw):
        if 'filename' not in kw:
            kw['filename'] = StringVar()
        self.id = 0
        for i in DEF_SET:
            self.__dict__[i] = StringVar(string_root)
        self.set(**kw)
    def set(self, **kw):
        """sets the values of the given dictionary into
        the local stringvars"""
        d = {}
        o = kw.pop('obj', None)
        self.id = kw.pop('id', 0)
        if o is not None:
            return self.set(**o.__dict__)
        for key in self.__dict__.keys():
        #for key, val in self.__dict__.iteritems():
            if key in kw:
                self.__dict__[key].set(kw[key])
            elif key != 'id':
                self.__dict__[key].set('')
            d[key] = True
        self.setByKwargs(d, kw)
    def setByKwargs(self, a, kw):
        """sets the values of the given dictionary into
        the local stringvars, this time adding in new ones if necessary."""
        for key, val in kw.iteritems():
            if key in a:
                pass
            elif not key in self.__dict__:
                self.__dict__[key] = StringVar()
                #self.__dict__[key].set(val)
            if key != id:
                self.__dict__[key].set(val)
    def build(self, isDetailed=True):
        """builds a normal DBConfig object from the existing Tk StringVar values
        """
        if not isDetailed:
            return DBConfig(filename=self.filename.get())
        kw = {}
        for key, val in self.__dict__.iteritems():
            if key != 'id' and key != 'filename':
                kw[key] = val.get()
        return DBConfig(**kw)
    def __str__(self):
        return str(self.build())

if __name__ == '__main__':
    pass