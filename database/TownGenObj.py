#===========================standard library imports==========================#
import sys
import os
from random import randint
import math
#============================3rd party imports================================#
from sqlalchemy.exc import IntegrityError
#================================local imports================================#
import utils
import cli_utils
from _sqlite3 import Row
#==================================Constants==================================#
AUTOROLL = True
ROLL_ITERATE = False
DESC_CONCAT = False
COMMIT = True
#=============================================================================#
powercenters = {13:'Conventional',18: 'Nonstandard', 70: 'Magical'}
poweralign = {35: 1, 39: 2, 41: 3, 61: 4, 63: 5, 64: 6, 90: 7,98: 8, 100: 9}
GUARDS_DIVISOR = 100
MILITIA_DIVISOR = 20
#=============================================================================#
officeholders = {60: ('Warrior',1), 80: ('Fighter', 2), 100: ('Fighter',1)}

FILEROOT = os.path.dirname(__file__)
DESCRIPT_FILE = 'dd35descrip.txt'
DUNGEON_ROOT = os.path.join(FILEROOT, 'dungeonmaster')
DESCRIPT_PATH = os.path.join(DUNGEON_ROOT,DESCRIPT_FILE)
EXPERTS_FILE = 'experts.txt'
EXPERTS_PATH = os.path.join(DUNGEON_ROOT, EXPERTS_FILE)
DUNGEON_STATIC_SAVE = os.path.join(DUNGEON_ROOT,'static tables')
MAPS_FILE = 'dungeon_core02'
def number(num):
    digit = num % 10
    #print 'digit=' + str(digit)
    if digit == 1 and num != 11:
        return '{}st'.format(num)
    if digit == 2 and num != 12:
        return '{}nd'.format(num)
    if digit == 3 and num != 13:
        return '{}rd'.format(num)
    else:
        return '{}th'.format(num)
def dungeon_core():
    return sys.modules[MAPS_FILE]
def protected_open(filename):
    return open(filename, 'r').read().split('\n')

dd35_traits = protected_open(DESCRIPT_PATH)
dd35_experts = protected_open(EXPERTS_PATH)

def rollstring(s):
    s0, s1 = s.split('d')
    #s0 = int(st[0])
    #s1 = int(st[1])
    return roll(int(s1), repeat=int(s0), auto=True)
def getAddStr(add):
    if add == 0:
        return ''
    elif add > 0:
        return '+{}'.format(add)
    else:
        return str(add)
def roll(number, repeat=1, default=60, add=0, auto=AUTOROLL):
    
    add_str = getAddStr(add)
    
    pn = sys.modules['cli_tools'].prompt_loop_null
    if auto:
        return randint(1, number) + add
    
    if number == 100:
        num_str = '%'
    else:
        num_str = str(number)
    if repeat == 1:
        rep_str = ''
    else:
        rep_sr = str(repeat)
        
    prompt = '{}d{}{}->'.format(rep_str, num_str, add_str)
    return prompt_loop(prompt, 'Please enter an integer,', None, input_type='int')
def prompt_loop(*arg, **kw):
    return sys.modules['cli_tools'].prompt_loop(*arg, **kw)
def lookup(dicti, die_result=10):
    max_ = 0
    min_ = None
    max_val = None
    #utils.printDebug('die result={}'.format(die_result))
    
    for d in dicti:
        #utils.printDebug( '{}: {}'.format(d, dicti[d]))
        if d > max_:
            max_ = d
        if d < min and min != -1 or min is None:
            min_ = d
        if d == -1:
            max_val = dicti[d]
    
    #exit()
    
    stri = dicti[max_]
    for i in range(max_):
        switch = max_-i
        if switch in dicti:
            stri = dicti[switch]
        #print '{} - {}'.format(switch, stri)
        if switch == die_result:
            return stri
def printDebug(s):
    sys.modules['cli_tools'].printDebug(s)
class TownGenObj():
    __class__ = 'override'
    def __init__(self, autoroll=AUTOROLL):
        self.session = utils.open_session()
        self.auto = autoroll
        self.char_buffer = []
        self.desc = []
        self.npc_classes = []
    def getCityInfo(self, roll=0, pop=0):
        assert roll != 0 or pop != 0
        
        #printDebug('getCityInfo={}'.format(d))
        """grabs the appropriate cityinfo object based on the roll"""
        rows = utils.process('cityinfo')
        cur = 0
        prev = 0
        POPULATION_MIN = 20
        d_lower = POPULATION_MIN
        
        #for row in rows:
        #    for key, val in row.__dict__.iteritems():
        #        printDebug('{}: {}'.format(key, val))
        #    exit()
    
        if roll != 0:
            sel = 'd_upper'
            d = roll
        elif pop != 0:
            sel = 'pop_upper'
            d = pop
        
        
        for i in range(len(rows)):
            row = rows[i]
            sel2 = getattr(row, sel)
            if sel2 <= d:
                cur = i
                prev = i-1
    
        if prev <= 0:
            prev = 0
            self.pop_lower = POPULATION_MIN
        else:
            prev_o = rows[prev]
            self.pop_lower = prev_o.pop_upper-1
        try:
            return rows[cur]
        except IndexError:
            utils.printDebug('static tables not initialized.')
    def getPopulation(self):
        """rolls a d% and finds a number based on that."""
        e = roll(100, auto=self.auto)
        pop_upper = self.cityinfo.pop_upper
        diff = pop_upper - self.pop_lower
        ratio = diff/100.0
        rate = ratio * e
        return int(math.ceil(self.pop_lower + rate))
    def getPowerCenter(self, modifier):
        """rolls a d20 to determine the power center.
        """
        assert modifier is not None
        #num = 0
        #for i in range(multiplier):
        num = roll(20, default=10, add=modifier, auto=self.auto)
        return lookup(powercenters, num)
    def printDescrip(self, pref, data):
        d = '{}: {}'.format(pref, data)
        print d
        self.desc.append(d)
    def inscribe(self):
        """records the results of the previous rolls onto the
        existing description in the town object
        """
        if DESC_CONCAT:
            stri = ''
        else:
            stri = self.town.description
        for line in self.desc:
            stri += line + '\n'
        self.town.description = stri
    def __acc_roll_qualify__(self, dClass, r):
        pc_class = dClass.pc_class
        return r != 0 and pc_class == 1 or (pc_class == 0 and r != 0)
    def classAssoc(self, parent, child, level):
        """Associates a character with it's assigned class
        """
        cca = utils.getClass('char_class_assoc')
        a = cca(level=level)
        try:
            a.child = child
            parent.classes.append(a)
        except KeyError:
            utils.printDebug('unable to associate {} and {}'.format(parent, child))
            print parent.rels 
    def printChars(self, qty, level, name):
        print '{}x {} level {}'.format(qty, number(level), name) 
    def addClass(self, dClass, num, level):
        """Etches characters with appropriate descriptors
        """
        if num != 0:
            self.printChars(num, level, dClass.name)
        for m in range(num):
            o = utils.getClass('Characters')()
            o.char_type = 'NPC'
            o.description = dd35_traits[randint(0, len(dd35_traits)-1)]
            self.classAssoc(o, dClass, level)
            #printDebug(o.dClass)
            
            self.char_buffer.append(o)
    def addClassCascade(self, dClass, rollResult, multiplier):
        """Adds the members of a given class to the community
        """
        if self.__acc_roll_qualify__(dClass, rollResult):
            new_roll = int(math.floor(rollResult/2))
            self.addClass(dClass, multiplier, rollResult)
            self.addClassCascade(dClass, new_roll, multiplier * 2)
    def getNPCf(self, dClass):
        highest_level = rollstring(dClass.town_char_level_roll)
        self.addClassCascade(dClass, highest_level, 1)
        if not dClass.pc_class:
            self.npc_classes.append(dClass)
    def makeClasses(self, multiplier):
        Classes = utils.getClass('Classes')
        for i in range(multiplier):
            self.npc_classes = utils.process(Classes, for_each=self.getNPCf,
                                             allow_none=False)
    def parseRemainder(self):
        #TODO: parseRemainder
        remainder = dict()
        pop_remainder = self.population-len(self.char_buffer)
        #printDebug('self.population={} self.char_buffer={}'.format(self.population, 
        #                                                           len(self.char_buffer)))
        #printDebug('pop_remainder={}'.format(pop_remainder))
        last_remainder = pop_remainder
        list_ = ['Commoner','Warrior','Expert']
        dict_ = {'Commoner':0.91, 'Warrior':0.05, 'Expert':0.03,'Aristocrat':0.5}
        
        for i in list_:
            v = int(math.floor(pop_remainder * dict_[i]))
            #printDebug('remainder[{}] = {}'.format(i, v))
            remainder[i] = v
            last_remainder = last_remainder - v
        na = int(math.floor(last_remainder * dict_['Aristocrat']))
        remainder['Aristocrat'] = na
        remainder['Adept'] = last_remainder - na
        #exit()
        return remainder
    def firstLevelNPCClasses(self):
        comm_class = None
        for dClass in self.npc_classes:
            multiplier = self.remainder[dClass.name]
            
            if dClass.name != 'Commoner' and multiplier >= -1:
                self.addClass(dClass, multiplier, 1)
            else:
                self.comm_class = dClass
    def getCity(self):
        prompt = 'which city is this?'
        cit_nTable = utils.nTables['Cities']
        return cli_utils.selectID(cit_nTable, default_value=1, prompt=prompt, override=True)
    def autoSize(self):
        d = roll(100, auto=self.auto)        
    def intverify(self, el):
        """verifies that the given parameter is an integer greater
        than zero, and if not, throws a tryAgain exception"""
        
        try:
            i = int(el)
            if i > 0:
                return i
            else:
                cli_utils.cli_tool_wrapper('tryAgain')
        except:
            cli_utils.cli_tool_wrapper('tryAgain')
    def manualSize(self):
        f = 'prompt_loop'
        prompt = 'population:'
        err = 'Please enter an integer greater than zero.'
        return cli_utils.cli_tool_wrapper(f, prompt, err, self.intverify)
        #pop = prompt_automatic('population-> ')
        #printDebug(pop)
    def getMixType(self):
        menu = sys.modules['cli_tools'].menu
        mixes = 'Isolated','Mixed','Integrated'
        return menu(mixes, exit_=False, default_value=0)        
    def highestLevel(self, classname, town=None, offset=1):
        if town is None:
            town=self.town
        ch = self.charactersbyLevelandClass(classname, town)
        if offset == 1:
            ch1 = ch.first()
            if ch1 is None:
                cli_utils.printDebug('No {} found in {}'.format(classname, town))
            else:
                assert ch1[0] is not None
                return ch1[0]
        else:
            i = 0
            for r in ch:
                row=r[0]
                if i == offset:
                    return row
                i = i + 1
    def proc_classes(self, el):
        return el[0]
    def charactersbyLevelandClass(self, classname, hometown, run=False):
        char_class_assoc = utils.getClass('char_class_assoc')
        Classes = utils.getClass('Classes')
        Characters = utils.getClass('Characters')
        #printDebug('charactersbyLevelandClass')
        #q = self.session.query(Classes).filter_by(dClass=classname)
        
        q = self.session.query(Characters, char_class_assoc, Classes)
        q = q.filter(Characters.id==char_class_assoc.character_id)
        q = q.filter(char_class_assoc.class_id==Classes.id)
        q = q.filter(Classes.name==classname)
        q = q.filter(Characters.hometown_id==hometown.id) 
        if run:
            return utils.process('Characters', query=q, for_each=self.proc_classes)
        else:
            return q
        #utils.process('char_class_assoc', query=q, for_each=self.proc_classes)
        #exit('CharactersbyLevelandClass')
    def getDomSpec(self, town):
        prompt='Which species is dominant in {}?'.format(town)
        a = cli_utils.selectID('Species', prompt=prompt)
        self.printDescrip('Dominant Species', a)
        return a
    def getDClass(self, classname):
        classes = utils.getClass('Classes')
        return self.session.query(classes).filter_by(name=classname).first()
    def popMixByMixTypeAndDomSpec(self, mix_type, dom_spec):
        #printDebug('mix_type={}'.format(mix_type))
        #printDebug('dom_spec={}'.format(dom_spec))
        popmix = utils.getClass('popmix')
        q = self.session.query(popmix).filter_by(mix_type=mix_type)
        return q.filter_by(dom_spec=dom_spec)
    def pop(self, size, town, spec, culture=None):
        pop = utils.getClass('Populations')(size=size)
        pop.location = town
        pop.species = spec
        pop.culture = culture
        self.session.add(pop)
    def getSpeciesObject(self, s):
        species = utils.getClass('Species')
        a = self.session.query(species).filter_by(name=s).first()
        assert a is not None
        return a
    def roll_spec(self):
        r = roll(100)
        cumulative = 0
        last = None
        for spec, percent in self.percents.iteritems():
            total_number = cumulative + percent
            if cumulative <= r and r <= total_number:
                return self.getSpeciesObject(spec)
                #return spec
            cumulative = cumulative + percent
            last = spec
        return last
    def roll_gen(self):
        bin = roll(2, auto=True)
        if bin == 1:
            return 'Female'
        elif bin == 2:
            return 'Male'
        else:
            exit('Zero')
    def for_each_name(self, el, gender, language):
        if str(el) == 'Olyvar':
            printDebug('{} {}'.format(el.name, el))
        #printDebug('{} {} {}'.format(el, el.gender, el.language))
    def half_elf(self):
        printDebug('half elf')
        dom_spec = str(self.dom_spec)
        if dom_spec == 'Human':
            return 'Elvish'
        elif dom_spec == 'Elf':
            return 'Common'
        else:
            r = roll(2, auto=True)
            if r == 1:
                return 'Elvish'
            else:
                return 'Common'
    def getLanguage(self, species=None, language_str=None):
        language_dict = {'Human':'Common','Elf':'Elvish',
                         'Half-Elf':None,'Dwarf':'Dwarvish',
                         'Gnome':'Gnome','Halfling':'Halfling',
                         'Half-orc':'Orcish','Other':None}
        assert species is not None or language_str is not None
        str_spec = str(species)
        ld = language_dict[str_spec]
        
        if language_str is None and str_spec == 'Half-Elf':
            language_str = self.half_elf()
        elif language_str is None and str_spec == 'Other':
            return None
        else:
            language_str = ld
        
        languages = utils.getClass('Languages')
        #printDebug(language_str)
        return self.session.query(languages).filter_by(name=language_str).first()
        #printDebug('{} {}'.format(a.__class__, a))
        #exit()
    def rand_name(self, species, gender):
        
        
        names = utils.getClass('Names')
        q = self.session.query(names).filter_by(gender=gender)
        a = q.filter_by(language=self.getLanguage(species=species)).all()
        try:
            return a[roll(len(a), auto=True)-1]
        except ValueError:
            return None
            #printDebug(species)
            #printDebug(gender)
            #printDebug(len(a))
            #exit('rand_name error')
    def setCharAttributes(self):
        for ch in self.char_buffer:
            #Species
            species = self.roll_spec()
            assert species.__class__ != str
            utils.assoc(ch, 'species', species)
            gender = self.roll_gen()
            ch.gender = gender
            ch.hometown = self.town
            try:
                ch.first_name = self.rand_name(species, gender)
            except IntegrityError:
                exit('Time to clear out the characters again')
    def setPop(self, num, mix_type, dom_spec):
        assert mix_type is not None
        
        resdict = dict()
        
        rows = utils.process('Species')
        other_percent = 0
        
        #a = q.all()
        #assert len(a) != 0
        
        for popmix in self.popMixByMixTypeAndDomSpec(mix_type, dom_spec):
            #printDebug(popmix)
            species = self.session.query(utils.getClass('Species')).get(popmix.sub_spec.id)
            rows.remove(popmix.sub_spec)
            if str(popmix.sub_spec) == 'Other':
                other_percent = popmix.percentage
            else:
                resdict[str(species)] = popmix.percentage
                size = num * (popmix.percentage / 100)
                spec = popmix.sub_spec
                self.pop(size, self.town, spec)
        for species in rows:
            per = float(other_percent)/float(len(rows))
            resdict[species] = per
            self.pop(int(num * per/100), self.town, species)
        self.percents = resdict
    def __for_each_power_center(self, modifier):
        powercenter = self.getPowerCenter(modifier)
        pca = lookup(poweralign, roll(100, auto=self.auto))
        alignment = utils.get('Alignments', pca)
        #self.printDescrip('Power Center:{} Alignment',alignment)
        
        d = 'Powercenter: {} - {}'.format(powercenter, alignment)
        print d
        self.desc.append(d)
        
        return alignment, powercenter
    def powerCenters(self, multiplier, modifier):
        a = []
        for m in range(multiplier):
            a.append(self.__for_each_power_center(modifier))
        return a
    def dumpBuffer(self):
        if COMMIT:
            self.session.add_all(self.char_buffer)
            self.session.commit()
            self.session.close()
            self.session = utils.open_session()
            self.char_buffer = []
    def experts(self):
        i = 0
        #printDebug(self.unoccupied('Expert'))
        for ch in self.unoccupied('Expert', ):
            if i < len(dd35_experts):
                ch.occupation = dd35_experts[i]
                i = i + 1
                self.printDescrip(ch.occupation, ch)
            else:
                return
    def barkeep(self):
        barkeep = self.highestLevel('Commoner')
        self.printDescrip('Barkeep', barkeep)
        if self.numMilitia > 0:
            barkeep.occupation = 'Barkeep, Militia'
            self.numMilitia = self.numMilitia - 1
    def guardsAndMilitia(self):
        for classname in ['Warrior','Fighter']:
            self.setGuards(classname)
    def unoccupied2(self, el):
        return el[0]
    def unoccupied(self, classname):
        q = self.charactersbyLevelandClass(classname, self.town)
        q = q.filter_by(occupation=None)
        return utils.process('Classes', query=q, for_each=self.unoccupied2)
    def setGuards(self, classname):
        for ch in self.unoccupied(classname):
            if self.numguards > 0:
                ch.occupation = 'Guard'
                self.numguards = self.numguards - 1
            elif self.numMilitia > 0:
                ch.occupation = 'Militia'
                self.numguards = self.numguards - 1
    def prompt(self):
        #print 'Du'
        #utils.process('Alignments',for_each=self.printDebug)
        """Build the character that populate a city, according to the SRD3.5
        Dungeon Master's Guide p138.
        """
        print "==============D&D 3.5 Dungeon Master's Guide Town Generator =========="
        self.population = None
        if cli_utils.yn_prompt_loop('Do you want to specify exact population?'):
            d = self.manualSize()
            self.population = d
            self.cityinfo = self.getCityInfo(pop=d)
        else:
            d = self.autoSize()
            self.cityinfo = self.getCityInfo(roll=d)
        
        self.printDescrip('town size', self.cityinfo.town_size)
        
        modifier = self.cityinfo.power_mod
        multiplier = self.cityinfo.power_mult
        
        if self.population is None:
            self.population = self.getPopulation()
            self.printDescrip('Population',self.population)
        else:
            self.desc.append('Population: {}'.format(self.population))
        
        self.powercenters = self.powerCenters(multiplier, modifier)
        
        self.numguards = int(math.floor(self.population/GUARDS_DIVISOR))
        self.numMilitia = int(math.floor(self.population/MILITIA_DIVISOR))
        self.printDescrip('Number of guards', self.numguards)
        self.printDescrip('Number of Militia', self.numMilitia)
        
        npc_classes = self.makeClasses(multiplier)
        
        self.remainder = self.parseRemainder()
        self.firstLevelNPCClasses()
        
        self.town = self.getCity()
        self.session.add(self.town)
        
        dom_spec = self.getDomSpec(self.town)
        self.dom_spec = dom_spec
        mix_type = self.getMixType()
        
        self.setPop(self.remainder['Commoner'], mix_type, dom_spec)
        
        print 'rendered NPCs: {}'.format(len(self.char_buffer))
        
        self.setCharAttributes()
        
        #for i in self.char_buffer:
        #    printDebug(i.species)
        
        self.session.add_all(self.char_buffer)
        self.session.commit()
        
        mayor = self.highestLevel('Aristocrat')
        
        if mayor is None:
            print 'This thorp is too small to have a mayor'
        else:
            self.printDescrip('Mayor', mayor)
            mayor.occupation = 'Mayor'
        
        c_class, c_th = lookup(officeholders, roll(100, auto=self.auto))
        constable = self.highestLevel(c_class, offset=c_th)
        assert constable is not None
        self.printDescrip('Constable', constable)
        constable.occupation = 'Constable'
        self.session.commit()
        
        self.guardsAndMilitia()
        
        self.barkeep()
        
        self.experts()
        self.inscribe()
        self.dumpBuffer()
        print '============Town generator successful end.==========='
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        