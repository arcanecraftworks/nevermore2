#===========================standard library imports==========================#
import math
import sys
import os
#============================3rd party imports================================#
from sqlalchemy import Sequence, Column, Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import relationship, backref, relation
from random import randint

FILEROOT = os.path.dirname(__file__)

#================================local imports================================#
#import cli_tools as main
import core07 as core
import utils as utils
import cli_utils

from utils import printDebug, yn_prompt_loop
from sqlalchemy.exc import IntegrityError
from TownGenObj import TownGenObj
#==================================Constants==================================#
Base = core.Base

char_buffer = []
AUTO_ROLL = True

#READ_IN = ['powercenters', 'cityinfo', 'Classes','Species','Cultures','popmix']
DESCRIPT_FILE = 'dd35descrip.txt'
DUNGEON_ROOT = os.path.join(FILEROOT, 'dungeonmaster')
DESCRIPT_PATH = os.path.join(DUNGEON_ROOT,DESCRIPT_FILE)
EXPERTS_FILE = 'experts.txt'
EXPERTS_PATH = os.path.join(DUNGEON_ROOT, EXPERTS_FILE)
DUNGEON_STATIC_SAVE = os.path.join(DUNGEON_ROOT,'static tables')

GUARDS_DIVISOR = 100
MILITIA_DIVISOR = 20
officeholders = {60: ('Warrior',1), 80: ('Fighter', 2), 100: ('Fighter',1)}
dd35_isolated_mix = {96: 'human',2: 'halfling', 1: 'elf'}
dd35_mixed = {79: 'human', 9: 'halfling', 5:'elf', 3: 'dwarf', 2: 'gnome', 
              1: 'half-elf', 1: 'half-orc'}
dd35_integrated_mix = {37: 'human', 20: 'halfling', 18: 'elf', 10: 'dwarf', 
                       7:'gnome', 5: 'half-elf', 3: 'half-orc'}
poweralign = {35: 1, 39: 2, 41: 3, 61: 4, 63: 5, 64: 6, 90: 7,98: 8, 100: 9}
powercenters = {13:'Conventional',18: 'Nonstandard', 70: 'Magical'}

SCOPE_DEFAULT = True
#=============================================================================#
def cascade(parent):
    CascadeMenu = sys.modules['cascadeMenu'].CascadeMenu
    CascadeMenu('Timeline',parent, menu)

def menu():
    return {
            'Test1':stub
            }
def stub():
    pass
#def protected_open(fn):
#        def open(*args, **kw):
#            try:
#                open(args[0], 'r').read().split('\n')
#            except:
#                return []
#        return open
#@protected_open
#def open_p(filename):
#    return 

def protected_open(filename):
    return open(filename, 'r').read().split('\n')


dd35_traits = protected_open(DESCRIPT_PATH)
dd35_experts = protected_open(EXPERTS_PATH)

def getTables():
    return Base.metadata.tables
# Tables 5-2 Random Town Generation && Power Centers  
class cityinfo(Base):
    __tablename__ = 'cityinfo'
    __tabletype__ = 'dd35'
    #===========================#
    id = Column(Integer, Sequence('dd35_5_2_seq'), primary_key=True)
    d_upper = Column(Integer)
    town_size = Column(String(20))
    pop_upper = Column(Integer)
    gp_limit = Column(Integer)
    power_mod = Column(Integer)
    power_mult = Column(Integer)
    level_mod = Column(Integer)
    def __repr__(self):
        return self.town_size
class char_class_assoc(Base):
    __tablename__ = 'char_class_assoc'
    __tabletype__ = 'back'
    __associationchild__ = 'Classes'
    #=====================#
    character_id = Column(Integer, ForeignKey('Characters.id'), primary_key=True)
    parent = relationship('Characters', backref='classes')
    class_id = Column(Integer, ForeignKey('Classes.id'), primary_key=True)
    child = relationship('Classes', backref=backref('members'))
    level = Column(Integer)
    def __repr__(self):
        return '{} is a(n) {}'.format(self.parent, self.child)
class Castles(core.Locations):
    __tablename__ = 'Castles'
    __tabletype__ = 'front'
    id = Column(None, ForeignKey('Locations.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity':'Castle', 'inherit_condition': 
                       (id==core.Locations.id)}
    name_singular = 'Castle'
    _idref = relation(core.Locations, foreign_keys=id, 
                      primaryjoin=id==core.Locations.id)
    power_center = Column(String(50))
    pc_align_id = Column(Integer, ForeignKey('Alignments.id'))
    pc_align = relationship('Alignments', foreign_keys=pc_align_id)
    
    def __repr__(self):
        return str(self.name)
class Classes(Base):
    __tablename__ = 'Classes'
    __tabletype__ = 'dd35'
    #============================#
    id = Column(Integer, Sequence('class_seq'), primary_key=True)
    name = Column(String(50))
    pc_class = Column(Boolean)
    town_char_level_roll = Column(String(50))
    rels = {'members':char_class_assoc,}
    #members = relationship("char_class_assoc",
    #                             cascade="all, delete-orphan",
    #                             passive_deletes=True)
    
    def __repr__(self):
        return str(self.name)
    def __str__(self):
        return str(self.name)
PMS = '{} community dominated by {} is {}% {}'
class popmix(Base):
    __tablename__ = 'popmix'
    __tabletype__ = 'dd35'
    #=====================#
    id = Column(Integer, Sequence('pm_seq'), primary_key=True)
    mix_type = Column(String(50))
    percentage = Column(Integer)
    sub_spec_id = Column(Integer, ForeignKey('Species.id'))
    sub_spec = relationship('Species', foreign_keys=sub_spec_id)
    dom_spec_id = Column(Integer, ForeignKey('Species.id'))
    dom_spec = relationship('Species', foreign_keys=dom_spec_id)
    
    
    def __repr__(self):
        return '{} {} {} {} {}'.format(str(self.id).ljust(2), 
                                    self.mix_type.ljust(10),
                                    str(self.percentage).ljust(2),
                                    self.sub_spec_id,
                                    self.dom_spec_id)
    def __str__(self):
        return PMS.format(self.mix_type,self.dom_spec,
                          self.percentage, self.sub_spec)
class Char_ext(core.Characters):
    __tablename__='Characters'
    __tabletype__='dd35'
    __table_args__ = {'extend_existing':True}
    name_singular = 'Character'
    #============================#
    #level = Column(Integer)
    rels = {'dClass':char_class_assoc}
    dClass = relationship('char_class_assoc',
                              cascade="all, delete-orphan",
                              passive_deletes=True,
                              foreign_keys=char_class_assoc.character_id,
                              backref='class_assoc')
def stub_passthrough(i):
    return int(i)
def rollstring(s):
    st = s.split('d')
    s0 = int(st[0])
    s1 = int(st[1])
    total = 0
    for i in range(s0):
        total += roll(s1, auto=True)
    return total
def roll(number, default=60, auto=AUTO_ROLL):
    if auto:
        return randint(1, number)
    elif number == 100:
        prompt = 'd%->'
    else:
        prompt = 'd{}->'.format(number)
    
    
    return sys.modules['cli'].prompt_loop(prompt,'Please enter an integer',
                                          stub_passthrough)   
def lookup(dicti, die_result=10):
    max = 0
    min = None
    max_val = None
    for d in dicti:
        #print '{}: {}'.format(d, dicti[d])
        if d > max:
            max = d
        if d < min and min != -1 or min is None:
            min = d
        if d == -1:
            max_val = dicti[d]
    stri = dicti[max]
    for i in range(max):
        switch = max-i
        if switch in dicti:
            stri = dicti[switch]
        #print '{} - {}'.format(switch, stri)
        if switch == die_result:
            return stri
def number(num):
    digit = num % 10
    #print 'digit=' + str(digit)
    if digit == 1 and num != 11:
        return '{}st'.format(num)
    if digit == 2 and num != 12:
        return '{}nd'.format(num)
    if digit == 3 and num != 13:
        return '{}rd'.format(num)
    else:
        return '{}th'.format(num)
def qByNonDescrip(session):
    Towns = utils.getClass('Towns')
    Locations = utils.getClass('Locations')
    q = session.query(Towns.id, Towns.name)
    q = q.group_by(Locations.id, Towns.id)
    return q.having(Towns.description==None)
def descript(town, des):
    stri = ''
    for descrip in des:
        stri += descrip + '\n'
    town.description = stri
def isTownLeft():
    session = utils.open_session()
    #Towns = core.Towns
    u = qByNonDescrip(session)
    o = u.first()
    session.close()
    print o
    
    if o is None:
        return False
    else:
        return True
def generate_all(autoroll=AUTO_ROLL):
    while isTownLeft():
        town_generate()

def town_generate(autoroll=AUTO_ROLL):
    session = utils.open_session(autoflush=False)
    prompt = 'Do you want the computer to roll automatically for you?'
    t = TownGenObj(autoroll=yn_prompt_loop(prompt))
    t.prompt()
    
    #renderChars(autoroll, session)
    #TODO: make neighborhoods/wards/boroughs for larger cities
    session.close()
def futils():
    return sys.modules['file_utils']

#cur = 0
#prev = 0
def proc(el, select=0):
    return el
    #printDebug(el)
    #if el.d_upper <= select:
    #    cur = el
    #    prev = i-1
    
def add_spec(session):
    #TODO: macro2
    a = ['Halfling','Half-Elf','Human','Dwarf','Elf','Gnome','Half-orc','Other']
    b = ['Halflings','Half-Elves','Humans','Dwarves','Elves','Gnomes','Half-orcs','Others']
    MEDIUM = 'Medium'
    size = {'Halfling':'Small',
            'Other':None,
            'Half-Elf':'Medium',
            'Human':MEDIUM,
            'Dwarf':MEDIUM,
            'Elf':MEDIUM,
            'Gnome':'Small',
            'Half-orc':MEDIUM,
            }
    Species = utils.getClass('Species')
    
    
    for i in range(len(a)):
        ai = a[i]
        bi = a[i]
        size_ = size[ai]
        o = Species(name=ai, name_plural=bi, size_class=size_)
        session.add(o)



def macro():
    raise NotImplementedError
def parseLang(el):
    sp = el.split('(')
    if len(sp) == 1:
        return el, ''
    else:
        return sp[0].rstrip(' '), sp[1].rstrip(')')
        #name = sp[0].rstrip(' ')
        #description = sp[1].rstrip(')')
        #printDebug('[{}, {}]'.format(name, description))
        #printDebug(sp)
        #exit()
#def setLanguages():
#    set = protected_open('C:\Users\\adminbp\workspace\Nevermore2\Nevermore\database\D&D Languages.txt')
#    sess = utils.open_session()
#    class_ = utils.nTables['Languages'].mClass
#    for el in set:
#        name, description = parseLang(el)
#        sess.add(class_(name=name, description=description))
#        #printDebug(el)
#    sess.commit()
#    sess.close()
def setCharAttributes(char_buffer, percents, town):
    printDebug(percents)
    printDebug(town)
    exit()
    
    for ch in char_buffer:
        species = roll_spec(percents)
        gender = gen()
        printDebug(rand_name(species, gender))
    return
    
    for ch in char_buffer:
        #Species
        species = roll_spec(percents)
        utils.assoc(ch, 'species', species)
        gender = gen()
        ch.gender = gender
        ch.hometown = town
        try:
            ch.first_name = rand_name(species, gender)
        except IntegrityError:
            exit('Time to clear out the characters again')
def renderChars(autoroll, session):
    """Build the characters that populate a city, according
    to the Dungeon Master's Guide.
    """
    global char_buffer
    d = roll(100, auto=autoroll)
    rows = utils.process('cityinfo', select=d)
    cur = 0
    prev = 0
    
    for i in range(len(rows)):
        row = rows[i]
        if row.d_upper <= d:
            cur = i
            prev = i-1
    
    if prev <= 0:
        prev = 0
        pop_lower = 1
    else:
        prev_o = rows[prev]
        pop_lower = prev_o.pop_upper-1
    try:
        cur_o = rows[cur]
    except IndexError:
        utils.printDebug('static tables not initialized.')
    
    
    des = []
    descrip = 'town size: ' + cur_o.town_size
    print descrip
    des.append(descrip)
    modifier = cur_o.power_mod
    multiplier = cur_o.power_mult
    
    e = roll(100, auto=autoroll)
    
    
    pop_upper = cur_o.pop_upper
    diff = pop_upper - pop_lower
    ratio = diff / 100.0
    pop = int(math.ceil(pop_lower + (ratio * e)))
    print 'population: ' + str(pop)
    
    assert multiplier is not None
    
    
    num = 0
    for i in range(multiplier):
        num += roll(20, default=10, auto=autoroll)
    powercenter = lookup(powercenters, num)
    descrip = 'Power Center = ' + powercenter
    print descrip
    des.append(descrip)
    
    pc_align = lookup(poweralign, roll(100, auto=autoroll))
   
    alignment = utils.get('Alignments', pc_align)
    
    descrip = 'Power Center Alignment = ' + str(alignment)
    print descrip
    des.append(descrip)
    
    numguards = int(math.floor(pop/GUARDS_DIVISOR))
    print 'Number of Guards: ' + str(numguards)
    
    numMilitia = int(math.floor(pop/MILITIA_DIVISOR))
    print 'Number of Militia: ' + str(numMilitia)
    
    npc_classes = []
    for dClass in session.query(Classes):
        highest_level = rollstring(dClass.town_char_level_roll)
        addClassCascade(dClass, highest_level, 1, session)
        if dClass.pc_class == False:
            npc_classes.append(dClass)
    assert len(npc_classes) > 0
    

    
    remainder = dict()
    pop_remainder = pop - len(char_buffer)
    num_commoners = int(math.floor(pop_remainder * 0.91))
    remainder['Commoner'] = num_commoners
    num_warriors = int(math.floor(pop_remainder * 0.05))
    remainder['Warrior'] = num_warriors
    num_experts = int(math.floor(pop_remainder * 0.03))
    remainder['Expert'] = num_experts
    last_remainder = pop_remainder - num_commoners - num_warriors - num_experts
    num_aristocrats = int(math.floor(last_remainder * 0.5))
    remainder['Aristocrat'] = num_aristocrats
    num_adepts = last_remainder - num_aristocrats
    remainder['Adept'] = num_adepts
    
    #first level NPC classes
    comm_class = None
    for dClass in npc_classes:
        multiplier = remainder[dClass.name]
        
        if dClass.name != 'Commoner' and multiplier >= -1:
            addClass(dClass, multiplier, 1, session)
        else:
            comm_class = dClass
    
    percents = []

    prompt='which city is this?'
    #q = qByNonDescrip(session)
    cit_nTable = utils.nTables['Cities']
    town = cli_utils.selectID(cit_nTable, default_value=1, prompt=prompt , override=True)
    descript(town, des)
    
    menu = sys.modules['cli_tools'].menu
    mixes = 'Isolated','Mixed','Integrated'
    mix_type = menu(mixes, exit_=False, default_value=0)
    #======================================================#
    Species = utils.getClass('Species')
    q = session.query(Species.id.label('id'), Species.name.label('name')).order_by(Species.id)
    dom_spec = cli_utils.selectID('Species', default_value=1, prompt='Which species is dominant in {}?'.format(town))
    
    setPop(comm_class, remainder['Commoner'], town, mix_type, dom_spec, percents)
    
    len_char = len(char_buffer)
    print 'rendered NPCs: {}'.format(len_char)
    
    setCharAttributes(char_buffer, percents, town)
    
    #=======================================================#
    menu = sys.modules['cli_tools'].menu
    mixes = 'Isolated','Mixed','Integrated'
    mix_type = menu(mixes, exit_=False, default_value=0)
    #Dumping initial definitions to the database because I'm lazy. 
    session.add_all(char_buffer)
    session.commit()

    mayor = highestLevel('Aristocrat', town, session)
    #TODO: ==================================================#
    if mayor is None:
        print 'this Thorp is too small to have a mayor'
    else:
        mayor.occupation = 'Mayor'
        print 'Mayor: ', mayor
    
    
    const1 = lookup(officeholders, roll(100, auto=autoroll))
    constable = highestLevel(const1[0], town, session, offset=const1[1])
    assert constable is not None
    
    constable.occupation = 'Constable'
    print 'Constable: ', constable
    # Guards and militia
    warriors = charactersbyLevelAndClass('Warrior', town, session).filter(core.Characters.occupation==None)
    for r in warriors:
        ch = r[0]
        if numguards > 0:
            ch.occupation = 'Guard'
            numguards = numguards -1
        if numguards == 0 and numMilitia > 0:
            ch.occupation = 'Militia'
            numMilitia = numMilitia -1
    fighters = charactersbyLevelAndClass('Fighter', town, session).filter(core.Characters.occupation==None)
    for r in fighters:
        ch = r[0]
        if numguards > 0:
            ch.occupation = 'Guard'
            numguards = numguards -1
        if numguards == 0 and numMilitia > 0:
            ch.occupation = 'Militia'
            numMilitia = numMilitia -1
    
    barkeep = highestLevel('Commoner', town, session)
    print 'Barkeep: ', barkeep
    if numMilitia > 0:
        barkeep.occupation = 'Barkeep, Militia'
        numMilitia = numMilitia -1
    else:
        barkeep.occupation = 'Barkeep'
    
    i = 0   
    experts = charactersbyLevelAndClass('Expert', town, session).filter(core.Characters.occupation==None)
    for exp in experts:
        ch = exp[0]
        if i < len(dd35_experts):
            ch.occupation = dd35_experts[i]
            i = i + 1
        else:
            ch.occupation = dd35_experts[randint(0, len(dd35_experts)-1)]
        print '{}: {}'.format(ch.occupation, ch)
        
    #TODO: experts
    #experts = charactersbyLevelAndClass('Expert', session).filter(core.Characters.occupation==None)
    dumpBuffer(session)
    session.close()
def dumpBuffer(session):
    global char_buffer
    session.add_all(char_buffer)
    session.commit()
    session.close()
    session = utils.open_session()
    char_buffer = []
def highestLevel(classname, town, session, offset=1):
    
    ch = charactersbyLevelAndClass(classname, town, session)
    #print ch.all()
    if offset == 1:
        ch1 = ch.first()
        if ch1 is None:
            utils.printDebug('No {} found in {}.'.format(classname, town))
        else:
            assert ch1[0] is not None
            return ch1[0]
    else:
        i = 0
        for r in ch:
            row=r[0]
            if i == offset:
                return row
            i = i + 1
def charactersbyLevelAndClass(classname, hometown, session):
    Characters = utils.getClass('Characters')
    q = session.query(Characters, char_class_assoc, Classes).\
                                    order_by(char_class_assoc.level.desc()).\
                                    filter(Characters.id==char_class_assoc.character_id).\
                                    filter(char_class_assoc.class_id==Classes.id).\
                                    filter(Classes.name==classname).\
                                    filter(Characters.hometown_id==hometown.id)
    return q
def roll_spec(percents):
    r = roll(100)
    cum = 0
    last = None
    for a in percents:
        num = a[0]
        tot_num = cum + num
        if cum <= r and r <= tot_num:
            return a[1]
        cum = cum + num
        last = a[1]
    return last
def pop(size, town, spec, culture, session):
    pop = core.Populations(size=size)
    pop.location = town
    pop.species = spec
    pop.culture = culture
    session.add(pop)
def PopMixByMixTypeAndDomSpec(session, mix_type, dom_spec):
    """Queries the popmix table of the database, filtered by
    the current mix type and dominant species.
    """
def getSubSpecies(session, sub_spec_id):
    session.query(utils.getClass('Species')).get(sub_spec_id)
def setPop(sub_spec, num, town, mix_type, dom_spec, resdict):
    printDebug('sub spec= {} {}'.format(sub_spec.__class__, sub_spec))
    exit()
    
    rows = utils.process('Species')
    other_percent = 0
    session = utils.open_session()
    
    for i in PopMixByMixTypeAndDomSpec(session, mix_type, dom_spec):
        #speci 
        
        culture = session.query(utils.getClass('Cultures')).get(i.sub_spec.id)
        assert culture is not None
        rows.remove(i.sub_spec)
        if str(i.sub_spec) == 'Other':
            other_percent = i.percentage
        else:
            resdict.append([i.percentage, i.sub_spec])
            size = num * (i.percentage / 100)
            spec = i.sub_spec
            pop(size, town, spec, culture, session)
    for row in rows:
        culture = session.query(utils.getClass('Cultures')).get(row.id)
        per = float(other_percent)/float(len(rows))
        resdict.append([per, row])
        div = per/100
        res = num * div
        size = int(res)
        pop(size, town, row, culture, session)


def printChars(qty, level, name):
    print '{}x {} level {}'.format(qty, number(level), name)
def addClass(dClass, num, level, session):
    #utils.printDebug('Add class!')
    global char_buffer
    printChars(num, level, dClass.name)
    for m in range(num):
        n = core.Characters()
        #n.first_name='NPC'
        n.char_type='NPC'
        n.description=dd35_traits[randint(0, len(dd35_traits)-1)]
        classAssoc(n, dClass, level)
        char_buffer.append(n)
def addClassCascade(dClass, rollResult, multiplier, session):
    if rollResult != 0 and dClass.pc_class == 1 or (dClass.pc_class == 0 and rollResult != 1):    
        addClass(dClass, multiplier, rollResult, session)
        addClassCascade(dClass, int(math.floor(rollResult/2)), multiplier * 2, session)
def classAssoc(parent, child, level):
    a = char_class_assoc(level=level)
    try:
        a.child = child
        parent.classes.append(a)
    except KeyError:
        utils.printDebug('unable to associate {} and {}'.format(parent, child))
        print parent.rels
def query_dict(mClass, key_by='id', session=None):
    if session is None:
        session = utils.open_session()
    specs = dict()
    for spec in session.query(mClass):
        specs[str(getattr(spec, key_by))] = spec
    return specs
def rand_name(culture, gender):
    rows = []
    assert culture is not None
    session = utils.open_session()
    st = ['Horse']
    
    if not str(culture) in st:
         printDebug('{} {}'.format(culture.__class__, culture))
         exit()
    else:
        printDebug(culture)
        return '<>'
    
    q = session.query(utils.getClass('Names')).filter_by(gender=gender, culture=culture)
    for u in q:
        rows.append(u)
    assert len(rows) != 0
    min = 0
    max = len(rows)-1
    res = randint(min, max)
    return rows[res]

def gen():
    bin = roll(2, auto=True)
    if bin == 1:
        return 'Female'
    elif bin == 2:
        return 'Male'
    else:
        exit('Zero')
def getNPCName(session):
    rows = []
    return session.query(utils.getClass('Names')).get('NPC')
#print('test1')
#sys.modules['Nevermore'].menu_hook.append(cascade)
        