#===========================standard library imports==========================#
from datetime import datetime
#============================3rd party imports================================#
#from sqlalchemy import desc, asc
#================================local imports================================#
import utils
from utils import printDebug
from EpicTime import EpicTimeLine
#==================================Constants==================================#
epics = None
def setEpic():
    global epics
    epics = EpicTimeLine()

class Filter():
    pass

def classQ(classname):
    session = utils.open_session()
    Scenes = utils.getClass(classname)
    return session.query(Scenes)

def scenesByEpoch(epoch, existing=None):
    Times = utils.getClass('Times')
    
    if existing is None:
        q = classQ('Scenes')
    else:
        q = existing
    return q.filter(Times.epoch==epoch)
def getCharacterByFirstName(name_str):
    name = classQ('Names').get(name_str)
    return classQ('Characters').filter_by(first_name=name).first()
def scenesByCharAppearance(min_, max_, *characters):
    #TODO: restrict by date
    """returns a list of scenes in which ALL the provided characters appear
    """
    assert len(characters) > 0
    #printDebug('begin')
    
    session = utils.open_session()
    Scenes = utils.getClass('Scenes')
    Characters = utils.getClass('Characters')
    Sca = utils.getClass('scene_char_assoc')
    
    q = session.query(Scenes, Sca, Characters)
    q = q.filter(Sca.scene_id==Scenes.id)
    q = q.filter(Sca.character_id==Characters.id)
    
    res = []
    
    for i in characters:
        r = q.filter(Characters.id==i.id)
        one = []
        for s in r:
            one.append(s[0])
            
        res.append(one)
    return __andmerge__(*res)
#def makeHolidayScenes(*arg):
    
def __andmerge__(first, *arg):
    res = []
    for i in first:
        for j in arg:
            if i in j and i not in res:
                res.append(i)        
    return res
def characterBirthdays(epochs, min_, max_, *characters):
    Scene = utils.getClass('Scenes')
    list_ = []
    for i in characters:
        if i.birthdate is not None:
            #printDebug(i.birthdate)
            list_.extend(__makeScenes__(epochs, i, min_, max_))
    #printDebug(list_)
    return list_
def __makeScenes__(epochs, character, min_, max_):
    
    epic_date = epics.makeEpicTime(character.birthdate)
    
    #date_obj = character.birthdate.collapseEpochs(epochs)
    #min_obj = min_.collapseEpochs(epochs)
    #max_obj = max_.collapseEpochs(epochs)
    
    date = character.birthdate.date_time
    day = date.day
    month = date.month
    
    if min_.date_time.year < date.year:
        min_.datetime.year = date.year
    #exit('makeScene')
    list_ = for_each_year(epochs, min_, max_, month, day, __procBirthdays__, 
                  character=character,
                  born_year=epic_date.year)
    
    return list_
    
    
def __procBirthdays__(month, day, year, epoch, character=None, born_year=None, **kw):
    
    Time = utils.getClass('Times')
    Scene = utils.getClass('Scenes')
    #try:
    dt = datetime(day=day, month=month, year=year)
    #except:
    #    printDebug('month={}'.format(month))
    time = Time(date_time=dt, epoch=epoch)
    epic_year = epics.makeEpicTime(time).year
    #try:
    year_diff = epic_year - born_year
    #except:
    #    printDebug('epic_year={} born_year={}'.format(epic_year.__class__, born_year.__class__))
    bday = birthdate(character, year_diff)
    return Scene(title=bday, time=time)

def birthdate(character, year_diff):
    BIRTHDAY_TEMPLATE = "{}{} {} birthday"
    
    name = str(character)
    len_name = len(name)
    if name[len_name-1] == 's':
        s = "'"
        #tmp = "{}' {}{} birthday"
    else:
        s = "'s"
        #tmp = "{}'s {}{} birthday"
        
    
    
    
    
    numeral = utils.textNumeral(year_diff)
    
    
    return BIRTHDAY_TEMPLATE.format(name, s, numeral)
    
def for_each_year(epochs, min_, max_, month, day, f, allow_none=False, **kw):
        #printDebug('{} {}'.format(min_, max_))
        min2 = min_
        max2 = max_
        ls = []
        
        year = min_.date_time.year
        epoch = min_.epoch
        if min2.date_time.month < month:
            year = year + 1
        while year <= max2.date_time.year:
            r = f(month, day, year, epoch, **kw)
            if r is not None or allow_none:
                ls.append(r)
            year, epoch = rollEpochs(year, epoch, epochs)
        return ls

#def rollEpoch():
def rollEpochs(year, epoch, epochs_list):
        y2 = year +1
        i = epoch.id
        if y2 <= epoch.max_year or epoch.max_year == -1:
            return y2, epoch
        else:
            for e in epochs_list:
                if e.id == i + 1:
                    return 1, e
        printDebug(year)
        printDebug(i)
        exit()
#def firstSceneChronologically():
#    Scenes = utils.getClass('Scenes')
#    Epochs = utils.getClass('Epochs')
#    Times = utils.getClass('Times')
#    
#    q = classQ('Times')
#    q.order_by(asc(Times.id.asc()))
#    
#    
#    for i in q:
#        printDebug('{}'.format(i))
#    
    
def lastSceneChronologically():
    pass
