#===========================standard library imports==========================#
import sys
#============================3rd party imports================================#
#================================local imports================================#
import utils
#==================================Constants==================================#
SCOPE_DEFAULT = True
#=============================================================================#
#STAR_EXC = 'star queries are not supported for printing. Please specify columns'
COL_LIMIT = 75
SEPARATOR = ' | '
NEWLINE_REP = {'\n':' '}
NULL_OPT = ' \n'
HIDE_EMPTY_COLUMNS = True
OLD_WAY = False
DISPLAY_LIMIT = -1
def yn_prompt_loop(*arg, **kw):
    """wrapper for cli_tools.yn_prompt_loop to make it easier to find
    from this module level.
    """
    return sys.modules['cli_tools'].yn_prompt_loop(*arg, **kw)
def cli_tool_wrapper(f, *arg, **kw):
    """wrapper for an arbitrary function in cli_tools to make it easier
    to find from this module level."""
    return getattr(sys.modules['cli_tools'], f)(*arg, **kw)
def printNTable(nTable, query=None, **kw):
    if nTable.__class__ == ''.__class__:
        nTable = utils.nTables[nTable]
    if nTable.__class__ != utils.nvmTable().__class__:
        nTable = utils.getNTable(class_=nTable)
    session = utils.open_session()
    if query is None:
        query = session.query(nTable.mClass)
    
    r = PrintQueryObject(query, obj_mode=True, nTable=nTable, 
                        **kw).print_query()
    if r == []:
        pto('No results to show.',kw['tab'])
def calc_col_widths(nTable, i, max_len, rows, q, previews):
    printDebug('i={} {}'.format(i.__class__, i))
    
    
    
    int_ = 0
    if i.__class__ == int_.__class__:
        i2 = q.column_descriptions[i]
        len1 = len(i2['name'])
        obj = False
    else:
        len1 = len(i)
        obj = True
    
    
    if not i in max_len or len1 > max_len[i]:
        max_len[i] = len1
            
    max_temp = 0
    zero = True
    for j in rows:
        
        max_temp = max_len[i]
        if obj:
            s = str(utils.searchVal(nTable, j, i))
        else:
            s = str(j[i])
        if len(s) > 0 and s not in NULL_OPT:
            zero = False
        if len(s) > COL_LIMIT:
            previews[s] = substring(s, COL_LIMIT)
            return COL_LIMIT
        elif len(s) > max_temp:
            max_temp = len(s)
            max_len[i] = max_temp
    
    if zero:
        return 0
    else:
        return max_temp
def printDebug(*args, **kw):
    sys.modules['cli_tools'].printDebug(*args, **kw)
def make_str(j, i, obj_mode=False):
    exit(i.__class__)
    
    if obj_mode:
        return str(getattr(j, i))
    else:
        try:
            return 
        except:
            raise
def makeHeaders(cols, max_len, q):
    #TODO: repair
    stri = ""
    for i in range(cols):
        i2 = q.column_descriptions[i]
        header = i2['name']
        if i == cols-1:
            stri = stri + header.ljust(max_len[i])
        else:
            stri = stri + header.ljust(max_len[i]) + SEPARATOR
    return stri
def findDisplayName(el, nTable):
    exit('findDisplayName')
def makeHeaders2(headers, max_len):
    assert max_len.__class__ == dict
    
    stri = ''
    for i in range(len(headers)):
        el, header = headers[i]
        if i == len(headers)-1:
            stri = stri + header.ljust(max_len[header])
        else:
            a = max_len[header]
            if a == 0 and HIDE_EMPTY_COLUMNS:
                pass
            elif a is None:
                stri += header
            else:
                b = header.ljust(a)
                stri += '{}{}'.format(b, SEPARATOR)
    return stri
def makeBorder(stri):
    BORDER_CHAR = "=" 
    t_len = len(stri)-1
    return ''.ljust(t_len, BORDER_CHAR)
def printRows(rows, cols, max_len, short=False):
    
    #printDebug('row={}'.format(rows))
    #printDebug('cols={}'.format(cols))
    printDebug('max_len={}'.format(max_len.__class__), interrupt_override=True)
    
    for i in max_len:
        printDebug(i)
        
    
    stri = ""
    for i in rows:
        for j in range(cols):
            p = str(i[j])
            stri += cutoff(p, max_len[j])
            
            if j != cols-1:
                stri += ' | '
        print stri
        stri = ""
def scrub(s, comp=NEWLINE_REP):
    if s is None:
        return s
    for i in comp:
        j = comp[i]
        s = str(s)
        s = s.replace(i, j)
    return s
def cutoff(c, width):
    return substring(c, COL_LIMIT).ljust(width)
#def printRowsObj(rows, headers, max_len, nTable, limit=-1):
#    assert False
#    if limit == -1:
#        lim = None
#    else:
#        lim = 0
#    for i in rows:
#        __print_one__(i, headers, max_len, nTable)
#        #exit('printRowsObj')
#        if lim is not None:
#            lim = lim + 1
#            if lim == limit:
#                print '{} more results'.format(len(rows)-lim)
#                return
def __print_one__(i, headers, max_len, nTable, short=True):
    assert False
    stri = ''
    len_h = len(headers)
    for j in range(len_h):
        el, header = headers[j]
        p = utils.searchVal(nTable, i, el)
        c = scrub(p)
        if max_len[header] == 0 and HIDE_EMPTY_COLUMNS:
            pass
        elif p is None or p == 'None':
            stri += ' '.ljust(max_len[header])
        else:
            stri += cutoff(c, max_len[header])
        if j != len_h-1 and max_len[header] != 0:
            stri += SEPARATOR
    print stri
class ColumnObject():
    """Column Object for collecting the info neccessary
    for the 
    
    """
    def __init__(self, nTable, header, attribute_name=None, rows=[]):
        self.nTable = nTable
        self.header = header
        self.rows = rows
        assert attribute_name is not None
        self.attribute_name = attribute_name
        
        self.setWidth()
    def comp(self, prev, len_x):
        if prev < len_x and len_x < COL_LIMIT:
            return len_x
        elif COL_LIMIT <= len_x:
            return COL_LIMIT
        else:
            return prev
    def getVal(self, row):
        return str(utils.searchVal(self.nTable, row, self.attribute_name))
    def setWidth(self):
        """Determine how wide the column should be.
        """
        max_temp = len(self.header)
        zero = True
        for row in self.rows:
            
            s = self.getVal(row)
                
            if len(s) > 0 and s not in NULL_OPT:
                zero = False
            
            if len(s) > COL_LIMIT:
                self.width = COL_LIMIT
                return
            elif len(s) > max_temp:
                max_temp = len(s)
        if zero:
            self.width = 0
        else:
            self.width = max_temp
    def getHeader(self):
        if self.width != 0:
            return self.header.ljust(self.width)
class ColumnResult(ColumnObject):
    def __init__(self, nTable, header, attribute_name=None, rows=[], index=0):
        self.index = index
        assert index == 0
        ColumnObject.__init__(self, nTable, header, attribute_name, rows)
    def getVal(self, row):
        try:
            return row[self.index]
        except IndexError:
            printDebug(row)
            printDebug('index={}'.format(self.index))
            exit()
class RowName(ColumnObject):
    def __init__(self, nTable, *arg, **kw):
        self.nTable = nTable
        self.header = nTable.sTable.name
        
class PrintQueryObject():
    """Object designed to store info required for printing nicely
    """
    def __init__(self, query, obj_mode=False, nTable=None, 
                 limit=-1, hide=False, column=None, tab=0):
        self.query = query
        self.obj_mode = obj_mode
        self.nTable = nTable
        self.limit = limit
        self.hide = hide
        self.column = column
        self.tab = tab
    def findHeader(self):
        for at_name, header in utils.parseColumns(self.nTable):
            if header == self.column:
                return at_name
    def print_query(self):
        rows = self.query.all()
        if self.hide:
            return rows
        if len(rows) == 0:
            return []
        self.column_list = []
        if self.obj_mode:
            for at_name, header in utils.parseColumns(self.nTable):
                #printDebug('at_name={}, header={}'.format(at_name, header))
                o = ColumnObject(self.nTable, header, attribute_name=at_name,
                                 rows=rows)
                self.column_list.append(o)
            override = False
        else:
            o = ResultObjectList(self.nTable, self.column)
            self.column_list.append(o)
            rows = o.rows
            override = True
        
        stri = self.makeHeaders2()
        pto(stri, self.tab)
        pto(makeBorder(stri), self.tab)
        self.printRows(rows, override=override)
    #def print_query(self):
    #    if not OLD_WAY:
    #        return self.print_query2()
    #    
    #    rows = self.query.all()
    #    if self.hide:
    #        return rows
    #    if len(rows) == 0:
    #        return []
    #    previews = dict()
    #    cols = len(self.query.column_descriptions)
    #    if self.obj_mode:
    #        self.headers = utils.parseColumns(self.nTable)
    #        max_len = self.maxLenObj(rows, previews)
    #        stri = self.makeHeaders(max_len)
    #    else:
    #        max_len = maxLen(cols, rows, self.query, previews, self.nTable)
    #        
    #        stri = makeHeaders(cols, max_len, self.query)
    #    print stri
    #    cross_border = makeBorder(stri)
    #    print cross_border
    #    if self.obj_mode:
    #        self.printRows(rows, max_len)
    #        return rows
    #    else:
    #        #printDebug('catch')
    #        printRows(rows, cols, max_len, short=True)
    #        #self.printRows(rows, max_len, short=True)
    #        return dict_result(self.query, rows)
    def maxLenObj(self, rows, previews):
        HASH_CHOICE = 1
    
        max_len = dict()
    
        for i in self.headers:
            a = calc_col_widths(self.nTable, i[0], max_len, 
                                rows, self.query, previews)
            assert a is not None
            max_len[i[HASH_CHOICE]] = a   
        return max_len
    def makeHeaders2(self):
        stri = ''
        for column in self.column_list:
            hd = column.getHeader()
            if hd is not None:
                stri += hd + SEPARATOR
        return stri
    #def makeHeaders(self, max_len):
    #    
    #    assert max_len.__class__ == dict
    #    headers = self.headers
    #    stri = ''
    #    for i in range(len(headers)):
    #        el, header = headers[i]
    #        if i == len(headers)-1:
    #            stri = stri + header.ljust(max_len[header])
    #        else:
    #            a = max_len[header]
    #            if a == 0 and HIDE_EMPTY_COLUMNS:
    #                pass
    #            elif a is None:
    #                stri += header
    #            else:
    #                b = header.ljust(a)
    #                stri += '{}{}'.format(b, SEPARATOR)
    #        #elif :
    #        #    pass
    #    return stri
    #def printRows(self, rows, max_len, short=False):
    def printRows(self, rows, override=False, short=False):
        
        if self.limit == -1:
            lim = None
        else:
            lim = 0
        for row in rows:
            self.__print_one__(row, override=override)
            if lim is not None:
                lim = lim + 1
                if lim == self.limit:
                    print '{} more results'.format(len(rows)-lim)
                    return
    def __print_one__(self, row, override=False):
        stri = getTab(self.tab)
        for column in self.column_list:
            #p = utils.searchVal(self.nTable, row, column.attribute_name)
            p = column.getVal(row)
            
            
            c = scrub(p)
            if column.width == 0 and HIDE_EMPTY_COLUMNS:
                pass
            elif p is None or p == 'None':
                stri += ' '.ljust(column.width) + SEPARATOR
            elif not override:
                stri += cutoff(c, column.width) + SEPARATOR
            else:
                stri += c.ljust(column.width) + SEPARATOR
        print stri
        #printDebug('catch')
        #exit('print one 2') 
    #def __print_one__(self, i, headers, max_len, nTable, short=False):
    #    #TODO: remove
    #    assert False
    #    #printDebug('catch')
    #    stri = ''
    #    len_h = len(headers)
    #    for j in range(len_h):
    #        el, header = headers[j]
    #        p = utils.searchVal(nTable, i, el)
    #        c = scrub(p)
    #        if max_len[header] == 0 and HIDE_EMPTY_COLUMNS:
    #            pass
    #        elif p is None or p == 'None':
    #            stri += ' '.ljust(max_len[header])
    #        else:
    #            if short:
    #                printDebug('Kirito')
    #            stri += cutoff(c, max_len[header])
    #        if j != len_h-1 and max_len[header] != 0:
    #            stri += SEPARATOR
    #    print stri
    def __repr__(self):
        stri = ''
        for key, val in self.__dict__.iteritems():
            stri += '{}\n'.format(val)
def pto(s, i):
    """Dips into the quantum foam to find the print tab function
    """
    return sys.modules['cli_tools'].printTabbedOverride(s, i)
def getTab(i):
    """Dips into the quantum foam to build the necessary tab spacing
    """
    return sys.modules['cli_tools'].getTab(i)
def findHeader(nTable, at_name):
    for at, header in utils.parseColumns(nTable):
        if equiv(at, at_name):
            return header
class ResultObjectList(ColumnObject):
    size_limit = COL_LIMIT * 2
    def __init__(self, nTable, attribute_name, header=None, display_string=True):
        if header is None:
            header = findHeader(nTable, attribute_name)
        self.display_string = display_string
        self.nTable = nTable
        self.attribute_name = attribute_name
        self.header = self.nTable.sTable.name
        self.runQuery()
        self.setWidth()
    def runQuery(self):
        sess = utils.open_session()
        self.rows = utils.process(self.nTable.mClass)
    def setWidth(self):
        """self cannot inherit from the parent class because
        ResultObjectLists have to compute column width values
        separately.
        """
        max_temp_1 = len(self.header)
        max_temp_2 = len(self.attribute_name)
        for row in self.rows:
            t = ColumnObject.getVal(self, row)
            max_temp_1 = self.comp(max_temp_1, len(str(row)))
            max_temp_2 = self.comp(max_temp_2, len(t))
        self.str_width = max_temp_1
        self.col_width = max_temp_2
        self.width = self.str_width + len(SEPARATOR) + self.col_width
    def getVal(self, row):
        if self.display_string:
            stri = self.getStr(row)
            col = self.getCol(row)
            s = '{}{}{}'.format(stri, SEPARATOR, col)
            #printDebug(s)
            return s
        else:
            return self.getCol(row)
    def getStr(self, row):
        return str(row).ljust(self.str_width)
    def getCol(self, row):
        a = ColumnObject.getVal(self, row)
        b = cutoff(a, self.col_width)
        return b.ljust(self.col_width)
    def getHeader(self):
        if self.display_string:
            stri = self.header.ljust(self.str_width)
            col = self.attribute_name.ljust(self.col_width)
            return '{}{}{}'.format(stri, SEPARATOR, col)
        
        #return '{}{}{}'.format(row, SEPARATOR, getattr(row, self.column))      
class ResultObject():
    def __init__(self, nTable, column=None):
        #TODO: result objects
    
        exit()
#def print_query3(q, obj_mode=False, nTable=None, 
#                 limit=-1, hide=False, column=None):
#    assert False
#    rows = q.all()
#    if hide:
#        return rows
#    if len(rows) == 0:
#        return []
#    previews = dict()
#    cols = len(q.column_descriptions)
#    if obj_mode:
#        headers = utils.parseColumns(nTable)
#        max_len = maxLenObj(rows, q, previews, nTable, headers)
#        stri = makeHeaders2(headers, max_len)
#    else:
#        max_len = maxLen(cols, rows, q, previews)
#        stri = makeHeaders(cols, max_len, q)
#    print stri
#    cross_border = makeBorder(stri)
#    print cross_border
#    if obj_mode:
#        printRowsObj(rows, headers, max_len, nTable, limit=limit)
#        return rows
#    else:
#        printRows(rows, cols, previews, max_len,)
#        return dict_result(q, rows)
#def printOneColumn(nTable, col_str, query=None, **kw):
#    
#    if query is None:
#        sess = utils.open_session()
#        query = sess.query(nTable.mClass)
#    print_query3(query, column=col_str, obj_mode=True, **kw)
#def maxLenObj(rows, q, previews, nTable, headers):
#    HASH_CHOICE = 1
#    
#    max_len = dict()
#    
#    for i in headers:
#        a = calc_col_widths(nTable, i[0], max_len, rows, q, previews)
#        assert a is not None
#        max_len[i[HASH_CHOICE]] = a
#       
#    return max_len
#def maxLen(cols, rows, q, previews, nTable):
#    max_len = bytearray(cols)
#    for i in max_len:
#        max_len[i] = 0
#    for i in range(cols):
#        max_len[i] = calc_col_widths(nTable, i, max_len, rows, q, previews)
#    return max_len
#def print_object(a):
#    print_query3(a, obj_mode=True)
#def print_query2(q):
#    assert False
#    global COL_LIMIT
#    rows = q.all()
#    previews = dict()
#    #Calculate column widths
#    cols = len(q.column_descriptions)
#    max_len = bytearray(cols)
#    for i in max_len:
#        max_len[i] = 0
#    for i in range(cols):
#        i2 = q.column_descriptions[i]
#        len1 = len(i2['name'])
#        if len1 > max_len[i]:
#            max_len[i] = len1
#            
#        for j in rows:
#            s = str(j[i])
#            if len(s) > COL_LIMIT:
#                previews[s] = substring(s, COL_LIMIT)
#                max_len[i] = COL_LIMIT
#            elif len(s) > max_len[i]:
#                max_len[i] = len(s)
#    stri = ""
#    #print column headers
#    for i in range(cols):
#        i2 = q.column_descriptions[i]
#        header = i2['name']
#        if i == cols-1:
#            stri = stri + header.ljust(max_len[i])
#        else:
#            stri = stri + header.ljust(max_len[i]) + ' | '
#    print(stri)
#    
#    #calculate length of cross border
#    t_len = len(stri)
#    stri = ""
#    for i in range(t_len):
#        stri = stri + "=" 
#    print stri
#        
#    stri = ""
#    for i in rows:
#        for j in range(cols):
#            p = str(i[j])
#            b = p in previews
#            if b:
#                stri += previews[p].ljust(max_len[j])
#            else:
#                stri += p.ljust(max_len[j])
#            if j != cols-1:
#                stri += ' | '
#        print stri
#        stri = ""
#    return dict_result(q, rows)
def substring(s, i):
    stri = ''
    
    if len(s) < i:
        return s
    for a in range(i-5):
        stri += s[a]
    stri += '...'
    return stri
def dict_result(q, rows=None):
    
    if rows is None:
        rows = q.all()
    cols = q.column_descriptions
    
    row_dicts = []
    for row in rows:
        row_dict = {}
        
        num_of_cols = len(cols)
        for index in range(num_of_cols):
            cols2 = cols[index]
            row_dict[cols2['name']] = row[index]

        row_dicts.append(row_dict)
    return row_dicts
def fkSelect3(col):
    assert False
    sys.modules['cli_tools'].print_err('fkSelect3')
    for findLevelNo in col.foreign_keys:
        origin = col.table
        destination = findLevelNo.column.table
        remote = col.name
        if designate(remote, origin):
            return selectID(destination)
        else:
            return None
def designate(remote, origin):
    assert False
    try:
        single = eval('dbmaps.{}.name_singular'.format(origin))
    except:
        single = origin
#=============================================================================#
    return sys.modules['cli_tools'].yn_prompt_loop('Do you want to designate a {} for this {}?'.format(remote, single), override=True)
def selectID(classname, tablename=None, override=False, 
             default_value=None, query=None, prompt=None):
    
    if classname.__class__ == str:
        nTable = utils.getNTable(classname)
    elif classname.__class__ == utils.nvmTable:
        nTable = classname
    else:
        raise TypeError('str or nvmtable required for SelectID')
    fk = sys.modules['nvm_types'].Fk
    o = fk(nTable, query=query)
    return o.prompt(remote=nTable.mClass, prompt=prompt)
    
    #assert False
    #nTable = utils.get_nvmTable(classname)
    #
    #tablename = nTable.name
    #if query is None:
    #    select_star(classname)
    #else:
    #    pass
    #    #print_query3(query)
    #if prompt is None:
    #    prompt = "Please type the id of the {0} that you wish to select.".format(nTable.mClass.name_singular)
    # 
    #s = sys.modules['cli_tools'].prompt_loop_eval(prompt, 'invalid command',
    #                        e='db.object_query_at("{}", i)'.format(nTable.classname), override=override, default_value=default_value)
    #return s
def select_star(classname, printTable=True):
    raise NotImplementedError
    #assert False
    nTable = utils.getNTable(classname)
    #session = utils.open_session()
    c = utils.find_columns(nTable)
    if c is None:
        print 'Table name not recognized.'
        return    
    stri = ""
    for el in c:
        mod = nTable.mod_path
        if mod is not None and mod != 'core':
            exec('import {}'.format(mod))
        stri += '{}.{}, '.format(nTable.getFullName(), el.name)
    q = eval('session.query({})'.format(stri))
    if printTable:
        raise NotImplementedError
        #return print_query3(q)
    else:
        return dict_result(q)
def trans_commit(session, override=False):
    assert False
    if override:
        session.close()
        session.commit()
        return 1
    try:
        session.commit()
        session.close()
        return 1
    except:
        print('error in commiting to database. Changes will be rolled back instead. It is possible that you entered a string that was too long.')
        session.rollback()
        session.close()
        return 0
query_command = 'python subquery'
def query_interp(i):
    assert False
    print 'Query Interpreter is not supported yet'
    raise Exception('not supported')
def test_end():
    assert False
    print('end of test.')
    exit()
def capwords(s):
    sp = s.split()
    new = ''
    for i in sp:
        new += '{} '.format(i.capitalize())
    return new.rstrip(' ')
    #printDebug('"{}"'.format(new))
    #return new
def getCaption(key, val):
    if val.__class__ == dict and 'caption' in val:
        return val['caption']
    else:
        return key
def findKey(typemap, caption):
    #printDebug('caption={}'.format(caption))
    for key, val in typemap.iteritems():
        if equiv(getCaption(key, val), caption):
            return key
def equiv(left, right):
    if left.__class__ != str:
        raise TypeError('left argument {} of class {} must be string.'.format(left,
                                                                              left.__class__))
    if right.__class__ != str:
        raise TypeError('right argument {} of class {} must be string.'.format(right,
                                                                               right.__class__))
    
    if left == '{}: '.format(right):
        return True
    if left == right.lower():
        return True
def pickType(a):
    if a.__class__ == dict:
        return a['type']
    else:
        return a
def getHeader(first, nTable):
    for i in utils.parseColumns(nTable):
        if first == i[1]:
            return i
def findType(caption, typemap):
    lower_caption = caption.lower()
    found_key = findKey(typemap, caption)
    assert found_key is not None
    return pickType(typemap[found_key])
def filter_ver(el, typemap=None, query=None, pair=None, nTable=None):
    #assert False
    assert query is not None
    assert nTable is not None
    if el is not None:
        args = el.split('=')
    elif pair is not None:
        args = []
    else:
        raise TypeError('No string or list data recieved.')
    #f_args = el.split('%')
    len_arg = len(args)

    if pair is not None:
        first = pair[0]
        second = pair[1]
    else:
        first = capwords(args[0])
        element, caption = getHeader(first, nTable)
        type_ = findType(caption, typemap)
        el2 = utils.staticForeignKeyName(nTable, element)
        o = getattr(sys.modules['nvm_types'], type_)
        second = o().verify(args[1])
    
    if len_arg == 2:
        kw = {el2:second}
        
        #printDebug('first={} {}'.format(element.__class__, element))
        #printDebug('second={} {}'.format(second.__class__, second))
        
        try:
            query = query.filter_by(**kw)
        except:
            printDebug('error mapping {} {} onto {}'.format(second.__class__,
                                                                   second,
                                                                   el2))
            exit()
        #printDebug('')
        return query, [first, second]
    else:
        raise IOError
 
    printDebug('filter ver end')
    return query
#def object_query_at(classname, index, session=None):
#    assert False
#    if session is None:
#        session = utils.open_session()
#        own_session = True
#    else:
#        own_session = False
#    
#    o = session.query(getClass(classname)).get(index)
#    assert o is not None
#    if own_session:
#        session.close()
#    return o
#def get_rel(columname):
#    assert False
#    return columname.rstrip('_id')
#def getClassnameFromID(foreignkey):
#    assert False
#    fk = foreignkey.split('.')
#    return fk[0].capitalize()
#def followAssoc(parent_table, id_column):
#    assert False
#    s = eval('dbmaps.{0}.{1}.foreign_keys'.format(parent_table, id_column))
#    s2 = None
#    for i in s:
#        s2 = i.column
#    s3 = s2.table
#    return s3
#def session_complete(findLevelNo, *args, **kwargs):
#    assert False
#    session = utils.open_session()
#    ans = findLevelNo(*args, **kwargs)
#    res = trans_commit(session)
#    return ans
