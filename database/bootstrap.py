from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Sequence, Column, Integer, String, Text
Base = declarative_base()

POLY_ON = 'polymorphic_on'

class nvMaps(Base):
    __tablename__='nvMaps'
    __tabletype__='meta'
    __mapper_args__ = {}
    #======================#
    id = Column(Integer, Sequence('map_seq'), primary_key=True)
    name = Column(String(50))
    alebmic_file = Column(Text)
    maps_file = Column(Text)
    static_load_directory = Column(Text)
    def __str__(self):
        return str(self.name)