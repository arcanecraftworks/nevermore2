#===========================standard library imports==========================#
from datetime import datetime
#============================3rd party imports================================#
#================================local imports================================#
import utils
from utils import printDebug
import query
#==================================Dev Flags==================================#
#==================================Constants==================================#
#=============================================================================#
class EpicTimeLine():
    """data management container that allows for years to be expressed in
    values greater than datetime.max_year
    """
    def __init__(self, *epochs):
        if len(epochs) == 0:
            epochs = utils.process('Epochs', for_each=self.__build__)
        
        self.epoch_list = epochs
         
    def makeEpicTime(self, d):
        if d.__class__ == EpicTime:
            return d
        else:
            return EpicTime(self.epoch_list, d)
    
    def __build__(self, el):
        return el.id, el.max_year
    
    def lesserOf(self, left, right):
        
        left_epic = self.makeEpicTime(left)
        right_epic = self.makeEpicTime(right)
        #Times = utils.getClass('Times')
        
        b = None
        
        
        if left.epoch < right.epoch:
            #printDebug('Asuna')
            b = True
        elif left.epoch > right.epoch:
            #printDebug('Kirito')
            b = False
        elif left.epoch.goes_forward:
            #printDebug('Suguha')
            b = left.date_time < right.date_time
        else:
            raise NotImplementedError()
        
        if b:
            return right
        else:
            return left
    
    def AreEqual(self, left, right):
        return left.epoch == right.epoch and left.date_time == right.date_time
    #def isGreaterThan(self, el):
    def greaterOf(self, left, right):
        if self.lesserOf(left, right) is left:
            return right
        else:
            return left
    #    if self.isLessThan(el):
    #        return False
    #    elif self.isEqualTo(el):
    #        return False
    #    else:
    #        return True
class EpicTime():
    def __init__(self, epochs_list, d):
        self.time = d
        self.year = self.collapseEpochs(epochs_list, d)
    
    def collapseEpochs(self, epoch_list, d):
        Time = utils.getClass('Times')
        return self.__coleps2__(epoch_list)
        #day = d.date_time.day
        #month = d.date_time.month
        #try:
        #    dt = datetime(day=day, month=month, year=year)
        #except:
        #    printDebug('year {} is out of  range'.format(year))
        #    exit()
        #return Time(date_time=dt, epoch=self.epoch)
    def __coleps2__(self, epoch_list):
        i = self.time.epoch.id
        y = 0
        for id_, max_year in epoch_list:
            #printDebug(epoch)
            #printDebug('id={}'.format(epoch.id))
            #printDebug('goes_forward={}'.format(epoch.goes_forward))
            #printDebug('i={}'.format(i))
            if id_ == i and not self.time.epoch.goes_forward:
                a = self.max_year - self.year
                #printDebug('Kirito={}'.format(a))
                return y + a
                return y + self.year
            elif id_ == i:
                #printDebug('Asuna')
                return y + self.time.date_time.year
            elif id_ < i:
                #printDebug('Suguha')
                y = y + max_year