#===========================standard library imports==========================#
import sys
#============================3rd party imports================================#
#================================local imports================================#
#==================================Constants==================================#
header = 'postgresql+psycopg2://'
create_command = "create database {0} with owner {1}"
drop_command = 'drop database {}'
#=============================================================================#
BUILD_TEMPLATE = '{0}{1}:{2}@{3}:{4}/{5}'
DEFAULT_SCHEMA = 'postgres'
TITLE = 'PostgreSQL'
ADMIN_ROLE = 'nevermore'
ADMIN_PASSWORD = 'nevermore'
def printDebug(s):
    sys.modules['cli_tools'].printDebug(s)
def build(password=None, 
          host=None, username=None, port=None, schema=None, **kw):
    
    return BUILD_TEMPLATE.format(header, username, password, 
                                host, port, schema)

def build_admin(**kw):
    #printDebug('kw={}'.format(kw))
    kw_admin = {}
    for key, val in kw.iteritems():
        if key == 'username':
            kw_admin[key] = ADMIN_ROLE
        elif key == 'password':
            kw_admin[key] = ADMIN_PASSWORD
        else:
            kw_admin[key] = val
    
    return build(**kw_admin)
    

def create_string(**kw):
    pass

#def createEmptyDatabase(admin_database='postgres', su_password='Telerion3017', **kw):
#    if dbconfig.dbms == 1:
#        #header = postgres_header
#        command = create_command
#    else:
#        print temp_message
#        exit()
#
#    engine = create_engine('{0}{1}:{2}@{3}/'.format(header, 
#                                                    dbconfig.user, 
#                                                    su_password, 
#                                                    dbconfig.host, 
#                                                    admin_database)
#                           )
#    conn = engine.connect()
#    conn.execute("commit")
#    conn.execute(command.format(dbconfig.schema, dbconfig.username))
#    conn.close()