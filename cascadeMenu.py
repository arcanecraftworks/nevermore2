#===========================standard library imports==========================#
from Tkinter import Menu
from Tkconstants import ACTIVE
#============================3rd party imports================================#
#================================local imports================================#
from cli_tools import printDebug, printDictOfObject
#==================================Constants==================================#

class CascadeMenu():
    def __init__(self, label, parent, command_dict, state=ACTIVE):
        #printDebug(label)
        self.menu = Menu(parent, tearoff=0)
        self.__com_list__(command_dict)
        parent.add_cascade(label=label, menu=self.menu, state=state)
    def __com_list__(self, command_dict):
        
        for i in command_dict:
            cdi = command_dict[i]
            
            if cdi.__class__ == [].__class__ and len(cdi) == 2:
                comm = cdi[0]
                state = cdi[1]
            else:
                comm = cdi
                state = ACTIVE
        
            self.menu.add_command(label=i, command=comm, state=state)
    def entryconfig(self, *arg, **kw):
        self.menu.entryconfig(*arg, **kw)
    def add_command(self, *arg, **kw):
        self.menu.add_command(*arg, **kw)
        
        