#===========================standard library imports==========================#
from Tkinter import Label, LEFT, BOTH, RAISED, NW, N, E, S, W,\
Button, Toplevel, Menu, ACTIVE, DISABLED
#============================3rd party imports================================#
#================================local imports================================#
from scrollableFrame import VerticalScrolledFrame
import entry
import database.utils as utils
from NCallback import Callback
from MessageBox import onQuest
from cli_tools import printDebug
#==================================Constants==================================#
VIEWFRAME_H = 400
VIEWFRAME_W = 600
CELL_WRAPLENGTH = 200
DEF_LIMIT = 100
ADDNEW_PADDING = 20
STR_LIMIT = 140
DEL_CONF = 'Are you sure you want to delete '+\
            '{} item{} from the database?'

REPLACE = {u'u\2019',"'"}
#=============================================================================#

class ViewFrame():
    EDIT_MENU = "Enter Edit Mode"
    DEL_MENU = "Enter Delete Mode"

    def __init__(self, parent, tablename=None, nTable=None, 
                 query=None, limit=DEF_LIMIT):
        self.root = parent
        assert tablename is not None or \
        nTable is not None or query is not None
        
        if tablename is not None and nTable is None:
            nTable = utils.nTables[tablename]      
        self.title = nTable.name
        self.LIMIT = limit
        self.deletemode=False
        if nTable is not None:
            self.nTable = nTable
            self.mClass = nTable.mClass
            self.query = None
        elif query is not None:
            self.mClass = None
            self.query = query
        self.session = utils.open_session()
        self.nTable = nTable
        self.to_delete = []
        #self.root = Toplevel()
        
        self.colNotes = dict()
        self.root.title(self.title)
        
        b = self.isPolymorphic()
        if b and self.mClass is not None:
            self.polyHost()
            self.initPoly()
        else:
            self.initUI()
    def initPoly(self):
        d = self.query.all()
        self.headers = utils.parseColumns(self.nTable)
        self.lenCheck(self.headers, d)
    def initUI(self):
        
        if self.nTable is not None:
            self.headers = utils.parseColumns(nTable=self.nTable)
        else:
            self.headers = self.parseQuery()
        self.refresh()
    def landing(self, a, rowno):
        #printDebug(a.__class__)
        
        if rowno == -1:
            rowno = len(self.array)+2
        
        self.makeRow(a, rowno)
        self.array.append(a)
        #self.refresh()
    def refresh(self, override=False):
        if 'frame' in self.__dict__:
            self.frame.pack_forget()
        array = utils.process(self.mClass, 
                              query=self.query, 
                              limit=self.LIMIT)
        
        self.array = array
        self.lenCheck(self.headers, array, override=override)
    def lenCheck(self, headers, array, override=False):
        if override:
            
            self.root.destroy()
            return
        elif len(array) == 0:
            result = onQuest('Table is empty. Create New?', 
                             title=self.title)
            if result == 'yes':
                if 'x_ntable' in self.__dict__:
                    nTable = self.x_ntable
                else:
                    nTable = self.nTable
                root = Toplevel()
                #assert False
                entry.EntryWindow(root, nTable=nTable)
        else:
            self.populate(headers, array)
    def populate(self, headers, array):
        #assert False
        self.buttons = []
        self.frame = VerticalScrolledFrame(self.root)
        self.frame.pack()
        self.widgets_here = self.frame.interior
        self.headers = headers
        colno = 1    
        for rowno in range(len(array)+1):
            if rowno == 0:
                row_obj = None
                for col in headers:
                    self.makeLabel(col[1], 0, colno)
                    colno= colno + 1
            else:
                row_obj = array[rowno-1]
                self.makeRow(row_obj, rowno)
            colno = 1
        self.frame.pack(side=LEFT, fill=BOTH)
        self.makeMenu()
    def makeRow(self, row_obj, rowno):
        #cli_utils = sys.modules['database.cli_utils']
        colno = 1
        self.makeButton(rowno, rowno, 0, row_obj)
        for col in self.headers:
            try:
                id_ = utils.searchVal(self.nTable, row_obj, col[0])
                self.makeCell(id_, rowno, colno)
                colno = colno + 1
            except UnicodeEncodeError:
                pass
    
    def setFrameType(self, obj, rowno):
        if not self.deletemode:
            nTable = utils.nTables[obj.__tablename__]
            self.editLaunch(nTable, _object=obj, rowno=rowno)
        else:
            for b in self.to_delete:
                if obj == b:
                    self.unDeleteObject(obj)
                    return
            self.deleteObject(obj)
    def deleteObject(self, obj):
        for b in self.buttons:
            if obj == b[0]:
                button = b[1]                    
                self.to_delete.append(obj)
                button.config(background='red')
    def unDeleteObject(self, obj):
        for b in self.to_delete:
            if obj == b:
                self.to_delete.remove(obj)
                for b in self.buttons:
                    if obj == b[0]:
                        b[1].config(background='gray')
    def makeLabel(self, label, rowno, colno):
        L = Label(self.widgets_here, text=label, 
                  borderwidth=3, relief=RAISED)
        L.grid(row=rowno, column=colno, sticky=W+N+E+S)
    def makeButton(self, label, rowno, colno, row_obj):
        b = Button(self.widgets_here, 
                   text=label, 
                   command=Callback(self.setFrameType, row_obj, rowno), 
                   borderwidth=3, 
                   relief=RAISED)
        b.grid(row=rowno, column=colno, sticky=W+N+E+S)
        self.buttons.append([row_obj, b])
    def makeCell(self, label, rowno, colno):
        
        if label is None:
            label = ''
        else:
            label = str(label)
        
        label = label[0:STR_LIMIT]
        if label == 'None':
            label = ''
        L = Label(self.widgets_here, 
              text=label, 
              background='white', 
              anchor=NW, 
              justify=LEFT, 
              wraplength=CELL_WRAPLENGTH)
        L.grid(row=rowno, 
               column=colno, 
               sticky=W+N+E+S, 
               padx=1, pady=1)
    def unicodeProc(self, u):
        s = ''
        for el in u:
            try:
                s = s + str(el)
            except Exception as e:
                if el in REPLACE:
                    s = s + REPLACE[el]
                else:
                    pass
        printDebug('s={}'.format(s))
        return s
    
    def parseQuery(self):
        for row in self.query:
            print row
        exit()
    def polyHost(self):
        map_dict = self.mClass.__mapper_args__
        
        host_name = map_dict['inherit_condition'].right.table.name
        host_nTable = utils.nTables[host_name]
        hostClass = host_nTable.mClass
        column = hostClass.__mapper_args__['polymorphic_on']
        value = map_dict['polymorphic_identity']
        
        query = self.session.query(hostClass)
        query = query.filter(column==value)
        self.query = query.filter_by(mk_del = False)
        
        assert self.query is not None
        self.x_ntable = self.nTable
        self.nTable = host_nTable
        return hostClass
    def isPolymorphic(self):
        mClass = self.nTable.mClass
        
        if '__mapper_args__' in mClass.__dict__:
            return 'polymorphic_identity' in mClass.__mapper_args__
        else:
            return False
    def makeMenu(self):
        self.menu = Menu(self.widgets_here, tearoff=0)
        
        self.menu.add_command(label=self.DEL_MENU, 
                              command=self.deleteMode)
        self.menu.add_command(label=self.EDIT_MENU, 
                              command=self.editMode, 
                              state=DISABLED)
        
        
        if 'x_ntable' in self.__dict__:
            c = Callback(self.editLaunch, self.x_ntable)
        else:
            c = Callback(self.editLaunch, self.nTable)
            
        self.menu.add_command(label='Add New', command=c)
        self.menu.add_command(label="Cancel")
        self.menu.add_command(label='Refresh', command=self.refresh)
        self.root.bind("<Button-3>", self.showMenu)
    def editLaunch(self, nTable, _object=None, rowno=-1):
        #printDebug('editLaunch')
        root = Toplevel()
        entry.EntryWindow(root, nTable,
                          return_=self.landing, 
                          mapped_object=_object,
                          rowno=rowno,
                          )
    def deleteMode(self):
        self.deletemode = True
        self.menu.entryconfig(self.DEL_MENU, state=DISABLED)
        self.menu.entryconfig(self.EDIT_MENU, state=ACTIVE)
    def editMode(self):
        if len(self.to_delete) > 0 and self.confirm() and self.commit():
            self.deletemode = False
            self.menu.entryconfig(self.DEL_MENU, state=ACTIVE)
            self.menu.entryconfig(self.EDIT_MENU, state=DISABLED)
            self.refresh(override=True)
        else:
            self.deletemode = False
    def commit(self):
        session = self.session
        try:
            for o in self.to_delete:
                utils.delete(session, o)
            session.commit()
            return True
        except:
            raise
    def confirm(self):
        num = len(self.to_delete)
        if num == 1:
            plural = ''
        else:
            plural = 's'        
        result = onQuest(DEL_CONF.format(num, plural))
        if result == 'yes':
            return True
        else:
            return False
    def showMenu(self, e):
        self.menu.post(e.x_root, e.y_root)
    def onExit(self):
        self.quit()