#===========================standard library imports==========================#
#============================3rd party imports================================#
#================================local imports================================#
from cli_tools import printDebug
#==================================Constants==================================#
#=============================================================================#
class Mouse():
    def __init__(self, root_obj, motion_callback=None,
                 click_callback=None):
        if motion_callback is None:
            motion_callback = self.motion
        if click_callback is None:
            click_callback = self.click
        
        root_obj.bind('<Motion>',motion_callback)
    def motion(self, event):
        pass
    def click(self, event):
        pass