import database.core03 as dbmaps
import database.utils as utils
import cli

globalSession = None
def assoc(parent, rel, child):
    a = parent.rels[rel]()
    try:
        a.child = child
        exec('parent.{}.append(a)'.format(rel))
    except KeyError:
        cli.printDebug('unable to associate {} and {}'.format(parent, child))
        print parent.rels      
def locationClone(parent, lies_within=None):
    dbmaps.locationClone(parent, lies_within)
def location(name, description, lies_within=None):
    loc = dbmaps.Locations(name=name, description=description)
    if lies_within is not None:
        assoc(loc, 'lies_within', lies_within)
        addGlobal(loc)
    return loc
def addGlobal(loc):
    global globalSession
    if globalSession is not None:
        globalSession.add(loc)
    else:
        globalSession = utils.open_session()
        globalSession.add(loc)
        
def location2(name, lies_within, description='', secondary=None):
    loc = location(name, description=description, lies_within=lies_within)
    if secondary is not None:
        assoc(loc, 'lies_within', secondary)
    return loc      
def city(name, lies_within, description=''):
    city = dbmaps.Cities(name=name, description=description)
    city_loc = locationClone(city, lies_within=lies_within)
    addGlobal(city)
    addGlobal(city_loc)
    return city_loc

def tcs_add():
    session = utils.open_session()
    
    
    session.commit()
    session.close()

if __name__ == '__main__':
    cli.cli_access
    print("end of TCS module.")
