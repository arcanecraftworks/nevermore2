#===========================standard library imports==========================#
import os
import sys
from Tkinter import Frame, Menu, Toplevel, Tk
from Tkconstants import BOTTOM, X, DISABLED, BOTH, ACTIVE, SUNKEN
import datetime
import traceback
#============================3rd party imports================================#
#================================local imports================================#
#import file_utils
#from file_utils import backup, restore
import database.utils as utils
from macro import macro as sep_macro
import entry
from timeline import Timeline
from cascadeMenu import CascadeMenu
from view import ViewFrame
from modselector import ModSelector
from NCallback import Callback
from cli_tools import printDebug, printDictOfObject, EXCEPT_OVERRIDE, runClear
#from DBConfig import ConfigFrame
from dbconfig2 import ConfigFrame
from headless import run_headless
from connector import autoload, connect
from tktools import top_window
#from Nevermore.database.query import getCharacterByFirstName,\
#    scenesByCharAppearance
#from pane import WorkPaneBin
#==================================Switches===================================#
HEADLESS_OVERRIDE = False
START_MAXIMIZED = True
SIGNIN_OVERRIDE = True
#CLI_OVERRIDE = False
MACRO = False
EXC_OVERRIDE = False
#==================================Constants==================================#
nevermore_version = 'Nevermore v0.3.0'
C_DRIVE = 'C:\\'
graceful_exit = False

DEFAULT_DBMS = 'PostgreSQL'
DEF_WIDTH = 800
DEF_HEIGHT = 600
menu_hook = []
EMPTY_BKGND = '#777'
FAREWELL = 'Okay? Okay.'
menu_set = False
LOG_FILETYPE = 'txt'
ROOT = os.path.dirname(__file__)
LOG = os.path.join(ROOT, 'log')
#=============================================================================#

def stub(*arg, **kw):
    pass

class mainFrame(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.entryMethods = dict()
        self.initUI()
    def initUI(self):
        self.parent.title(nevermore_version)
        self.config(background=EMPTY_BKGND)
        #self.wp = WorkPaneBin(self)
        
        self.timeline = Timeline(self, borderwidth=5, 
                     relief=SUNKEN,
                     height=200,
                     width=500,
                     background='#FFF')
        #self.timeline = self.timelineframe.timeline
        self.initMenuBar()
        self.timeline.pack(side=BOTTOM, fill=X)
        self.pack(fill=BOTH, expand=1)
        self.configwindow(override=True)
    def initMenuBar(self):
        menubar = Menu(self.parent)
        self.menubar = menubar
        self.parent.config(menu=menubar)
        self.fileMenu = self.makeMenu('File')
        self.new_menu = self.makeMenu('New', state=DISABLED)
        self.viewMenu = self.makeMenu('View', state=DISABLED)
        self.dungMenu = self.makeMenu('Dungeon', state=DISABLED)
        self.menu_set = False
        
        self.makeMenu('Window', state=DISABLED)
        self.makeMenu('Settings', state=DISABLED)
        #self.makeMenu('Dungeon')
        
        self.timeline.unpack(menubar)
        self.makeMenu('Macro')
    def makeMenu(self, key, **kw):
        return CascadeMenu(key, self.menubar, self.md(key), **kw)
    def md(self, menu_key):
        ps = 'New','View','Window','Settings',
        
        
        if menu_key == 'File':
            return {
                    'Exit':self.onExit,
                    'Open':self.configwindow
                    }
        elif menu_key == 'Macro':
            return {
                    }
                    #'Backup':file_utils.backup,
                    #'Restore':file_utils.restore
                    #}
        elif menu_key == 'Dungeon':
            return {
                    'Town Gen':self.town_gen_launch,
                    #'Add data':self.dungMacro
                    }
        elif menu_key in ps:
            return dict()
        else:
            assert False
    def town_gen_launch(self):
        import database.dungeon_core02 as dc2
        utils.create_all()
        dc2.town_generate()
    def dungMacro(self):
        import database.dungeon_core02 as dc2
        utils.create_all()
        dc2.macro()
    def macro2(self):
        printDictOfObject(self)
    def onView(self):
        #root = Toplevel(self.master)
        ViewFrame(nTable=utils.nTables['Worlds'])
    def load(self):
        if len(utils.nTables) == 0:
            assert False
        
        
        #printDebug('load={}'.format(len(utils.nTables)))
        
        self.tablesCascade(self.new_menu, command=self.entryLaunch)
        self.tablesCascade(self.viewMenu, command=self.viewLaunch)

        #printDictOfObject(self.cfg)
        try:
            self.addTitle(self.cfg.alias.get())
        except AttributeError:
            pass
        self.loadWorkstation()
        self.re_enable()
        #printDebug('load end')
    def addTitle(self, alias):
        s = '{} - {}'.format(nevermore_version, 
                             alias)
        
        self.parent.title(s)
    def loadWorkstation(self):
        for el in menu_hook:
            printDebug(el)
            el(self)
        self.timeline.initialize()
        #TODO: code
        #for chapter in utils.open_session().query(utils.getClass('Chapters')):
        #    printDebug('id={} cn={} title={}'.format(chapter.id, chapter.chapter_number, chapter.title))
        #    if chapter.id == 2:
        #        chapter.chapter_number = 1 title: 
        
        #exit('loadworkstation')
    def setHolidays(self):
        sess = utils.open_session()
        for i in sess.query(utils.getClass('Holidays')):
            printDebug('{} {} {}'.format(i.id, i, i.fixed_day, i))
            #i.makeScenes()
    def re_enable(self):
        #to_enable = ['New','Window','Settings','View','Macro']
        #printDebug('re-enable')
        to_enable = ['New','View','Macro','Dungeon']
        for i in to_enable:
            self.menubar.entryconfig(i, state=ACTIVE)
        self.fileMenu.entryconfig('Open', state=DISABLED)
        self.menu_set = True
    def tablesCascade(self, menu, command=None):
        if not self.menu_set:
            for t in utils.nTables:
                nTable = utils.nTables[t]
                try:
                    mC = nTable.mClass
                    assert mC is not None
                except AttributeError as e:
                    printDebug(nTable)
                    runClear(str(e))
                except AssertionError as e:
                    e.override = False
                    raise e
                ATTR = '__tabletype__'
                if ATTR in mC.__dict__ and getattr(mC, ATTR) == 'front':
                    self.addToMenu(nTable, menu, command=command)
    def viewLaunch(self, s):
        root = Toplevel(self.master)
        ViewFrame(root, tablename=s)
    def addToMenu(self, nTable, menu, command=stub):
        label = nTable.sTable.name
        c = Callback(command, label)
        menu.add_command(label=label, command=c)
    def configwindow(self, override=False):
        #printDebug('configWindow')
        try:
            a = autoload(self, callback=self.__configwindow2__)
            
            if a is None:
                a = self.configLaunch()
                #printDebug('a={}')
            #else:
            #    #exit('afjldksfj')
            #    self.initialize()
            #    self.addTitle(a)
        except:
            raise
    def __configwindow2__(self, a):
        
        self.initialize()
        self.addTitle(a)
        return a
    def configLaunch(self):
        printDebug('configLaunch', interrupt_override=True)
        root2 = top_window()
        c = Callback(self.connectZero, parent=self)
        self.configGUI = ConfigFrame(root2, None, callback_obj=c)
    def connectZero(self, *arg, **kw):
        b = connect(*arg, **kw)
        printDebug('b={}'.format(b))
        return b
    def onEntry(self):
        root2 = Toplevel(self.master)
        #global eventhook
        #eventhook = stub
        entry.EntryWindow(root2)
    def stub(self):
        exit('stub')   
    def onExit(self):
        self.quit()   
    def centerWindow(self):
        centerWindow(self)
    def modSelLaunch(self):
        root = Toplevel(self.master)
        ModSelector(root, return_=self.initialize)
    def passwordProbs(self):
        exit('MainFrame.passwordProbs')
    def unInit(self):
        exit('MainFrame.unInit')
    def initialize(self):
        #printDebug('cfg={}'.format(cfg))
        try:
            utils.initTables()
            self.load()
            
        except Exception as e:
            if EXCEPT_OVERRIDE:
                raise
            if 'override' in e.__dict__ and not e.override:
                exit('initial setup complete. Reload to access.')
            else:
                raise
    def entryLaunch(self, s):
        root2 = Toplevel(self.master)
        #TODO: entry
        entry.EntryWindow(root2, tablename=s)
def buildGeometry(width, height, offset_x, offset_y):
    return '{}x{}+{}+{}'.format(width, height, offset_x, offset_y)
def centerWindow(parent, w=DEF_WIDTH, h=DEF_HEIGHT):
    #w = DEF_WIDTH
    #h = DEF_HEIGHT
    sh = parent.parent.winfo_screenheight()
    sw = parent.parent.winfo_screenwidth()
    x = (sw - w)/2
    y = (sh - h)/2
    parent.parent.geometry(buildGeometry(w, h, x, y))
def maximize(root):
    root.wm_state('zoomed')
def macro():
    #TODO: macro code here
    sep_macro()
    exit('macro successful')

def main():
    
    root = Tk()
    if START_MAXIMIZED:
        maximize(root)
    
    if HEADLESS_OVERRIDE or os.name == 'posix':
        run_headless()
    else:
        main = mainFrame(root)
        root.mainloop()
    
    junk = str(raw_input(FAREWELL))
#if __name__ == '__main__':
#    main()    
def makeName():
    dt = datetime.datetime.now()
    str_dt = str(dt)
    
    return str_dt.replace(':','-')

def printLog(filename):
    with open(filename, 'w') as file_:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        file_.write("*** print_tb:")
        traceback.print_tb(exc_traceback, limit=1, file=file_)
        file_.write( "*** print_exception:")
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=2, file=file_)
        file_.write("*** print_exc:")
        traceback.print_exc()
        file_.write("*** format_exc, first and last line:")
        formatted_lines = traceback.format_exc().splitlines()
        file_.write(formatted_lines[0])
        file_.write(formatted_lines[-1])
        file_.write("*** format_exception:")
        file_.write(repr(traceback.format_exception(exc_type, exc_value,
                                          exc_traceback)))
        file_.write("*** extract_tb:")
        file_.write(repr(traceback.extract_tb(exc_traceback)))
        file_.write("*** format_tb:")
        file_.write(repr(traceback.format_tb(exc_traceback)))
        file_.write("*** tb_lineno:{}".format(exc_traceback.tb_lineno))
if __name__ == '__main__':
    #confirm('Imports')
    try:
        main()#f = FaxObjList()
        #confirm('build')
        #print(repr(f))
        #confirm('print')
        #mainloop(f)
    except:
        printDebug(datetime.datetime.now())
        name = makeName()
        if not os.path.exists(LOG):
            os.mkdir(LOG)
        filename = os.path.join(LOG, '{}.{}'.format(name, LOG_FILETYPE))
        printLog(filename)        
        
