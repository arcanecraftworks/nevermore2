#========================library methods============================#
from Tkinter import Frame, Tk, RAISED, Label, LEFT, BOTH, X, Menu,\
Button, Entry, StringVar, Toplevel, IntVar
import calendar

#======================nevermmore methods===========================#
#from Nevermore import maximize
from scrollableFrame import VerticalScrolledFrame
from cli_tools import printDictOfObject, printDebug, tryAgain
from Nevermore import NCallback
#==========================constants================================#
MONTH_ERR = '{} is not a valid value for a month. Months must be'+\
            ' between 1 and 12 inclusive.'
    
LABEL_BKGD = 'white'
WEEKDAYS_SUN = {0:'Sunday',1:'Monday',2:'Tuesday',3:'Wednesday',
        4:'Thursday', 5:'Friday', 6:'Saturday'}
WEEKDAYS_MON = {6:'Sunday',0:'Monday',1:'Tuesday',2:'Wednesday',
            3:'Thursday', 4:'Friday', 5:'Saturday'}
MONTHS = {1: 'January', 2: 'February', 3: 'March', 4: 'April',
          5: 'May', 6: 'June', 7: 'July', 8: 'August', 
          9: 'September', 10: 'October', 11: 'November',
          12: 'December'}
DEF_YEAR = 2015
DEF_MONTH = 10
ONE = 1
CELL_WIDTH = 80
HEADER_HEIGHT = 30

def findWeekDay(dt):
    no = dt.weekday()
    #printDebug('findWeekDay={}'.format(no))
    return WEEKDAYS_MON[no]
def stub(*arg, **kw):
    pass
class Picker(Frame):
    def __init__(self, parent, setFrameType, negative=False):
        Frame.__init__(self, parent)
        self.parent = parent
        self.c = setFrameType
        self.var = IntVar()
        e = Entry(self, textvariable=self.var)
        e.pack(side=LEFT)
        b = Button(self, text='OK', command=self.setFrameType)
        b.pack(side=LEFT)
        self.pack()
        self.negative = negative
    def setFrameType(self):
        if self.negative:
            self.c.kw['val'] = (self.var.get()) * -1
        else:
            self.c.kw['val'] = self.var.get()
        self.c.setFrameType()
        self.parent.destroy()
class NCalendarFrame(Frame):
    def __init__(self, parent, year=DEF_YEAR, month=ONE, 
                 return_=stub):
        Frame.__init__(self, parent)
        self.parent = parent
        self.year = year
        self.month = month
        self.return_ = return_
        self.makeCalendar()
        self.headerFrame = HeaderFrame(parent)
        self.headerFrame.pack(fill=X)
        self.frame = VerticalScrolledFrame(parent)
        self.widgets_here = self.frame.interior
        self.initUI()
        self.frame.pack(side=LEFT, fill=BOTH)
        self.makeTitle()
    def initUI(self):
        row = 0
        self.initMenubar()
        #self.makeCalendar()
        
        cells = []
        for week in self.makeCalendar():
            weekCells = []
            col = 0
            for daya in week:
                day = daya[0]
                e = CalendarCell(self.widgets_here, day,
                                 self.setFrameType)
                e.grid(row=row, column=col)
                col = col + 1
                weekCells.append(e)
            row = row + 1
            cells.append(weekCells)
            self.cells = cells
    def initMenubar(self):
        #TODO scrub
        
        menubar = Menu(self.parent)
        self.menubar = menubar
        self.parent.config(menu=menubar)
        fileMenu = Menu(menubar, tearoff=0)
        self.filemenu = fileMenu
        
        #Jump Year====Jump Month=====Set Month===Set Year====Jump Month===Jump YEAR
        #ONE          ONE            cust        cust        ONE          ONE
        #Mult         mult                                   mult         mult
        #kw = dict()
        kw = {'label':'delta_year','setFrameType': self.set, 'negative':True}
        cLand = NCallback(self.pickLanding, **kw)
        cLaunch = NCallback(self.launchPicker, cLand, negative=True)
        menubar.add_command(label='<<==', command=cLaunch.setFrameType)
        
        cLand = NCallback(self.pickLanding, 
                         label='month',
                         setFrameType=self.set,
                         negative=True
                         )
        cLaunch = NCallback(self.launchPicker, cLand, negative=True)
        menubar.add_command(label='<<=', command=cLaunch.setFrameType)
        
        cLaunch = NCallback(self.step,
                           label='delta_year',
                           negative=True)
        menubar.add_command(label='<==', command=cLaunch.setFrameType)
        
        cLaunch = NCallback(self.step,
                           label='delta_month',
                           negative=True)
        menubar.add_command(label='<=', command=cLaunch.setFrameType)
        
        cLand = NCallback(self.pickLanding,
                         label='year',
                         setFrameType=self.set)
        cLaunch = NCallback(self.launchPicker, cLand)
        menubar.add_command(label='Set Year', command=cLaunch.setFrameType)
        
        cLand = NCallback(self.pickLanding,
                         label='month',
                         setFrameType=self.set)
        cLaunch = NCallback(self.launchPicker, cLand)
        menubar.add_command(label='Set Month', command=cLaunch.setFrameType)
        
        cLaunch = NCallback(self.step,
                           label='delta_month')
        menubar.add_command(label='=>', command=cLaunch.setFrameType)
        
        cLaunch = NCallback(self.step,
                           label='delta_year')
        menubar.add_command(label='==>', command=cLaunch.setFrameType)
        
        cLand = NCallback(self.pickLanding, 
                         label='month',
                         setFrameType=self.set,
                         negative=True
                         )
        cLaunch = NCallback(self.launchPicker, cLand)
        menubar.add_command(label='=>>', command=cLaunch.setFrameType)
        
        kw = {'label':'delta_year','setFrameType': self.set, 'negative':True}
        cLand = NCallback(self.pickLanding, **kw)
        cLaunch = NCallback(self.launchPicker, cLand)
        menubar.add_command(label='==>>', command=cLaunch)
    def launchPicker(self, setFrameType, negative=False):
        root = Toplevel()
        Picker(root, setFrameType, negative=negative)
    def step(self, label=None, negative=False):
        if negative:
            a = -1
        else:
            a = 1
        kw = dict()
        kw[label] = a
        self.set(**kw)
    def pickLanding(self, val=None, label=None, 
                    setFrameType=None, negative=False):
        kw = dict()
        kw[label] = val
        kw['negative'] = negative
        setFrameType(**kw)
    def set(self, **kw):
        if 'year' in kw and kw['year'] is not None:
            self.year = kw['year']
        if 'month' in kw and kw['month'] is not None:
            self.month = kw['month']
            self.monthroll()
        if 'delta_month' in kw and kw['delta_month'] is not None:
            self.month = self.month + kw['delta_month']
            self.monthroll()
        if 'delta_year' in kw and kw['delta_year'] is not None:
            self.year = self.year + int(kw['delta_year'])
        self.refresh()
    def monthroll(self):
        if self.month <= 0:
            self.month = self.month + 12
            self.year = self.year - 1
        if self.month > 12:
            self.month = self.month - 12
            self.year = self.year + 1
    def refresh(self):
        self.makeCalendar()
        d = self.c
        for weekno in range(len(d)):
            week = d[weekno]
            for dayno in range(len(week)):
                day = week[dayno]
                try:
                    e = self.cells[weekno][dayno]
                    e.set(day[0])
                except IndexError:
                    pass
        self.makeTitle()
    def makeCalendar(self):
        try:
            return self.makeCal()
        except OverflowError:
            self.year = DEF_YEAR
            self.month = DEF_MONTH
            return self.makeCal()
    def makeCal(self):
        e = self.__makeCal__(year=self.year, month=self.month)
        
        self.c = e
        assert self.c is not None
        return self.c
    @staticmethod
    def __makeCal__(year=1, month=1):
        c = calendar.TextCalendar(calendar.SUNDAY)
        d = c.yeardays2calendar(int(year), ONE)
        if month.__class__ == int:
            try:
                return d[month-1][0]
            except IndexError:
                tryAgain(MONTH_ERR.format(month))
        elif month.__class__ == str and month in MONTHS.values():
            return d[NCalendarFrame.__get_key__(MONTHS, month)][0]
        elif NCalendarFrame.__int_test__(month):
            return d[int(month)-1][0]
        
        return d[month-1][0]
    @staticmethod
    def __int_test__(s):
        try:
            int(s)
            return True
        except:
            raise
            #return False
    @staticmethod
    def __get_key__(dict_, val):
        for i in dict_:
            if dict_[i] == val:
                return i
    def setFrameType(self, a):
        kw = dict()
        kw['day'] = a
        kw['month'] = self.month
        kw['year'] = self.year
        self.return_(**kw)
        self.parent.destroy()
    def onExit(self):
        pass
    def configwindow(self):
        pass
    def timetest(self):
        pass
    def makeTitle(self):
        self.monthroll()
        s = 'Calendar - {} {}'.format(MONTHS[self.month], self.year)
        self.parent.title(s)
class HeaderFrame(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.initUI()
    def initUI(self):
        col = 0
        for i in WEEKDAYS_SUN:
            z = Frame(self,
                      borderwidth=3,
                      relief=RAISED,
                      width=CELL_WIDTH,
                      height=HEADER_HEIGHT)
            Label(z, text=WEEKDAYS_SUN[i]).place(x=0, y=0)
            z.grid(row=0, column=col)
            col = col + 1
class CalendarCell(Frame):
    def __init__(self, parent, daynumber, return_):
        Frame.__init__(self, parent)
        self.return_ = return_
        self.daynumber = daynumber
        self.var = StringVar()
        self.initUI()   
    def initUI(self):
        self.config(height=CELL_WIDTH, 
                    width=CELL_WIDTH, 
                    borderwidth=1,
                    relief=RAISED,
                    )
        
        if self.daynumber == 0:
            self.blankCell()
        else:
            self.mainCell()
    def blankCell(self):
        self.config(background='gray')
    def labelCell(self):
        self.config(background=LABEL_BKGD)
    def mainCell(self):
        self.labelCell()
        s = '{}.'.format(self.daynumber)
        self.var.set(s)
        #printDebug('set ' + s)
        if not 'L' in self.__dict__:
            self.makeLabel()
    def makeLabel(self):
        L = Button(self, textvariable=self.var, command=self.setFrameType)
        L.place(x=0, y=0)
        L.config(background=LABEL_BKGD)
        self.L = L
    def setFrameType(self):
        self.return_(self.daynumber)
    def set(self, val):
        self.var.set(val)
        self.daynumber = val
        if val == 0:
            self.blankCell()
            if 'L' in self.__dict__:
                self.L.place_forget()
        else:
            self.mainCell()
            self.L.place(x=0, y=0)
            
if __name__ == '__main__':
    root = Tk()
    c = NCalendarFrame(root)
    c.mainloop()
    #printDictOfObject(c)
 