#===========================standard library imports==========================#
import sys
#from headless import OverridenNotImplementedError
#============================3rd party imports================================#
#================================local imports================================#
#==================================Dev Flags==================================#
DEBUG = True
EXCEPT_OVERRIDE = True
PRINT_AUTO = False
interrupt = True
double_override = False
#==================================Constants==================================#
SQL_display = False
ASSERT_OVERRIDE = False
AUTO_ECHO = True
ADMIN_OVERRIDE = True
RAISE_ON_TRUE = True
USE_ERR_STREAM = True
#=============================================================================#
BREAK_COMM = 'STOP'
INTERRUPT = 'Type {} to interrupt'.format(BREAK_COMM)
DICT_MISSING = '{} object has no attribute __dict__'
DICT_OF_OBJECT = 'dict Of Object'
MENU_PROMPT = 'choice->>'
TYPE_ERR_MSG = 'menu prompt requires {}, got {}'
    
TAB = '     '


auto_set = [
            ['Do you want to specify exact population? [Y/N]','y'],
            ['population:','100'],
            
            
            ['which city is this?','Maruvann'],
            ['Which species is dominant in Maruvann?','Human'],
            ['choice->>',0]
          ]
class Menu():
    def __init__(self, ordinal, menu_dict, exit=False):
        assert ordinal.__class__ == list
        assert menu_dict.__class__ == dict
    def exec_(self, *arg, **kw):
        if self.ordinal is not None:
            a = menu(self.ordinal, *arg, **kw)
        else:
            a = menu(self.options.keys(), exit_=self.exit_, *arg, **kw)
        try:
            b = self.menu_dicts[a]
        except KeyError:
            printDebug('No method available for "{}"')
        c = b(*arg, **kw)
        return c
def makeCaption(s, colon=True):
    #printDebug('MakeCaption="{}"'.format(s))
    a = s.split('_')
    b = ''
    for i in a:
        b = b + i.capitalize() + ' '
    b = b.strip(' ')
    
    #printDebug('MakeCaption="{}"'.format(b))
    if colon:
        c = '{}:'.format(b)
    else:
        c = b
    return c

def printDebug(s, override=False, interrupt_override=False):
    if interrupt and not DEBUG:
        return
    
    
    if override or DEBUG:
        print >> sys.stderr, s
    if interrupt and ((not interrupt_override) or double_override):
        try:
            t = str(raw_input(INTERRUPT))
        except ValueError:
            print 'error FATAL: sterr file closed'
            exit(1)
        if t == BREAK_COMM:
            runClear()
def printDictOfObject(o, exit_=True):
    assert False
    print o.__class__
    try:
        printDict(o.__dict__)
        if exit_:
            exit(DICT_OF_OBJECT)
    except AttributeError:
        printDebug(DICT_MISSING.format(o.__class__))
def printDict(di):
    for i in di:
            try:
                j = str(di[i])
            except:
                j = '<undefined>'
            print '{}: {}'.format(i, j)
def yn_prompt_loop(prompt='', override=False, tab=0, **kw):
    #printDebug('yn prompt loop')
    return prompt_loop('{} [Y/N]'.format(prompt),
                       'Please answer y or n',
                       yn_boolean_choice, 
                       override=override,
                       tab=tab,
                       **kw)
def yn_boolean_choice(c):
    if c == 'Y' or c == 'y':
        return True
    if c == 'N' or c == 'n':
        return False
    else:
        tryAgain('Y or N is required (not case sensitive)')
def menu_exec(options, ordinal=None, exit_=False, *arg, **kw):
    
    #assert ordinal is not None
    if ordinal is not None:
        a = menu(ordinal, *arg, **kw)
    else:
        a = menu(options.keys(), exit_=exit_, *arg, **kw)
    try:
        b = options[a]
    except KeyError:
        printDebug('No method available for "{}"'.format(a))
    c = b(*arg, **kw)
    
    
    return c
def menu(a, prompt=MENU_PROMPT, options=[], exit_=False, 
         override=False, default_value=None, title=None, tab=0):
    
    if prompt.__class__ != str:
        raise TypeError(TYPE_ERR_MSG.format(''.__class__, prompt.__class__))
    if (AUTO_ECHO or not PRINT_AUTO) and title is not None:
        printTabbedOverride(title, tab)
    if (AUTO_ECHO or not PRINT_AUTO):
        print_array(a, tab=tab)
    ex = len(a)-1
    for i in options:
        ex=ex+1
        if default_value is None and not PRINT_AUTO:
            menu_print(ex, i, tab=tab)
    if exit_ and default_value is None and not PRINT_AUTO:
        ex = len(a)
        menu_print(ex, 'exit', tab=tab)
    
    kw = {'ciel':ex,'max_val':-1}
    sel = prompt_loop(prompt,'Please enter a valid integer',
                       int_validate, 
                       input_type='int', override=override,
                       tab=tab,
                       **kw
                       )
    
    assert sel is not None
    
    if sel < len(a):
        return a[sel]
    else:
        return options[sel-len(a)]
def int_validate(i, ciel=0, floor=0, max_val=None):
    
    if i.__class__ != int:
        i = int(i)
    
    if max_val is not None and ciel == max_val:
        return i
    if floor <= i and i <= ciel:
        return i
    else:
        s = 'i={}\nciel={}\nfloor={}'.format(i, ciel, floor)
        raise IOError(s)
def menu_print(i, el, tab=0):
    print '{}[{}] {}'.format(getTab(tab), i, el)   
def print_array(a, tab=0):
    for i in range(len(a)):
        menu_print(i, a[i], tab=tab)
def prompt_loop_null(prompt, **kw):
    return prompt_loop(prompt, '', None, **kw)
def getTab(index):
    t = ''
    for i in range(index):
        t = '{}{}'.format(t, TAB)
    #printDebug('tab={}'.format(t))
    return t
def __buildcom__(tab, input_type, prompt):
    return '{}(raw_input("{}{} "))'.format(input_type, getTab(tab), prompt)
def prompt_loop(prompt, err_response, f, input_type='str', 
                override=False, default_value=None, tab=0, 
                force_loop=False, **kw):
    if tab.__class__ != int:
        raise TypeError('Integer required for tab index')
    if f is None:
        f = passthrough
    
    global debug
    if default_value is not None and DEBUG:
        return f(default_value, **kw)
    
    if PRINT_AUTO:
        pa = prompt_automatic(prompt, tab=tab, override=override)
        if pa is not None:
            return f(pa, **kw)
        else:
            command = __buildcom__(tab, input_type, prompt)
    else:
        command = __buildcom__(tab, input_type, prompt)
    return __loop__(command, f, err_response, **kw)
def tryAgain(msg=None):
    """Raises an exception that is designed to be caught
    by the promptLoop method, causing the loop to repeat.
    """
    a = Exception()
    a.override = not RAISE_ON_TRUE
    if msg is not None:
        a.msg = msg
    raise a
def runClear(msg=None):
    """Raises an Exception that is designed to pass directly
    through the promptloop method, causing it to propagate to
    the parent methods and deliberately crash the program.
    """
    a = Exception()
    a.override = RAISE_ON_TRUE
    if msg is not None:
        a.msg = msg
    raise a
def parseOut(e):
    ov = None
    msg = None
    
    if 'override' in e.__dict__:
        ov = e.override
    if 'msg' in e.__dict__:
        msg = e.msg
    
    return ov, msg
def __loop__(command, f, err_response, **kw):
    """Runs the given function in a loop that will keep going if
    an exception is raised and only break if the f function completes
    successfully.
    
    exceptions without an override in place will pass through and crash, but
    if the override is set to {} it will loop around.
    """.format(not RAISE_ON_TRUE)
    while True:
        try:
            i = eval(command)
            return f(i, **kw)
        except ValueError:
            printDebug('Integer required.')
        except SystemExit:
            raise
        except Exception as e:
            if ASSERT_OVERRIDE:
                raise
            override, msg = parseOut(e)
            if msg is None:
                msg = err_response
            if override is None:
                raise
            else:
                raiseOnTrue(override)
                if USE_ERR_STREAM:
                    printDebug(msg, override=True, interrupt_override=True)
                else:
                    print msg
def raiseOnTrue(b):
    if b == RAISE_ON_TRUE:
        raise
def exceptionParse(f, command, err_response, **kw):
    assert False
    try:
        i = eval(command)
        return f(i, **kw)
    except ValueError:
        printDebug(err_response, override=True)
    except SystemExit:
        raise
    except NotImplementedError as e:
        if 'ignore' in e.__dict__ and e.ignore:
            printDebug(err_response, override=True)
        else:
            raise
    except TypeError:
        raise
    except AssertionError as e:
        a = 'ignore' in e.__dict__ and e.ignore
        b = 'override' in e.__dict__ and e.override
        if a or b:
            printDebug(err_response, override=True)
        else:
            raise
    except:
        printDebug(err_response, override=True)
def passthrough(a):
    return a
i=0
def prompt_automatic(s, tab=0, override=False):
    #assert not '_' in s
    if not override:
        assert not '_' in s
    
    
    global i
    len_a = len(auto_set)
    within_bounds = i < len_a
    if within_bounds:
        compared = auto_set[i][0] == s
    else:
        compared = False
    
    if PRINT_AUTO and i < len(auto_set) and auto_set[i][0] == s:
        a = auto_set[i]
        lena = len(a)
        
        if lena == 2:
            t = a[1]
        elif lena == 1:
            t = ''
        else:
            printDebug(a)
            raise IOError
        i = i + 1
        
        if AUTO_ECHO:
            #tb = getTab(tab)
            #if s == 'time->':
                #printDebug('type="{}"'.format(tb))
            print '{}{} {}'.format(getTab(tab), a[0], t)
            #print '{} {}'.format(a[0], t)
        return t
    elif within_bounds and not compared:
        #printDebug('Kayaba')
        printDebug('expected "{}" recieved "{}"'.format(auto_set[i][0], s),
                   interrupt_override=True)
        return None
    else:
        #printDebug('Asuna')
        return None
def print_automatic(s, override=False):
    #print s
    if AUTO_ECHO or override:
        print s
def printTabbed(self, s, index=0, *args, **kw):
    t = getTab(index) + s
    print_automatic(t, *args, **kw)
def printTabbedOverride(s, index=0, tab=0):
    if index == 0 and tab !=0:
        index = tab
    t = getTab(index) + s
    print_automatic(t, override=True)
def printClass(o):
    printDebug('{} {}'.format(o.__class__, o))
    
    
    
    
    