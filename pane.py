#===========================standard library imports==========================#
from Tkinter import PanedWindow, VERTICAL, BOTH, Label, Frame
#============================3rd party imports================================#
#================================local imports================================#
#==================================Constants==================================#

#=============================================================================#
class WorkPaneBin(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        m = PanedWindow(self, orient=VERTICAL)
        m.pack(fill=BOTH, expand=1)
        
        top = Label(m, text='top pane')
        m.add(top)
        
        bottom = Label(m, text="bottom pane")
        m.add(bottom)


class WorkPane(PanedWindow):
    def __init__(self, parent, *arg, **kw):
        PanedWindow.__init__(parent, *arg, **kw)
        
        