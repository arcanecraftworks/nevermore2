#===========================standard library imports==========================#
from ttk import OptionMenu, Menubutton
import Tkinter
#============================3rd party imports================================#
#================================local imports================================#
from cli_tools import printDebug
from Nevermore import NCallback
#==================================Constants==================================#
def stub(*arg, **kw):
    pass
class ActiveOptionMenu(OptionMenu):
    pass
class ActiveOptionMenu2(OptionMenu):
    #def __init__(self, *arg, **kw):
    #    OptionMenu(self, *arg, **kw)
    #    self.f = kw.pop('command', None)
    def __init__(self, master, variable, default=None, *values, **kwargs):
        """Construct a themed OptionMenu widget with master as the parent,
        the resource textvariable set to variable, the initially selected
        value specified by the default parameter, the menu values given by
        *values and additional keywords.

        WIDGET-SPECIFIC OPTIONS

            style: stylename
                Menubutton style.
            direction: 'above', 'below', 'left', 'right', or 'flush'
                Menubutton direction.
            command: callback
                A callback that will be invoked after selecting an item.
        """
        kw = {'textvariable': variable, 'style': kwargs.pop('style', None),
              'direction': kwargs.pop('direction', None)}
        Menubutton.__init__(self, master, **kw)
        self['menu'] = Tkinter.Menu(self, tearoff=False)

        self._variable = variable
        self._callback = kwargs.pop('command', None)
        print 'callback={}'.format(self._callback)
        
        yn = str(raw_input('interrupt'))
        if yn == 'y':
            assert False
        
        
        if kwargs:
            raise Tkinter.TclError('unknown option -%s' % (
                kwargs.iterkeys().next()))

        self.set_menu(default, *values)


    def set_menu(self, default=None, *values):
        #printDebug('Suguha')
        """Build a new menu of radiobuttons with *values and optionally
        a default value."""
        menu = self['menu']
        menu.delete(0, 'end')
        #exit('active')
        for val in values:
            #exit('{}'.format(self._callback))
            menu.add_radiobutton(label=val,
                command=Tkinter._setit(self._variable, val, self._callback))

        if default:
            self._variable.set(default)


    