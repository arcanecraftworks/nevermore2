
import sys
import os
#import debug

from sqlalchemy.exc import ProgrammingError
from sqlalchemy.exc import IntegrityError
from psycopg2 import IntegrityError as psycoIntegrityError

import file_utils as futils
from database import utils as db
from database import bootstrap
from Nevermore.cli_tools import printDebug
macro = False
SQL_display = False


NVM_ROOT = os.path.dirname(__file__)
sys.path.append(NVM_ROOT)
db.setProjectHome(NVM_ROOT)
DEFAULT_LOAD_DIR = os.path.join('core03','static tables')
#import tcs
debug = False
g_override = True
debug_override = True
recursive_entry = True
transaction_override = True
engine = None
front_tables = []
#db_choices = 'SQLite3 in Memory','MySQL','PostgreSQL'
tablename = None
build_override = False
g_session = None

#entry_tables = dict()

def print_err(s, override=False):
    debug.printDebug(s, override=override)
def new_config():
    choice = menu(db.dbm_systems, 'Which Database Management System (DBMS) do you want to use?')
    
    if choice == 0:
        testdb()
    elif choice == 1:
        alias = str(raw_input('Configuration Alias->'))
        host = str(raw_input('IP Address/hostname-> '))
        port = str(raw_input('port-> '))
        u = str(raw_input("username-> (Type 'None' to specify user at access time)"))
        
        d = str(raw_input("schema-> "))
        #off = prompt_loop_eval('Offline mode creates a text file backing up all of the python commands issued by the database, so they can be re-issued. Activate offline?[Y/N]',
        #                       'Please answer y or n',e='yn_boolean_choice(i)')
        data = db.DBConfig(alias=alias, username=u, dbmanagement=choice, portnumber=port, hostname=host, schemaname=d, offlineMode=False)
        db.coldbs.append(data)
        futils.write_config(data)
        print 'New configuration saved. establishing connection...'
        db.engine_configObj(data)
        prepdb(data)
        
        
        return data
    else:
        return
def modSelect():
    db.create_all()
    core = bootstrap.nvMaps(name='core', maps_file = db.DEFAULT_CORE, static_load_directory=DEFAULT_LOAD_DIR)
    session = db.open_session()
    session.add(core)
    core = db.nImport(db.DEFAULT_CORE, 'core')
    #TODO: unwind hard-code for module selection.
    if yn_prompt_loop("Do you want to add Dungeonmasters' Mod to this database?"):
        dm = bootstrap.nvMaps(id=2, name="Dungeonmaster's Mod", maps_file = 'dungeon_core02', static_load_directory=os.path.join('dungeonmaster','static tables'))
        session.add(dm)
        import database.dungeon_core02 as dc
        dc = dc
    if yn_prompt_loop("Do you want to add Silverscreen's Mod to this database?"):
        ss = bootstrap.nvMaps(name="Silverscreen Mod", maps_file = 'silverscreen_core')
        session.add(ss)
        import database.silverscreen_core as sc
        sc = sc
    session.commit()
    session.close()
    db.create_all()
    futils.restore_static()
def prepdb(dbconfig):
    
    yn = prompt_loop('set up {} as a new database? (any information in the datbase will be lost) press N if accessing an existing installation. [Y/N]'.format(dbconfig.schema),'Please answer y or n', yn_boolean_choice)
    if yn:
        yn2 = prompt_loop('deleting all data, confirm? [Y/N]', 'Please answer y or n', yn_boolean_choice)
    if yn and yn2 and dbconfig.dbms != 2:
        if dbconfig.user == 'None':
            dbconfig.user = str(raw_input("user-> "))
        stri = str(raw_input('password for user {}->> '.format(dbconfig.user)))
        db.engine_configObj(dbconfig, password=stri)
        db.refresh_postgres(dbconfig)
    elif dbconfig.dbms == 2:
        stri = str(raw_input('password for user {}->> '.format(dbconfig.user)))
        db.engine_configObj(dbconfig, password=stri)

        modSelect()
        db.create_all()
def testdb():
    import database.dungeon_core
    db.engine_config(0)
    db.create_all()
def getSchema(coldb, password):
    coldb.schema = 'nevermore'
    db.engine_configObj(coldb, password)
    engine = db.engine
    coll = ['tcsmdb','valkyrie','shadowmoor']
    i = menu(coll)
    if i == len(coll):
        return None
    else:
        return coll[i]
    exit('getSchema exit')
def cli_access():
    global debug
    print debug.nevermore_version
    print("Welcome to the Nevermore Command Line Interface.\n")
    futils.read_config()
    i = 0
    for j in db.coldbs:
        menu_print(i, j.alias)
        i = i + 1
    menu_print(i, 'new config')
    if macro:
        default_value = 3
    else:
        default_value = None
    choice = prompt_loop_eval('Which database do you want to use?','Please enter an integer from the list.','int_validate(i, {})'.format(i), input_type='int', default_value=default_value)
    if choice == 0:
        username = str(raw_input('username-> '))
        password = str(raw_input('password-> '))
        
        c = db.coldbs[0]
        c.user = username
        c.schema = getSchema(c, password)
        db.engine_configObj(c, password)
        #db.initTables()
    elif choice == 1:
        testdb()
    elif choice == i:
        c = new_config()
        #TODO: troubleshoot sqlite3
    else:
        new = False
        c = db.coldbs[choice]
        if c.user == 'None':
            c.user = str(raw_input('username-> '))
        if not macro:
            password = str(raw_input('password for user {0}->>'.format(c.user)))
        else:
            password = 'Telerion3017'
        db.engine_configObj(c, password)
    
    try:
        db.initTables()
    except ProgrammingError:
        modSelect()
        db.initTables()
    db_menu() 
def menu_print(i, el):
    print('[' + str(i) + '] ' + str(el))   
def print_array(a):
    for i in range(len(a)):
        menu_print(i, a[i])
def table_check(inp, g_session, override=False):
    #we need a bona-fide classname
    ver_tablename = None
    inCap = inp.capitalize()
    inp = inp.lower()
    printDebug('inCap = ' + inCap)
    printDebug('inp = ' + inp)
    
    
    for t in db.nTables:
        nTable = db.nTables[t]
        if inCap == nTable.classname or inCap == nTable.name or inCap == nTable.getNameSingular():
            ver_tablename = nTable
        if inp == nTable.classname or inp == nTable.name or inCap == nTable.getNameSingular():
            ver_tablename = nTable
    
    if ver_tablename is not None:
        return ver_tablename
    else:
        raise IOError('input error - table not recognized')
def table_validate(inp, g_session, override=False):
    #printDebug('table_validate start')
    #printDebug('inp = {}'.format(inp.lower()))
    if inp.lower() == 'exit' or inp.lower() == 'menu':
        return 'exit'
    #we need a bona-fide classname
    nTable = table_check(inp, g_session, override=override)
    
    if override:
        agnostic_entry(nTable, g_session, display_full=True)
    else:
        try:
            agnostic_entry(nTable, g_session, display_full=True)
        except:
            print('internal agnostic error')
def data_entry(modify=False):
    while True:
        printDebug('data entry - debug loop')
        global g_session
        g_session = db.open_session()
        
        global tablename
        if tablename is None and not modify:
            print_tables()
            ex = prompt_loop_eval('table->> ',"Please enter a valid table name. 'exit' or 'menu' will return you to the previous menu.",
                                  'table_validate(i, g_session, override=True)', override=True)
        elif not modify:
            agnostic_entry(tablename, g_session)
        else:
            ex = modifyRecord()
        if ex == 'exit':
            return False
        else:
            cont_choices = 'save and new','save and exit','save and switch tables','rollback and new'
            i = menu(cont_choices)
            yn = cont(i, g_session)
            #print_err('yn: ' + str(yn))
            if not yn:
                return False      
def cont(i, session):
    cont_choices = 'save and new','save and exit','save and switch tables','rollback and new'
    global tablename
    if i == 0:
        db.trans_commit(session, transaction_override)
        return True
    if i == 1:
        db.trans_commit(session, transaction_override)
        tablename = None
        return False
    if i == 2:
        db.trans_commit(session, transaction_override)
        tablename = None
        return True
    if i == 3 or i == 4:
        
        if len(session.dirty) + len(session.new) != 0:
            yn = prompt_loop('This will roll back your most recent entry. confirm? [Y/N]',
                             'Please enter Y or N',yn_boolean_choice, override=True)
        else:
            yn = True
        
        if yn:
            session.rollback()
            session.close()
        if i == 3:
            return True
        if i == 4:
            tablename = None
            return False
    else:
        exit('unreachable')
def menu(a, prompt='choice->>', options=[], exit=True, override=False, default_value=None):
    print_array(a)
    ex = 0
    for i in options:
        ex=ex+1
        menu_print(ex, i)
    if exit:
        ex = len(a)
        menu_print(ex, 'exit')
    else:
        ex = ex-1
    return prompt_loop_eval(prompt,'Please enter a valid integer','int_validate(i, {}, max_val=-1)'.format(ex), input_type='int', override=override, default_value=default_value)
def int_validate(i, ciel, floor=0, max_val=None):
    if max_val is not None and ciel == max_val:
        return i
    if i <= ciel and floor <= i:
        return i
    else:
        print 'i: ' + str(i)
        print 'ciel: ' + str(ciel)
        raise Exception('input error')
def print_tables():
    #tables = db.get_tables()
    nTables = db.nTables
    for t in nTables:
        nTable = nTables[t]
        exec('import database.{0} as {0}'.format(nTable.mod_path))
        if nTable.mClass.__tabletype__ == 'front':
            s = nTable.mClass.name_singular
            print nTable.getNameSingular()
def prompt_loop_eval(prompt, err_response, e, input_type='str', override=False, session=None, default_value=None):
    #override=True
    command = '{}(raw_input("{} "))'.format(input_type, prompt)
    global debug
    if default_value is not None and debug:
        i = default_value
        return eval(e)
    if override and debug:
        i = eval(command)
        return eval(e)
    else:
        while True:
            if override:
                #printDebug(command, override=True)
                i = eval(command)
                return eval(e)
            else:
                try:
                    i = eval(command)
                    return eval(e)
                except:
                    print err_response
def promptLoop(prompt, err_response, findLevelNo, input_type='str', override=False, session=None, default_value=None, *args, **kwargs):
    command = 'str(raw_input("{} "))'.format(prompt)
    global debug
    if default_value is not None and debug:
        i = default_value
        return findLevelNo(i)
    if override and debug:
        printDebug(command)
        i = eval(command)
        return findLevelNo(i, args, kwargs)
    else:
        while True:
            try:
                i = eval(command)
                return findLevelNo(i, *args, **kwargs)
            except:
                printDebug(err_response, override=True)
def prompt_loop(prompt, err_response, findLevelNo, input_type='str', override=False, session=None, default_value=None):
    global debug
    if default_value is not None and debug:
        return findLevelNo(default_value)
    command = '{}(raw_input("{} "))'.format(input_type, prompt)
    if override and debug:
        #print_err('prompt overridden')
        i = eval(command)
        return findLevelNo(i)
    else:
        while True:
            try:
                #print_err('inner loop')
                i = eval(command)
                return findLevelNo(i)
            except:
                printDebug(err_response, override=True)
def stub(i):
    return i
def yn_boolean_choice(c):
    if c == 'Y' or c == 'y':
        return True
    if c == 'N' or c == 'n':
        return False
    else:
        raise Exception('choice')
def modifyRecord():
    global g_session
    table = prompt_loop_eval('table->','Table name not recognized',e='table_check(i, g_session)')
    ident = prompt_loop('which field will you use to identify your entry [id/name/title]','invalid input',findLevelNo=idcheck)
    print_err('ident = ' + ident)
    print_err('table = ' + table)
    row = prompt_loop_eval('{}->'.format(ident),'row not found',e="rowCheck(i, '{0}','{1}')".format(ident, table), override=True)
    entry = prompt_loop_eval('field name->','field name not recognized.',e='entryCheck("{}", i)'.format(table))
    field = eval('row.{}'.format(entry.name))
    print 'existing value: ' + field
    new_val = str(raw_input('new value-> '))
    eval('row.{0} = {1}'.format(entry.name, new_val))
    return
def entryCheck(classname, fieldname):
    return eval('dbmaps.{0}.{1}'.format(classname, fieldname))
def idcheck(i):
    if i == 'id' or i == 'ID' or i == 'Id':
        return 'id'
    elif i == 'name' or i == 'Name':
        return 'name'
    elif i == 'title' or i == 'Title':
        return 'title'
def db_menu():
    while True:
        items = 'Command Interpreter','Data Entry','Modify existing records','backup database to {}'.format(futils.BACKUP_FILETYPE),'restore_config from {}'.format(futils.BACKUP_FILETYPE)
        if not macro:
            pick = menu(items, override=True)
        else:
            printDebug('db_menu overridden by macro')
            pick = 0
        #pick = menu(items, override=True)
        if pick == len(items):
            return
        elif pick == 0:
            command_loop()
        elif pick == 1:
            data_entry()
        elif pick == 2:
            data_entry(modify=True)
        elif pick == 3:
            futils.backup()
        elif pick == 4:
            futils.restore_config()
        else:
            print 'not yet supported.'       
def yn_prompt_loop(prompt='', override=False):
    #print_err( 'yn_prompt_loop')
    return prompt_loop('{} [Y/N]'.format(prompt),'Please answer y or n',yn_boolean_choice, override=override)
def command_loop():
    session = db.open_session()
    if macro:
        macro_f()
        exit()
    while True:
        command = str(raw_input(">>> "))
        if command == "exit":
            session.commit()
            session.close()
            return
        elif command == "die":
            exit()
        elif command == "menu":
            session.commit()
            session.close()
            db_menu()
        else:
            exec(command)
def remove_id(stri):
    return stri.rsplit('_id')
def parse_table(stri):
    table = stri
    classname = db.getClassName(table)
    ttype = eval('{}.__tabletype__'.format(classname))
    d = dict()
    d[table] = ttype
    return d
def printObj(obj, indent=0):
    stri = ''.ljust(4*indent)
    print '{}{}'.format(stri, obj.__class__)
    printDict(obj.__dict__, indent=indent+1)
def printDict(d, indent=0):
    stri = ''.ljust(4*indent)
    for i in d:
        print '{}{}: {}'.format(stri, i, d[i])
def col_entry(col, parent_table):
    #print_err('col entry - ' + str(col))
    colObj = debug.ColObj(col)
    seth = col.foreign_keys
    prim = col.primary_key
    if seth == set() and not prim:
        #print_err('section 1')
        if col.type == 'INTEGER':
            #print_err('prompt loop start')
            prompt_loop_eval('{}: '.format(col.name), 'Please enter a valid integer', 'stub(i)', input_type='int', override=True)
            #print_err('prompt loop end')
        else:
            c = str(raw_input(col.name + ': '))
        stri = '{}="{}", '.format(col.name, c)
        colObj.select_string = stri
        colObj.reltype = 'norel'
        #print_err('section 1 end')
    elif not prim:
        #print_err('section two')
        colObj.fk = db.fkSelect3(col)
    
    #print_err('col exit')        
    return colObj
def relAbsVal(i):
    if i == 'absolute' or i == 'Absolute':
        return True
    elif i == 'relative' or 'Relative':
        return False
    else:
        raise Exception('input error')
def sessionF(findLevelNo, session=None, override=False):
    trip = False
    if session is None:
        trip = True
        session = db.open_session()
    
    r = findLevelNo(session)
    if trip and override:
        session.commit()
        session.close()
    elif trip and not override:
        try:
            session.commit()
            session.close()
        except:
            printDebug('Commit Unsuccessful', override=True)
def transcommit(session):
    try:
        session.commit()
        session.close()
    except:
        print 'Error in committing to database. changes will be rolled back instead.'
        session.rollback()
        session.close()
def addForeignKeys(t, columns):
    print_err('add foreign keys')
    #print_err('len = ' + str(len(columns)))
    for c in columns:
        #print c
        if c.col_orig != '' and c.assoc is None:
            exec('t.{0} = c.fk'.format(c.col_orig))
        elif c.col_orig != '' and c.assoc is not None:
            a = c.assoc()
            a.child = c.fk
            s = 't.{}.append(a)'.format(c.col_orig)
            print_err(s)
            exec(s)
def rels(nTable, columns):
    assert nTable.__class__ == db.nvmTable().__class__
    classname = nTable.classname
    #print_err('rels begin')
    s = 'from database.{} import {}'.format(nTable.mod_path, nTable.classname)
    #printDebug(s)
    exec(s)
    b = eval("'rels' in {}.__dict__".format(classname))
    if b:
        relist = eval('{}.rels'.format(classname))
    else:
        return
    for rel in relist:
        if db.designate(rel, classname):
            destination = relist[rel].__associationchild__
            colObj = debug.ColObj(rel, fk=db.selectID(destination), assoc=relist[rel], col_orig=rel)
            columns.append(colObj)
def agnostic_entry(nTable, session=None, display_full=False):
    print_err('agnostic entry')
    
    
    
    print nTable.__class__
    try:
        printDebug(nTable.sTable)
    except AttributeError:
        nTable = db.nTables[nTable]
        printDebug(nTable.__class__)
        printDebug(nTable.sTable)
    table = nTable.sTable.name
    classname = nTable.classname
    if table=='exit' or table == 'menu':
        return 'exit' 
    if session == None:
        session = db.open_session()
    
    
    n = db.find_columns(nTable)
    global tablename
    tablename = table
    
    if display_full:
        db.select_star(classname)
    
    print ('new {}!'.format(nTable.mClass.name_singular))
    
    exec('import database.{}'.format(nTable.mod_path))
    if 'build' in nTable.mClass.__dict__:
        return eval('clibuilder.{}(session=session)'.format(nTable.mClass.build))
    else:
        stri = ""
        columns = []
        if n is None:
            print 'table name not recognized'
            return
        for col in n:
            #print col
            colObj = col_entry(col, table)
            stri += colObj.select_string
            columns.append(colObj)
        
        tsub = 'nTable.mClass({kwarg})'.format(kwarg=stri)
        t = eval(tsub)
        session.add(t)
        rels(nTable, columns)
        addForeignKeys(t, columns)
        return t
def copy_pc_class():
    session = db.open_session()
    for i in session.query(db.getClass('Classes')):
        if i.pc_class == 0:
            i.pc_classb = False
        else:
            i.pc_classb = True
    session.commit()
    session.close()
def clean_all():   
    session = db.open_session()
    
    for t in db.nTables:
        nTable = db.nTables[t]
        for row in session.query(db.getClass(nTable.classname)):
            for d in nTable.sTable.c:
                if str(d.type) == 'INTEGER' or str(d.type) == 'DATETIME' or str(d.type) == 'BOOLEAN':
                    pass
                else:
                    printDebug(d.type)
                    
                    cCol = getattr(row, d.name)
                    if cCol is not None:
                        cCol = cCol.lstrip()
    session.commit()
    session.close()
def addNVMTable():
    pass
    #TODO: finish?
def structure_pop_mix():
    mixtypes = dict()
    
    specs = []
    session = db.open_session()
    for row in session.query(db.getClass('popmix')):
        specs.append(row)
        print row
    
    
    mixtypes = []
    with open(os.path.join(NVM_ROOT, 'database','dungeonmaster','static tables','mixtype.csv'), 'r') as findLevelNo:
        headers = None
        for line in findLevelNo:
            
            line2 = line.rstrip('\n')
            a = line2.split(',')
            
            if headers is None:
                headers = []
                for i in a:
                    headers.append(i.strip())
                #headers = a
                print 'headers = ' + str(headers)
            else:
                
                mt = db.getClass('mixtype')(id=a[0], dom_spec_id=a[1], mix_type=a[2])
def resetName():
    db.create_all()
    futils.restore_static()       
    exit('restore complete')
def town(name, id=None):
    return db.getClass('Towns')(name=name, id=id)
def makeTown(town_name, session, id=None):
    t = town(town_name, id=id)
    session.add(t)
    session.commit()
def townOfBeginnings(override=False):
    session = db.open_session()
    if override:
        makeTown(session)
    else:
        try:
            makeTown('Town of Beginnings', session, id=1)
        except IntegrityError, psycoIntegrityError:
            printDebug('Town of Beginnings not imported')
        finally:
            session.close()
def macro_f():
    db.create_all()
    print('macro')
    futils.restore_config()
    print('...macro end')

if __name__== '__main__':
    #ACTION NO LONGER ALLOWED
    assert False
    #tables = db.getTableObj('Alignments')
    #exit()
    cli_access()
