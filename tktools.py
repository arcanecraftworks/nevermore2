#===========================standard library imports==========================#
from Tkinter import Tk
#============================3rd party imports================================#
#================================local imports================================#
#==================================Constants==================================#
#=============================================================================#

def top_window():
    root2 = Tk()
    lift(root2)
    return root2

def lift(root, temp=False):
    root.attributes("-topmost", True)
    if temp:
        root.attributes("-topmost", False)    