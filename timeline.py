#===========================standard library imports==========================#
import sys
from Tkinter import Canvas, Frame, Tk, Label, Radiobutton, IntVar

from Tkconstants import NE, SW, ALL, E, SUNKEN, DISABLED, ACTIVE
from ttk import Button
from mouse import Mouse
#============================3rd party imports================================#
#================================local imports================================#
import database.utils as utils
import database.query as query
#import database
#from cli_tools import printDebug
from TimeDetails import TimeDetails
from cascadeMenu import CascadeMenu

from MessageBox import onWarn
from scenelist import *
from Scale import EpochScale
#from Nevermore import lift
#==================================Constants==================================#

AUTO_OPTIONS = False

PIXEL_LEN = 7
LEVELS = 5, 10, 20, 40, 50, 70
LEVEL_LIMIT = 6

COLORS = {'seconds':'red',
         'minutes':'goldenrod',
         'hours':'green',
         'days':'forest green',
         'months':'blue',
         'years':'purple',
         'epochs':'black'}


#TIME = {'hours':'hour','minutes':'minute','seconds':'second'}

EPOCH_ERR = 'Timeline cannot display multiple epochs at this point'



TIMELINE_OPTIONS_TITLE = 'Timeline Options'
#=============================================================================#



class Timeline(Canvas):
    def __init__(self, parent, *arg, **kw):
        Canvas.__init__(self, parent, *arg, **kw)
        self.parent = parent
        self.borderwidth = kw['borderwidth']
        Mouse(self, motion_callback=self.moCall)
        #self.tick_info = dict()
        self.refreshIndicator()
        self.min_time = None
        self.max_time = None
    def screenWidth(self):
        return self.parent.winfo_screenwidth()
    def moCall(self, event):
        x = event.x - BORDER_X
        
        sw = self.screenWidth() - (BORDER_X*2)
        if 0 < x and x < sw and 'max_scale' in self.__dict__:
            if self.max_scale.key != 'None':
                time = self.getTime(self.max_scale, x)
                self.refreshIndicator(s=time, x=x)
            self.refreshSceneIndicator()
    
    def initialize(self, scenes_mode=True):
        query.setEpic()
        if scenes_mode:
            self.defaultAllScenes()
        self.init2()
    def dispLimit(self):
        #printDebug(s)
        printDebug('min_={}'.format(self.min_time))
        printDebug('max_={}'.format(self.max_time))
    def drawEdges(self, sw):
        """Draws a bounding box for the timeline
        """
        self.tick(0)
        self.create_line(BORDER_X, CORNER_Y, sw, CORNER_Y)
        #printDebug(BORDER_X)
        
        self.tick(sw-BORDER_X)
    def tick(self, X, level=MAX_LEVEL, color='black', text=None):
        """Does one thing and one thing very well - draws a tick mark on the timeline
        """
        
        X = BORDER_X + X
        Y = CORNER_Y - level
        self.create_line(X, Y, X, CORNER_Y, fill=color)
        self.create_text(X, Y, text=text, anchor=SW)
    def init2(self):
        #printDebug('init2', interrupt_override=True)
        sw = self.parent.winfo_screenwidth()-BORDER_X
        self.drawEdges(sw)
        if self.blank:
            return
        
        self.scale = EpochScale(self, sw-BORDER_X, self.min_time, self.max_time)
        #self.scale.month = 0
        delta = self.scale.delta
        
        if 'scene_list' in self.__dict__:
            scene_list = self.scene_list
        else:
            scene_list = []
        
        for i in scene_list:
            #printDebug('i={}'.format(i))
            i.run_q(self.scale.min_time, self.scale.max_time)
            
        #printDebug('min={} max={}'.format(self.scale.min_time, 
        #                                  self.scale.max_time))
        prev_time = self.scale.min_time
        for i in self.scale.step_range:
            next_time = self.scale.getTime(i)
            
            time_range = prev_time, next_time
            self.scale.disp_step(i, *time_range)
            
            for scenelist in scene_list:
                
                scenelist.disp_step(i, *time_range)
            prev_time = next_time
        #printDebug('scale delta={} last delta={}'.format(self.scale.delta,
        #                                                 self.scale.mid_delta))
        #printDebug(prev_time)
        self.refreshIndicator()
        self.sc_ind = self.create_text(sw, self.borderwidth+TEXT_DEPTH,
                                       text=self.deltaPrint(delta), 
                                       anchor=NE)
        if AUTO_OPTIONS:
            printDebug('Macro enabled options launch', interrupt_override=True)
            self.startLaunch()
    def deltaPrint(self, delta):
        YEAR_MOD = 365
        years = delta.days // YEAR_MOD
        days = delta.days % YEAR_MOD
        if delta.days == YEAR_MOD:
            return delta
        else:
            return '{} years, {} days, {}'.format(years, 
                                                  days, 
                                                  self.__dt__(delta))
    def __dt__(self, delta):
        
        hour = delta.seconds // 3600
        seconds = delta.seconds % 3600
        minutes = seconds // 60
        seconds = seconds % 60
        
        hour = str(hour).zfill(2)
        minutes = str(minutes).zfill(2)
        seconds = str(seconds).zfill(2)
        
        return '{}:{}:{}'.format(hour, minutes, seconds)
    def refreshIndicator(self, s='00:00:00', x=0):
        sw = self.screenWidth() - BORDER_X
        if 'main_ind' in self.__dict__:
            self.delete(self.main_ind)
        self.main_ind = self.create_text(sw, self.borderwidth, 
                                        text=s, anchor=NE)
    def refreshSceneIndicator(self, s='Placeholder!', x=0):
        sw = self.screenWidth() - BORDER_X
        if 'sc_ind' in self.__dict__:
            self.delete(self.sc_ind)
            
        KEY = 'Scenes'
    def resetLevels(self):
        self.levels = dict()
        for i in range(LEVEL_LIMIT):
            self.levels[i] = 0
    def findLevelNo(self, x, text):
        if not 'levels' in self.__dict__:
            self.resetLevels()
        for i in self.levels:
            lev = self.levels[i]
            #printDebug('lev={}, x={}'.format(lev, x))
            if lev < x:
                self.levels[i] = x + self.findSize(text)
                #printDebug('returning={}'.format(i))
                return i
        return None
    def findSize(self, text):
        return len(text) * PIXEL_LEN        
    
    #def tickTime(self, prev, time, i, t, x, max_scale): 
    #    printDebug('prev={}'.format(prev), interrupt_override=True)
    #    printDebug('time={}'.format(time), interrupt_override=True)
    #    printDebug('i={}'.format(i), interrupt_override=True)
    #    printDebug('t={}'.format(t), interrupt_override=True)
    #    printDebug('x={}'.format(x), interrupt_override=True)
    #    printDebug('max_scale={}'.format(max_scale))
    #    
    #    time_t = getattr(time, DATE[i])
    #    prev_t = getattr(prev, DATE[i])
    #    if i in t and time_t != prev_t:
    #        order_id = ORDER.index(i)
    #        max_index = ORDER.index(max_scale.key)
    #        level_index= order_id - max_index
    #        
    #        assert level_index < len(LEVELS)
    #        
    #        if level_index < 0:
    #            return
    #        
    #        if level_index == DISP_LEV:
    #            text = self.getText(i, time_t)
    #        else:
    #            text = None
    #        self.tick(x, 
    #                  level=LEVELS[level_index], 
    #                  color=COLORS[i],
    #                  text=text)
    def getText(self, scale_key, numeral, secondary=''):
        
        months_dict = sys.modules['Calendar'].MONTHS
        
        #assert False
        if scale_key == 'years':
            return numeral
        elif scale_key == 'months':
            m = months_dict[numeral]
            self.scale.month_i = m
            return m
        elif scale_key == 'days':
            return '{} {}'.format(utils.textNumeral(numeral), 
                                  months_dict[self.scale.month_i])
        else:
            return numeral
    def defaultAllScenes(self):
        kw = {}
        kw['color'] = 'red'
        kw['query'] = query.classQ('Scenes')
        kw['choice'] = 'All Scenes'
        a = utils.process('Scenes', query=kw['query'], for_each=self.loop)
        if len(a) == 0:
            self.blank = True
        else:
            self.blank = False
        #self.trip = False
        self.scene_list = [SceneList(self, **kw)]
    def chrono_order(self, el):
        #TODO: account for epoch
        return el.time.date_time
    def loadEpochs(self):
        self.offsets = []
        self.prev_years = 0
        self.epochs = utils.process('Epochs', for_each=self.eLoop)
    def eLoop(self, el):
        printDebug(self.prev_years)
        self.offsets.append(self.prev_years)
        self.prev_year += el.max_year
    def loop(self, el):
        #TODO: Epochs
        #self.trip = False
        etd = el.time
        
        if self.min_time is None:
            self.min_time = el.time
        if self.max_time is None:
            self.max_time = el.time
            
        mint = self.min_time
        #mine = self.min_time.epoch
        maxt = self.max_time
        #exit('dev exit - timeline.loop')
        
        et = query.epics
        assert et is not None
        
        if self.min_time is None or et.greaterOf(mint, etd) is mint:
            self.max_time = el.time
        if self.max_time is None or et.lesserOf(mint, etd) is mint:
            self.min_time = el.time
        return el
        #if self.min_time is None or mint.isGreaterThan(etd):
        #    self.min_time = el.time
        #if self.max_time is None or maxt.isLessThan(etd):
        #    self.max_time = el.time
        #return el
    def timeSortKey(self, time):
        return time.date_time
    def unpack(self, parent):
        CascadeMenu('Timeline',parent, self.menu())
    def menu(self):
        return {
                'Refresh': self.refresh,
                'Options': [self.optionsLaunch, ACTIVE],
                'Range':[self.startLaunch, DISABLED]
                }
    def refresh(self):
        #printDebug('refresh')
        self.delete(ALL)
        self.resetLevels()
        self.initialize(scenes_mode=False)
        self.refreshIndicator()
    def optionsLaunch(self):
        #root = Tk()
        #lift(root)
        TimeDetails(min_=self.min_time, max_=self.max_time,
                    return_=self.setFrameType,
                    height=100, width=400, scene_list=self.scene_list)
    def run(self, info):
        printDebug('run={}'.format(info))
        exit('timeline.run')
    def setFrameType(self, min_, max_, info):
        self.scene_list = []
        for i in info:
            #printDebug(i)
            #for j in i['list_']:
            #    printDebug(j.time)
            self.scene_list.append(SceneList(self, **i))
        
        tmp = self.min_time, self.max_time
        
        try:
            self.min_time = min_
            self.max_time = max_
            #self.run(info)
            self.refresh()
            return True
        except ZeroDivisionError:
            self.min_time, self.max_time = tmp
            onWarn('Ending time must come after Begin time')
            return False
    def startLaunch(self):
        #TimeFrame = sys.modules['nTime'].TimeFrame
        EntryWindow = sys.modules['entry'].EntryWindow
        
        
        nTable = utils.nTables['Times']
        root = Tk()
        
        session = utils.open_session()
        first_time = session.query(nTable.mClass).first()
        
        
        min_wrap = TimeWrap(first_time, label='Start Time')
        
        EntryWindow(root, nTable=nTable, return_=self.capture, 
                        mapped_object=min_wrap)
    def capture(self, a, rowno=None):
        exit('woot!')
def lift(*arg):
    return sys.modules['Nevermore'].lift(*arg)        
class TimeWrap():
    def __init__(self, time, label='new Window'):
        self.time = time
        self.label = label
    def __str__(self):
        return self.label  

class TimeDetails0(Frame):
    def __init__(self, parent, min_=None, max_=None, return_=None, *arg, **kw):
        #printDebug('TimeDetails')
        self.return_ = return_
        Frame.__init__(self, parent, *arg, **kw)
        self.parent = parent
        self.parent.title(TIMELINE_OPTIONS_TITLE)
        #
        
        Label(self, text='Start').grid(row=0, column=0)
        Label(self, text='End').grid(row=1, column=0)
        
        self.sf0 = SelectFrame(self, obj=min_)
        self.sf0.grid(row=0, column=1)
        self.sf1 = SelectFrame(self, obj=max_)
        self.sf1.grid(row=1, column=1)
        
        g = Button(self, command=self.onGo,text="Go")
        g.grid(row=2, column=1, sticky=E)
        
        self.pack()
    def onGo(self):
        a = self.sf0.get()
        b = self.sf1.get()
        assert a is not None and b is not None
        self.return_(a, b)
        self.parent.destroy()
class SelectFrame(Frame):
    def __init__(self, parent, obj, *args, **kw):
        Frame.__init__(self, parent, *args, **kw)
        self.parent = parent
        printDebug('obj={} {}'.format(obj.__class__, obj))
        MODES = [
                 ("Scene",True),
                 ("Time", False)
                 ]
        
        self.v = IntVar()
        i = 0
        for text, mode in MODES:
            b = Radiobutton(self, text=text,
                            variable=self.v, value=mode)
            b.grid(row=i, column=0)
            i = i + 1
            
            
        fkSelect = sys.modules['entry'].fkSelect
        timeFrame = sys.modules['nTime'].TimeFrame
            
            
        scenes_class = utils.nTables['Scenes'].mClass
        f = fkSelect(self, scenes_class)
        if obj.__class__ == scenes_class().__class__:
            printDebug('scene set')
            f.set(obj)
        elif obj is not None:
            assert TypeError('set fail')
        f.grid(row=0, column=1)
        self.sceneframe = f
        
        times_class = utils.nTables['Times'].mClass
        t = timeFrame(self, return_type=times_class,
                      epoch=True,
                      date=True,
                      time=True,
                      borderwidth=3,
                      relief=SUNKEN
                      )
        if obj.__class__ == times_class().__class__:
            #TODO: set 2
            printDebug('time set')
            t.set(obj)
        else:
            printDebug('time skip')
        t.grid(row=1, column=1) 
        self.timeframe = t
    def get(self):
        a = self.sceneframe.get()
        
        
        #if self.v:
        #    printDebug('Lin')
        #    a = self.timeframe.get()
        #else:
        #    printDebug('Kuvira')
        #    a = self.sceneframe.get()
        assert a is not None
        
        
        
        
        
        
        
        
        
        
        