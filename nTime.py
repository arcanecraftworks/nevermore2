#========================library methods============================#
from Tkinter import Frame, Tk, IntVar, Entry, Label, LEFT, Toplevel
from ttk import Button
import sys
from datetime import datetime
#======================3rd party methods============================#
#======================nevermmore methods===========================#
import database.utils as utils
#from NCalendare import MONTHS, findWeekDay
import NCalendar
from cli_tools import printDebug, printDict, printDictOfObject
from MessageBox import onWarn
#==========================constants================================#
DEF_WIDTH = 2
WK_OVERRIDE = False
DISPAY_ZEROES = False
    
RT_MSG = 'object found does not match expected return type.\n'+\
                       'found: {}. expected {}'

class TimeFrame(Frame):
    def __init__(self, parent, time=True, date=True, year=True, 
                 epoch=True, event=True, return_type=None, 
                 override=False, fields=None, int_advance=None,
                 obj=None, *args, **kw):
        
        #if obj is not None:
        
        assert not override
        
        assert return_type is not None
        self.return_type = return_type
        Frame.__init__(self, parent, *args, **kw)
        self.event_option = event
        try:
            parent.title('Time Entry')
        except AttributeError:
            pass
        self.entries = dict()
        self.time = time
        self.date = date
        self.epoch = epoch
        self.year = year
        if override:
            self.initialize()
        else:
            self.initUI()
            
        if obj is not None:
            self.set(obj)
    def __repr__(self):
        s = 'time = {}\n'.format(self.time)
        s += 'date = {}\n'.format(self.date)
        s += 'epoch = {}\n'.format(self.epoch)
        s += 'year = {}\n'.format(self.year)
        return s
    def initUI(self):
        b = Button(self, text="Set", command=self.initialize)
        b.grid(row=0, column=0)
        self.pack()
    def initialize(self):    
        row = 0
        col = 0
        self.entry_widgets = dict()
        if self.time:
            t = timeField(self)
            t.grid(row=0, column=0)
            self.entry_widgets['time'] = t
        if self.date:
            col = col + 1
            d = dateField(self, year=self.year)
            d.grid(row=0, column=1)
            self.entry_widgets['date'] = d
            col = 0
            row = row + 1
        if self.epoch:
            col = col + 1
            mClass = utils.nTables['Epochs'].mClass
            fkSelect = sys.modules['entry'].fkSelect
            findLevelNo = fkSelect(self, mClass)
            findLevelNo.grid(row=1, column=0, columnspan=2)
            self.entry_widgets['epoch'] = findLevelNo
    def get(self):
        kw = dict()
        di = self.entries
        if len(di) == 0:
            return None
        for e in di:
            w = di[e]
            d = w.get()
            if d.__class__ == dict().__class__:
                for i in d:
                    a = d[i]
                    kw[i] = a
            else:
                kw[e] = w.get()
        
        timeClass = utils.nTables['Times'].mClass
        a = self.return_type == timeClass
        b = 'mapped_object' in self.__dict__
        self.rangeCheck(kw)
        #exit()
        if a and b:
            printDebug('Kirito')
            return self.editTime(**kw)
        elif a:
            printDebug('Asuna')
            return self.buildTime(**kw)
        elif b:
            printDebug('Kayaba')
            return self.edit(**kw)
        else:
            printDebug('Silica')
            return self.build(**kw)
    def rangeCheck(self, kw):
        raise NotImplementedError
    def reset(self):
        self.mapped_object = None
    def build(self, **kw):
        return self.return_type(**kw)
    def edit(self, **kw):
        a = self.mapped_object
        for e in kw:
            o = kw[e]
            a.__dict__[e] = o
            #exec('a.{} = o'.format(e))
        return a
    def editTime(self, epoch=None, **kw):
        
        a = self.mapped_object
        
        if a is None:
            return self.buildTime(epoch=epoch, **kw)
        else:
            a.date_time = datetime(**kw)
            a.epoch = epoch
            return a
    def buildTime(self, epoch=None, **kw):
        
        dt = datetime(**kw)
        a = utils.nTables['Times'].mClass
        return a(date_time=dt, epoch=epoch)
    def set(self, a):
        #assert False
        #printDictOfObject(a, exit_=False)
        #printDebug('a.__class__='.format(a.__class__))
        #printDebug('Timeframe Set={}'.format(a))
        
        if a is None:
            return
        try:
            assert a.__class__ == self.return_type
        except:
            raise
            printDebug(RT_MSG.format(a.__class__, self.return_type))
            #exit()
        self.initialize()
        ew = self.entry_widgets
        b = a.__dict__
        if 'epoch_id' in b:
            ew['epoch'].set(a.epoch)
        if 'date_time' in b:
            ew['time'].set(a.date_time)
            ew['date'].set(a.date_time)
        self.mapped_object = a
        #printDebug('Timeframe set end')
    def clear(self):
        self.reset()
class timeField(Frame):
    def __init__(self, parent):
        #printDebug('timeField')
        Frame.__init__(self, parent)
        self.entries = dict()
        self.initUI()
    def initUI(self):
        self.makeEntry('hour')
        Label(self, text=':').pack(side=LEFT)
        self.makeEntry('minute')
        Label(self, text=':').pack(side=LEFT)
        self.makeEntry('second')
    def makeEntry(self, key, width=DEF_WIDTH):
        i = IntVar(self)
        e = Entry(self, textvariable=i, width=width)
        e.pack(side=LEFT)
        self.entries[key] = e
    def get(self):
        kw = dict()
        
        for e in self.entries:
            findLevelNo = self.entries[e]
        for e in self.entries:
            kw[e] = self.entries[e].get()
        return kw
    def set(self, a):
        #printDebug('timeField.set')
        e = ['hour','minute','second']
        for i in e:
            el = getattr(a, i)
            #printDebug(el)
            self.entries[i].config(text=el)
class dateField(Frame):
    def __init__(self, parent, year=True):
        Frame.__init__(self, parent)
        self.entries = dict()
        self.year = year
        self.initUI()
    def initUI(self):
        self.makeEntry('month')
        Label(self, text='/').pack(side=LEFT)
        self.makeEntry('day')
        if self.year:
            Label(self, text='/').pack(side=LEFT)
            self.makeEntry('year', width=4)
        else:
            self.entries['year'] = IntVar()
        b = Button(self, text='NCalendar', command=self.calLaunch)
        b.pack(side=LEFT)
    
    def makeEntry(self, key, width=DEF_WIDTH):
        i = IntVar(self)
        i.set(1)
        #printDebug('{}: {}'.format(key, i))
        e = Entry(self, textvariable=i, width=width)
        e.pack(side=LEFT)
        self.entries[key] = e
    def get(self):
        kw = dict()
        for e in self.entries:
            kw[e] = self.entries[e].get()
        return kw
    def set(self, a):
        printDebug('a={} {}'.format(a.__class__, a))
        printDebug('datetime={} {}'.format(a.date(), a.time()), interrupt_override=True)
        #printDebug(''.format(a.time()))
        #exit()
        #printDebug(a.__class__)
        #printDebug('dateField.set')
        e = ['day','month','year']
        for i in e:
            #printDebug(i)
            b = getattr(a, i)
            #printDebug(b)
            c = self.entries[i]
            #printDebug(c)
            #printDebug('{}: {}'.format(i, c))

            self.entries[i].set(b)
        
        #printDict(self.entries)
        #exit()
def printTime(dt, weekday=True):
    assert dt is not None
    s = '{} {}, {}'
    one = NCalendar.MONTHS[dt.month]
    two = dt.day
    year = dt.year
    
    
    
    
    s1 = s.format(one, two, year)
    
    if weekday and not WK_OVERRIDE:
        wday = NCalendar.findWeekDay(dt)
        s1 = '{} {}'.format(wday, s1)
    if str(dt.time()) != '00:00:00' or DISPAY_ZEROES:
        s1 = '{} {}'.format(dt.time(), s1)
    return s1
    
if __name__ == '__main__':
    root = Tk()
    t = TimeFrame(root)
    t.mainloop()
        