#===========================standard library imports==========================#
import sys
from Tkinter import Frame, IntVar, Checkbutton, Label, Button
#============================3rd party imports================================#
#================================local imports================================#
import database.utils as utils
from cli_tools import printDebug
from file_utils import restoreModule
#==================================Constants==================================#
MODSELECT_MSG = 'Uninitialized database detected. Please select ' +\
                'the mods that you wish to use. Core mod v.7 will'+\
                ' be installed automatically'
WRAP_LENGTH = 200
#=============================================================================#

def stub(*arg, **kw):
    pass
class ModSelector(Frame):
    def __init__(self, parent, return_=stub, headless=False):
        Frame.__init__(self, parent)
        parent.lift()
        self.parent = parent
        self.return_ = return_
        self.parent.title('Select Mod tables')
        self.initUI()
        self.z = IntVar(self)
        self.z.set(0)
    def initUI(self):
        self.z = IntVar()
        
        self.el = Label(self, text=MODSELECT_MSG)
        self.el.config(wraplength=WRAP_LENGTH)
        self.el.grid(row=0, column=0, columnspan=4)
        self.check = Checkbutton(self, variable=self.z)
        self.check.grid(row=1, column=0)
        self.label = Label(self, text="DungeonMaster's Mod")
        self.label.grid(row=1, column=1)
        self.gobutton = Button(self, text='Choose', command=self.go)
        self.gobutton.grid(row=2, column=0, columnspan=2)
        self.can = Button(self, text='Cancel', command=self.cancel)
        self.can.grid(row=2, column=3)
        self.pack()
    def cancel(self):
        self.parent.destroy()
    def go(self, dungeon_override=False):
        self.loadMods(z=self.z)
        if self.return_():
            self.parent.destroy()
    @staticmethod
    def loadMods(z=None, dungeon_override=True):
        from database.bootstrap import nvMaps
        from database import core07
        if (z is not None and z.get()) or dungeon_override:
            from database import dungeon_core02
        utils.create_all()
        session = utils.open_session()
        
        if session.bind is None:
            session.bind = utils.engine
        
        maps = []
        nt = nvMaps(name='core', maps_file='core07',
                    static_load_directory='core03\static tables')
        maps.append(nt)
        if (z is not None and z.get()) == 1 or dungeon_override:
            dc = nvMaps(name="DungeonMaster's Mod", 
                        maps_file='dungeon_core02',
                        static_load_directory='dungeonmaster'+
                        '\static tables')
            maps.append(dc)
        
        session.add_all(maps)
        session.commit()
        session.close()
        if not sys.modules['connector'].SKIP_TWO:
            exit('Initialization completed. Reload to access')
        