import sys
import database.utils as utils
from database.core05 import Worlds, Universes

coll = []
def place(classname, name, lies_within):
	global coll
	o = utils.nTables[classname].mClass(name=name)
	if lies_within is not None:
		utils.assoc(o, 'lies_within', lies_within)
	
	coll.append(o)
	return o

def state(name, lies_within):
	return place('States', name, lies_within)
def castle(name, lies_within):
	return place('Castles', name, lies_within)
def loc(name, lies_within):
	return place('Locations', name, lies_within)
def city(name, lies_within):
	return place('Cities', name, lies_within)
def town(name, lies_within):
	return place('Towns', name, lies_within)


def exc():
	session = utils.open_session()
	shadow_univ = Universes(name='Shadow Universe')
	shadow_world = Worlds(name='Shadow World')
	mainland = loc('Main Continent', shadow_world)
	shdw = state('Shadowmoor', mainland)
	sea = loc('Endless Sea', shadow_world)
	sea.terrain = 'Ocean'
	
	frnt = loc('Frontier', shdw)
	toro = castle('Torodor', frnt)
	edin = castle('Edinor', frnt)
	cald = castle('Calderon', frnt)
	harf = castle('Harfang', frnt)
	rein = castle('Reinhart', frnt)
	morr = town('Morrowind', frnt)
	est = loc('Estuary', shdw)
	crad = town('Cradleborough', est)
	shdw.capital = crad
	selm = town('Selmburg', est)
	cross = town('Crossguard', est)
	belf = town('Belford', est)
	ralb = town('Ralberg', est)
	lind = town('Lindarth', est)
	gp = loc('Grassland Plain', shdw)
	gp.terrain = 'Rolling Plains'
	gren = town('Greenfold', gp)
	trad = town('Tradehall', gp)
	arro = town('Arrowmark', gp)
	alg = town('Algade', gp)
	vf = loc('Verdin Forest', gp)
	vf.terrain = 'Heavily Wooded'
	bar = town('Barrowdell', vf)
	wh = loc('Withered Heath', gp)
	
	
	patch = state('Patch Island', sea)
	patch.terrain = 'Volcanic ash'
	eirosh = town('Eirosh', patch)
	
	emm = state('Emmeria', mainland)
	ef = loc('Emmaline Forest', emm)
	ef.terrain = 'Heavily Wooded'
	ema = town('Emmeria', ef)
	emm.capital = ema
	il = town('Ildaria', emm)
	el = town('Elessa', emm)
	ard = town('Ardailien', emm)
	ef = loc('Arwendine Forest', emm)
	
	wd = state('Warren Downs', mainland)
	wd.terrain = 'low hills'
	ed = town('Edmonton', wd)
	ack = town('Ackerman', wd)


	taz = state('Tazhvordan', mainland)
	taz.terrain = 'Endless desert'
	ark = city('Arkash', taz)
	#taz.capital = ark

	mor = loc('Eru Moradin', mainland)
	mor.terrain = 'Mountainous'
	thor = state('Thorondim', mor)
	nyr = town('Nyradim', thor)
	uru = town('Uruband', thor)
	ang = town('Angrod', thor)
	tan = town('Tanradun', thor)
	ver = town('Veremdul', thor)
	eri = town('Eriador', thor)
	
	man = state('Mandria', mainland)

	tir = state('Tirago', mainland)
	town('Tolbana', tir)
	town('Urbus', tir)
	town('Lobria', tir)
	town('Frieven', tir)
	town('Taft', tir)
	town('Coral', tir)
	town('Panzarene', tir)
	town('Ronbaru', tir)
	town('Mishe', tir)
	town('Floria', tir)
	town('Myujen', tir)
	town('Algade', tir)
	town('Granzam', tir)
	town('Pani', tir)
	town('Marten', tir)
	town('Danac', tir)
	town('Kamdet', tir)
	town('Collinia', tir)
	
	session.add_all(coll)
	session.commit()
	session.close()