from Tkinter import Frame, Scrollbar, RIGHT, TRUE, BOTH, LEFT, VERTICAL,\
Canvas, FALSE, NW, Y, Listbox, END, ANCHOR   # from x import * is bad practice
#from ttk import *
from cli_tools import *
# http://tkinter.unpythonic.net/wiki/VerticalScrolledFrame

class VerticalScrolledFrame(Frame):
    """A pure Tkinter scrollable frame that actually works!
    * Use the 'interior' attribute to place widgets inside the scrollable frame
    * Construct and pack/place/grid normally
    * This frame only allows vertical scrolling

    """
    def __init__(self, parent, *args, **kw):
        Frame.__init__(self, parent, *args, **kw)            

        vscrollbar = Scrollbar(self, orient=VERTICAL)
        vscrollbar.pack(fill=Y, side=RIGHT, expand=FALSE)
        canvas = Canvas(self, bd=0, highlightthickness=0,
                        yscrollcommand=vscrollbar.set)
        canvas.pack(side=LEFT, fill=BOTH, expand=TRUE)
        vscrollbar.config(command=canvas.yview)

        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior,
                                           anchor=NW)

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())
        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())
        canvas.bind('<Configure>', _configure_canvas)

class ScrolledListBox(Frame):
    def __init__(self, parent, *args, **kw):
        Frame.__init__(self, parent)
        
        scrollbar = Scrollbar(self)
        scrollbar.pack(side=RIGHT, fill=Y)
        listbox = Listbox(self)
        listbox.pack(fill=BOTH, expand=1)
        
        listbox.config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=listbox.yview)
        for i in args:
            try:
                listbox.insert(END, i)
            except:
                exit('repr method on {} returns non-string.'.format(i.__class__))
        self.listbox = listbox
    def bind(self, findLevelNo):
        self.listbox.bind("<<ListboxSelect>>", findLevelNo)
    def deleteAnchor(self):
        self.listbox.delete(ANCHOR)
    def insert(self, el):
        self.listbox.insert(END, el)
    def config(self, *arg, **kw):
        self.listbox.config(*arg, **kw)
    
class ScrolledListBox2(ScrolledListBox):
    def __init__(self, parent, *arg, **kw):
        self.__makeSet(*arg)
        ScrolledListBox.__init__(self, parent, *arg, **kw)
        self.callback = kw.pop('callback', None)
        self.bind(self.string_callback)
    def string_callback(self, el):
        sender = el.widget
        idx = sender.curselection()
        value = sender.get(idx)
        return self.callback(self.kwgs[value])
    def checkDicts(self):
        if not 'kwgs' in self.__dict__:
            self.kwgs = {}
        if not 'ids' in self.__dict__:
            self.ids = {}
    def __makeSet(self, *arg):
        self.checkDicts()
        for o in arg:
            key = str(o)
            if key in self.kwgs:
                self.ids[key] = self.ids[key] + 1
                o.id = self.ids[key]
                so = str(o)
                self.kwgs[so] = o
                self.ids[so] = o.id
            else:
                self.kwgs[key] = o
                self.ids[key] = 0
    def add(self, el):
       self.__makeSet(el)
       self.insert(el)
        
        
        