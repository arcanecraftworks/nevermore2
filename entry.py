#===========================standard library imports==========================#
import sys
import Tkinter
from Tkinter import Entry, Text, IntVar, Checkbutton, StringVar, Toplevel, \
Frame
from Tkconstants import SUNKEN, RIGHT, LEFT, END, NE, N, W, INSERT, BOTH

#from Tkinter import Entry, Text, SUNKEN, IntVar, Checkbutton, RIGHT, END,\
#NE, N, W, StringVar, Toplevel, INSERT, BOTH, LEFT, TclError
from ttk import OptionMenu, Label, Button
#============================3rd party imports================================#
#================================local imports================================#
#from Nevemore.OptionMenu import ActiveOptionMenu
import database
from MessageBox import onQuest
#import Tkconstants
from scrollableFrame import VerticalScrolledFrame, ScrolledListBox
from cli_tools import printDebug, makeCaption
from nTime import TimeFrame
from nvm_types import Name
#==================================Constants==================================#
NAMES_PK = 'duplicate key value violates unique constraint "Names_pkey"'
#session = utils.open_session()
DEF_HEIGHT = 400
DEF_WIDTH = 600
MENU_CELLS = 3
DEF_FIELD_WIDTH = 50
TEXT_HEIGHT = 10
FKSELECT_WIDTH = 32
M2M_LABEL_WIDTH = 35
SELECT_LISTBOX_COLHEIGHT = 4
COM_ROW = SELECT_LISTBOX_COLHEIGHT + 1
TIME_ARRAY = ['H','M','S','D','Mo']
TO_DICT = {'H':23,'M':59,'S':59,'D':31,'Mo':12}
FROM_DICT = {'H':0,'M':0,'S':0,'D':1,'Mo':1}
EMPTY_OPTION = '-------'
SCROLL_COL = 4
MAX_ROWS = 99
RWRN = 'repr method on {} returns non-string.'

DEBUG_CONFIRM = True
        
block_times = True
#==============================================================================#

def stub(*arg, **kw):
    pass
#def makeCaption(*arg, **kw):
#    return sys.modules['cli_to'].makeCaption(*arg, **kw)
class EntryWindow(Frame):
    def __init__(self, parent, nTable=None, tablename=None, 
                 return_=stub, mapped_object=None, rowno=-1):
        #printDebug('EntryWindow')
        self.rowno=rowno
        self.__name__ = 'EntryWindow'
        self.mapped_object = mapped_object
        assert nTable is not None or tablename is not None
        if tablename is None:
            tablename = nTable.name
        if nTable is None:
            nTable = database.utils.nTables[tablename]
        self.nTable = nTable
        
        Frame.__init__(self, parent)
        self.parent = parent
        self.return_ = return_
        self.resetName()
        self.initUI(nTable)
        
        if self.mapped_object is not None:
            self.set(self.mapped_object)
    def initUI(self, nTable):
        self.frame = EntryFrame(self, nTable=nTable,
                                 mapped_object=self.mapped_object)
        self.frame.pack(fill=BOTH, expand=1)
        self.subframe = Frame(self)
        Button(self.subframe, text='Save', 
               command=self.onSave).grid(row=1, column=0)
        Button(self.subframe, text='Save and New', 
               command=self.onSN).grid(row=1, column=1)
        Button(self.subframe, text='Close', 
               command=self.onClose).grid(row=1, column=2)
        self.subframe.pack()
        
        self.pack(fill=BOTH, expand=1)
    #@utils.with_session(session)
    def onSave(self, new=False):
        session = database.utils.open_session()
        a = self.frame.load()
        assert a is not None
        session.add(a)
        try:
            session.commit()
        except:
            raise
            session.rollback()
            session.rollback()
            session.add(a)
            session.commit()
        try:
            self.return_(a, rowno=self.rowno)
        except Tkinter.TclError:
            pass
        
        if new:
            self.frame.keepStickies()
        else:
            self.parent.destroy()
    def dataOverride(self, a, n):
        d = self.nTable.mClass.__dict__
        assert n is not None
        
        result = 'yes'
        result = onQuest('Name "{}" Already exists! Override data?'.format(str(a)))
        
        if 'allow_edit' in d and result=='yes':
            #printDebug('Fork successful', override=True)
            
            d1 = a.__dict__
            for i in d1:
                exec('n.{} = d1[i]'.format(i))
            return True
        else:
            return False
    def onSN(self):
        self.onSave(new=True)
        self.rowno = -1
        self.resetName()
        self.frame.mapped_object = None
    def resetName(self):
        mClass = self.nTable.mClass
        if 'name_singular' in mClass.__dict__:
            n = mClass.name_singular
        else:
            n = makeCaption(str(mClass.__tablename__), colon=False)
        self.parent.title('New {}'.format(n))
    #def onWarn(self, s="Depreciated function call"):
    #    assert False
        #box.showwarning("Warning", s)
    def commit(self):
        self.session.commit()   
    def onClose(self):
        self.parent.destroy()
    def set(self, obj):
        self.frame.setAll(obj)
        self.parent.title(str(obj))
class EntryFrame(Frame):
    def __init__(self, parent, nTable=None, tablename=None, mapped_object=None):
        #printDebug('entryFrame')
        assert nTable is not None or tablename is not None
        if tablename is None:
            tablename = nTable.name
        if nTable is None:
            self.nTable = database.utils.nTables[tablename]
        
        Frame.__init__(self, parent)
        
        self.frame = VerticalScrolledFrame(self)
        self.frame.pack(side=LEFT, fill=BOTH, expand=1)
        self.widgets_here = self.frame.interior
        self.parent = parent
        self.config(borderwidth=3, relief=SUNKEN)
        self.fields = dict()
        self.assoc_fields = dict()
        self.stickies = dict()
        self.curRow = 0
        if tablename is not None:
            self.nTable = database.utils.nTables[tablename]
            database.utils.getClass(tablename).gui(self)
        self.mapped_object = mapped_object
    def simpleField(self, elname, caption=None):
        self.makeField(caption, elname, fieldType='String')
    def fkField(self, elname, remote, caption=None):
        self.makeField(caption, elname, remote=remote, fieldType='12M')
    def m2mField(self, elname, remote, width=DEF_FIELD_WIDTH):
        caption = None
        self.makeField(caption, elname, remote=remote, 
                       fieldType='M2M', width=width)
    def textField(self, elname, caption=None):
        self.makeField(caption, elname, fieldType='Text')
    def checkField(self, elname, caption=None, default=False):
        self.makeField(caption, elname, fieldType='bool', default=default)
    def intField(self, elname, width=5, caption=None, int_advance=True):
        self.makeField(caption, elname, fieldType='Int', 
                       width=width, int_advance=int_advance)
    def timeField(self, elname, caption=None, **kw):
        self.makeField(caption, elname, fieldType='Time', **kw)
    def dateField(self, elname, caption=None):
        self.makeField(caption, elname, fieldType='Date')
    def optionField(self, elname, options, caption=None):
        self.makeField(caption, elname, options=options)
    def nameField(self, elname, caption='Name: '):
        self.makeField(caption, elname, fieldType='Name')
    def fieldChoices(self):
        return {
                'String':StringField,
                'Int':IntField,
                'Text':TextField,
                '12M':fkSelect,
                'M2M':m2mPlaceholder,
                'Time':TimeFrame,
                'bool':CheckField,
                'Name':NameField
                }
    def makeField(self, caption, elname, width=DEF_FIELD_WIDTH, 
                  remote=None, fieldType='String', int_advance=True,
                  options=[], echo=False, **kw):
        if caption is None:
            caption= self.makeCaption(elname)
        
        self.makeLabel(caption)
        
        choice = self.fieldChoices()[fieldType]
        
        if remote.__class__ == ''.__class__:
            remote = database.utils.nTables[remote]
        
        if len(options) > 0:
            m = OptionField(self.widgets_here, options, width=width)
        else:
            kw['width'] = width
            kw['int_advance'] = int_advance
            kw['fields'] = self.fields
            kw['remote'] = remote
            #del kw['echo']
            try:
                m = choice(self.widgets_here, **kw)
            except TypeError:
                #printDebug(fieldType)
                exit()
        
        #elif fieldType=='String':
        #    m = StringField(self.widgets_here, width=width)
        #elif fieldType=='Int':
        #    m = IntField(self.widgets_here, width=width, int_advance=int_advance)
        #elif fieldType=='Text':
        #    m = TextField(self.widgets_here)
        #elif fieldType=='12M':
        #    m = fkSelect(self.widgets_here, remote)
        #elif fieldType=='M2M':
        #    m = m2mPlaceholder(self.widgets_here, remote, width)
        #elif fieldType=='Time' or fieldType=='TimeWrap':
        #    
        #    m = TimeFrame(self.widgets_here, **kw)
            #if elname == 'birthdate':
                #printDebug(m)
        #elif fieldType=='bool':
        #    m = CheckField(self.widgets_here, **kw)
        #elif fieldType=='Name':
        #    m = NameField(self.widgets_here, fields=self.fields)
        
        if fieldType == 'M2M':
            self.assoc_fields[elname] = m
        else:
            self.verifyField(elname, m)
        
        if fieldType=='Int' or fieldType=='Time' or fieldType=='bool':
            m.grid(row=self.curRow, column=1, sticky=W)
        else:
            m.grid(row=self.curRow, column=1)
        self.makeSticky(elname)
        self.curRow = self.curRow + 1    
    def verifyField(self, elname, m, assoc=False):
        if elname in self.fields:
            pass
            #printDebug('duplicate key {}'.format(elname))
        elif assoc:
            self.assoc_fields[elname] = m
        else:
            self.fields[elname] = m
    def checkAssocField(self):
        raise NotImplementedError
    def makeLabel(self, caption):
        L = Label(self.widgets_here, text=caption, justify=RIGHT)
        L.grid(row=self.curRow, column=0, sticky=NE)    
    def makeSticky(self, elname):
        self.stickies[elname] = Tkinter.IntVar()
        c = Tkinter.Checkbutton(self.widgets_here, 
                        text="sticky", 
                        variable=self.stickies[elname], 
                        takefocus=False)
        c.grid(row=self.curRow, column=2, sticky=N)
    def load(self):
        if self.mapped_object is None:
            a = self.nTable.mClass()
        else:
            a = self.mapped_object
        for i in self.fields:
            j = self.fields[i].get()
            if j is not None:
                s = 'a.{} = j'.format(i)
                try:
                    exec(s)
                except TypeError:
                    database.utils.assoc(a, i, j)
        for i in self.assoc_fields:
            if i == 'descended_from':
                o = self.assoc_fields[i]
            o = self.assoc_fields[i]
            if o.dirty:
                res_array = o.get()
                for remote_object in res_array:
                    try:
                        database.utils.assoc(a, i, remote_object)
                    except AssertionError:
                        raise
                        #printDebug('a={} {}'.format(a.__class__, a))
                        #printDebug('i={} {}'.format(i.__class__, i))
                        #printDebug('rem_o={}'.format(remote_object.__class__, remote_object))
        assert a is not None
        return a
    def keepStickies(self):
        for i in self.fields:
            el = self.fields[i]
            st = self.stickies[i].get()
            if st == 0:
                el.clear()
            elif 'reset' in el.__dict__:
                el.reset()
        for i in self.assoc_fields:
            if self.stickies[i].get() == 0:
                self.assoc_fields[i].clear()
            elif 'reset' in el.__dict__:
                el.reset()
    @staticmethod
    def makeCaption(s):
        return makeCaption(s)
    def set(self, index, value):
        if index in self.fields:
            m = self.fields[index]
        if index in self.assoc_fields:
            m = self.assoc_fields[index]
        m.set(value)
    def setAll(self, obj):
        for i in self.fields:
            #printDebug(i)
            j = self.fields[i]
            if j is not None:
                self.setOne(j, i, obj)
        for i in self.assoc_fields:
            j = self.assoc_fields[i]
            if j is not None:
                self.setOne(j, i, obj)
    def setOne(self, field, i, obj):
        #printDebug(field)
        #printDebug(i)
        #exit()
        
        #s = 'field.set(obj.{})'.format(i)
        try:
            s = getattr(obj, i)
            field.set(s)
            if 'dirty' in field.__dict__:
                field.dirty = False
        except:
            #utils.printDebug('{}.set(obj.{})'.format(field, i))
            raise
class NameField(Frame):
    def __init__(self, parent, default='', fields=None, **kw):
        #TODO: entry
        #assert False
        width=FKSELECT_WIDTH
        assert fields is not None
        self.fields = fields
        self.obj = None
        self.nTable = database.utils.nTables['Names']
        self.remote = self.nTable.mClass
        
        self.session = database.utils.open_session()
        self.checkTable = Name.checkTable
        Frame.__init__(self, parent)
        self.parent = parent
        self.initUI(width)
    def initUI(self, width):
        self.svar = StringVar()
        e = Entry(self, textvariable=self.svar, width=width)
        e.grid(row=0, column=0, sticky=W)
        e.config(width=width)
        b = Button(self, text='Details', command=self.mean, takefocus=False)
        b.grid(row=0, column=1)
        self.pack()
    def mean(self):
        root = Toplevel(self.master)
        m = EntryWindow(root, nTable=self.nTable, return_=self.newReturn)
        #exit(self.z.get())
        name = self.svar.get()
        m.frame.set('name', name)
        o = self.checkTable(name)
        if o is None:
            self.obj = database.utils.getClass('Names')(name=name)
        else:
            self.obj = o
            m.frame.setAll(o)
    
    def newReturn(self, a, **kw):
        name = a.name
        r = self.checkTable(name)
        if r is None:
            self.setObj(a)
        else:
            self.setObj(r)
    def setObj(self, a):
        self.obj = a
        if str(a) == 'None':
            s = ''
        else:
            s = str(a)
        self.svar.set(s)
    def get(self):
        if self.svar.get() == '':
            #printDebug('Null Object')
            return None
        if self.obj is None:
            #printDebug('New Object')
            session = database.utils.open_session()
            q = session.query(database.utils.getClass('Names'))
            val = self.svar.get()
            e = q.get(val)
            n = database.utils.getClass('Names')(name=self.svar.get())
            b = 'name_plural' in self.fields
            if e is not None:
                #printDebug('Existing Object')
                return e
            elif b:
                n.name_pural = self.parent.fields['name_plural']
            #printDebug('New Object')
            return n
                
        else:
            #printDebug('Existing Object')
            return self.obj
    def set(self, value):
        
        if value is None:
            self.svar.set('')
        else:
            self.svar.set(value)
    def clear(self):
        self.set('')
        self.obj = None
class OptionField(Frame):
    def __init__(self, parent, options, width=FKSELECT_WIDTH, command=None, default=None, **kw):
        #printDebug(options)
        
        Frame.__init__(self, parent)
        self.parent = parent
        self.a = options
        self.command = command
        self.var = StringVar(self)
        self.var.set(default)
        self.initUI(width)
        
        if default is not None:
            self.var.set(default)
    def initUI(self, width):
        self.var.set(self.a[0])
        #e = apply(OptionMenu, (self, self.var))
        #e = apply(OptionMenu, (self, self.var) + tuple(self.a))
        
        e = OptionMenu(self, self.var, self.a[0], *self.a, command=self.command)
        #e.add_all(self.a)
        e.grid(row=0, column=0, sticky=W)
        e.config(width=width)
        self.wid = e
        self.pack()
    def get(self):
        a = self.var.get()
        if a == '':
            return None
        else:
            return a
    def set(self, value):
        self.var.set(value)
    def clear(self):
        self.var.set('')
    #def add(self, el):
    #    self.wid.insert(END, el)
class StringField(Frame):
    def __init__(self, parent, width=DEF_FIELD_WIDTH, **kw):
        Frame.__init__(self, parent)
        self.z = StringVar()   
        self.m = Entry(self, textvariable=self.z, width=width)
        self.m.grid(row=0, column=0)
    def get(self):
        return self.z.get()
    def set(self, value):
        if str(value) == 'None':
            value = ''
        self.z.set(value)
    def clear(self):
        self.z.set('')
class IntField(Frame):
    def __init__(self, parent, width=DEF_FIELD_WIDTH/2, 
                 int_advance=True, default=0, **kw):
        Frame.__init__(self, parent)
        self.z = IntVar()
        self.z.set(default)
        self.int_advance = int_advance
        self.m = Entry(self, textvariable=self.z, width=width)
        self.m.grid(row=0, column=0, sticky=W)
        self.b = IntVar()
        if int_advance:
            Checkbutton(self, text="Increment on New", 
                        variable=self.b, takefocus=False).\
                        grid(row=0, column=2, sticky=N)
        else:
            self.b.set(0)
        self.pack()
    def get(self):
        return self.z.get()
    def set(self, value):
        return self.z.set(value)
    def clear(self):
        if self.b.get() == 0:
            self.z.set(0)
        else:
            tmp = self.z.get()
            self.z.set(tmp + 1)
class CheckField(Frame):
    def __init__(self, parent, default=False, **kw):
        Frame.__init__(self, parent)
        self.z = IntVar()
        self.z.set(default)
        self.m = Checkbutton(self, variable=self.z)
        self.m.grid(row=0, column=0, sticky=W)
    def get(self):
        b = self.z.get()
        if b == 1:
            return True
        elif b == 0:
            return False
    def clear(self):
        self.z.delete(0, END)
    def set(self, value):
        self.z.set(value)
class TextField(Frame):
    def __init__(self, parent, width=DEF_FIELD_WIDTH, height=TEXT_HEIGHT, **kw):
        Frame.__init__(self, parent)
        self.m = Text(self, width=width-13, height=height)
        self.m.grid(row=0, column=0)
    def get(self):
        s = self.m.get(1.0, END).rstrip('\n')
        return s
    def set(self, value):
        self.clear()
        if value is not None:
            self.m.insert(INSERT, value)
        else:
            self.m.insert(INSERT, '')
    def clear(self):
        self.m.delete(1.0, END)
class fkSelect(Frame):
    def __init__(self, parent, remote, default='', width=FKSELECT_WIDTH,
                 on_update=None, **kw):
        self.session = database.utils.open_session()
        self.on_update = on_update
        #printDebug('on_update={}'.format(on_update))
        Frame.__init__(self, parent)
        self.parent = parent
        assert remote is not None
        self.remote = remote
        self.nTable = database.utils.nTables[remote.__tablename__]
        self.var = StringVar(self)
        self.var.set(default)
        self.initUI(width)
    def initUI(self, width):
        options = database.utils.process(self.remote)
        self.a = [EMPTY_OPTION]
        self.a.extend(options)
        
        e = OptionMenu(self, self.var, *self.a, command=self.on_update)
        e.grid(row=0, column=0, sticky=W)
        e.config(width=width)
        
        b = Button(self, text='New', command=self.onNew)
        b.grid(row=0, column=1)
        #self.pack()
    def onNew(self):
        root = Toplevel(self.master)
        m = EntryWindow(root, nTable=self.nTable, return_=self.newReturn)
    def newReturn(self, a, **kw):
        self.var.set(a)
        self.obj = a
        if self.on_update is not None:
            self.on_update(a)
    def get(self):
        s = self.var.get()
        for i in self.a:
            if str(i) == s and i.__class__ is not str:
                return i
        if 'obj' in self.__dict__ and self.obj.__class__ != str:
            return self.obj
    def set(self, value):
        #printDebug('fkSelect.set={}'.format(value))
        self.var.set(value)
        self.obj = value
        self.dirty = True
        self.on_update
    def comp_set(self, key, obj):
        #assert obj.__class__ != str
        
        #printDebug('comp_set')
        #printDebug('obj={}'.format(obj))
        for i in self.a:
            #stri = str(i)
            class_ = i.__class__
            if class_ != str and getattr(i, key) == obj:
                self.set(i)
    def clear(self):
        self.var.set('')
        self.obj = None
        self.dirty = False
class m2mPlaceholder(Frame):
    def __init__(self, parent, remote, width=M2M_LABEL_WIDTH, **kw):
        Frame.__init__(self, parent)
        self.parent = parent
        self.nTable = database.utils.nTables[remote.__tablename__]
        self.var = StringVar(self)
        self.array = []
        b = Button(self, text='Select', command=self.expand)
        b.grid(row=0, column=0)
        self.pack()
        #self.var2 = StringVar()
        c = Label(self, textvariable=self.var, width=width)
        c.grid(row=0, column=1)
        self.dirty = False
    def expand(self):
        root = Toplevel(self.master)
        assert self.array is not None
        m = m2mSelect(root, self, 
                      b_array=self.array, 
                      return_=self.updateLabel)
    def updateLabel(self, a, dirty, remove=None):
        session = database.utils.open_session()
        #printDebug(a)
        if a is None:
            self.array = []
        else:
            tmp = []
            rem = []
            for i in a:
                if i is not None:
                    tmp.append(i)
            for i in remove:
                assert 'child' in i.__dict__
                session.delete(i)
            self.array = tmp
            session.add_all(tmp)
            
            self.var.set(self.array)
            self.dirty = dirty
    def get(self):
        return self.array
    def set(self, obj=None, array=None):
        assert array is not None or obj is not None
        
        if array is None:
            array = []
            for i in obj:
                array.append(i)
        self.var.set(str(array))
        self.array = array
        #self.dirty = True
    def update(self, *args, **kw):
        set(args, kw)
    def clear(self):
        self.array = []
        self.var.set('')
        #self.dirty = False
    def reset(self):
        tmp = []
        for i in self.array:
            if 'child' in i.__dict__:
                #printDebug(i)
                tmp.append(i.child)
        self.array = tmp
class m2mSelect(Frame):
    def __init__(self, parent, placeholder, 
                 b_array=[], return_=stub, session=None):
        assert session is None
        
        self.session = database.utils.open_session()
        
        #if session is None:
        #    assert False
        #    self.session = utils.open_session(autoflush=True)
        #else:
        #    self.session = session
        Frame.__init__(self, parent)
        self.parent = parent
        self.nTable = placeholder.nTable
        self.setFrameType = self.nTable.mClass
        self.return_ = return_
        self.parent.title('Select from {}'.format(self.nTable.name))
        self.initUI(self.nTable, b_array=b_array)
        self.rem = []
    
    def procfe(self, i, b_array=None, elements=None):
        if not self.inArray(i, b_array):
            try:
                elements.append(i)
                return i
            except:
                raise
                #exit(RWRN.format(i.__class__))
    
    def initUI(self, nTable, b_array=None):
        elements = []
        
        self.a = database.utils.process(nTable.mClass, for_each=self.procfe,
                               elements=elements,
                               b_array=b_array
                               )
            
        assert False
        self.origin = ScrolledListBox(self, elements)
        self.origin.grid(row=0, column=0, rowspan=SELECT_LISTBOX_COLHEIGHT)
        self.origin.bind(self.onSelect)
        
        blank1 = Label(self, text=' ')
        blank1.grid(row=0, column=1)
        
        selectButton = Button(self, text='==>', command=self.forward)
        selectButton.grid(row=1, column=1)
        filbutton = Button(self, text='Filter Out', command=self.filter)
        filbutton.grid(row=2, column=1)
        deselButton = Button(self, text='<==', command=self.backward)
        deselButton.grid(row=3, column=1)
        
        self.var = StringVar()
        self.var2 = StringVar()
        self.obj = None
        self.pack()
        
        self.b = b_array
        #lb2 = ScrolledListBox(self, self.b)
        #self.dest = lb2
        #self.dest.bind(self.onSelect2)
        #lb2.grid(row=0, column=2, rowspan=SELECT_LISTBOX_COLHEIGHT)
        #new = Button(self, text='Add New', command=self.onNew)
        #new.grid(row=COM_ROW, column=0)
        
        okay = Button(self, text='OK', command=self.onOK)
        okay.grid(row=COM_ROW, column=1)
        
        cancel = Button(self, text='Cancel', command=self.onCancel)
        cancel.grid(row=COM_ROW, column=2)
        self.dirty = False
    def inArray(self, el, array):
        for i in array:
            if 'child' in i.__dict__ and i.child == el:
                return True
        return False
    def onSelect(self, val):
        sender = val.widget
        idx = sender.curselection()
        value = sender.get(idx)
        self.var.set(value)
    def onSelect2(self, val):
        sender = val.widget
        idx = sender.curselection()
        #try:
        if len(idx) > 0:
            value = sender.get(idx)
            self.var2.set(value)
        #except TclError:
        #    printDebug('{} "{}"'.format(idx.__class__, idx))
    def getObj(self, var=1):
        if var == 1:
            s = self.var.get()
        elif var == 2:
            s = self.var2.get()
        for i in self.a:
            if s == str(i):
                self.obj = i
        return self.obj
    def forward(self):
        o = self.getObj()
        self.origin.deleteAnchor()
        self.b.append(o)
        assert o is not None
        assert str(o) is not 'None'
        self.dest.insert(str(o))
        self.dirty=True
        try:
            self.rem.remove(o)
        except ValueError:
            pass
    def filter(self):
        self.origin.deleteAnchor()
    def backward(self):
        o = self.getObj(var=2)
        self.dest.deleteAnchor()
        try:
            self.remove(self.b, o)
        except ValueError:
            assert False
            #printDebug('value error')
        self.origin.insert(o)
    def remove(self, array, el):
        for i in array:
            if 'child' in i.__dict__ and i.child == el:
                array.remove(i)
                self.rem.append(i)
            elif i == el:
                array.remove(i)
    def onNew(self):
        root = Toplevel(self.master)

        EntryWindow(root, nTable=self.nTable, 
                        return_=self.newReturn)
    def newReturn(self, a, **kw):
        assert a is not None
        try:
            self.b.append(a)
            self.dest.insert(str(a))
            self.dirty = True
        except TypeError:
            raise
    def onOK(self):
        self.return_(self.b, self.dirty, remove=self.rem)
        self.master.destroy()
    def makeList(self):
        pass
        self.b
    def onCancel(self):
        self.master.destroy()
def macro():
    pass











