#===========================standard library imports==========================#
import sys
from datetime import datetime, timedelta
#============================3rd party imports================================#
#================================local imports================================#
import database
from cli_tools import *
#==================================Constants==================================#
ORDER = ['seconds','minutes','hours','days','months','years','epochs']
#=============================================================================#
MIN_STEP = 2
MULTS = 1, 2, 5, 10, 25
LEVELS = 5, 10, 20, 40, 50, 70

DISP_LEV =2
DATE = {'days':'day','months':'month','years':'year',
        'hours':'hour','minutes':'minute','seconds':'second', 'epochs':'epoch'}
def getColors():
    return sys.modules['timeline'].COLORS
class ScaleObj():
    def __init__(self, key, multiplier, val):
        self.key = key
        self.multiplier = multiplier
        self.val = val
    def __str__(self):
        return '{} {}: {}'.format(self.multiplier,
                           self.key,
                           self.val)
    def __repr__(self):
        return 'key={}\nmult={}\nvalue={}'.format(self.key, 
                                               self.multiplier,
                                               self.val)
        #s = 'key={}\n'.format(self.key)
        #s += 'mult={}\n'.format(self.multiplier)
        #s += 'value={}'.format(self.val)
        #return s
class EpochScale():
    """Class represents all of the data needed to print out a time scale on the
    imbedded timeline.
    """
    def __init__(self, parent_frame, width_pixels, min_time, max_time):
        self.width_pixels = width_pixels
        #printDebug(width_pixels)
        self.parent_frame = parent_frame
        self.typeConfirm(min_time)
        self.typeConfirm(max_time)
        
        self.min_time = min_time
        self.max_time = max_time
        self.month_i = min_time.date_time.month
        #assert False
        
        self.delta = self.makeDelta()
        self.totals = self.total_times()
        self.num_steps = width_pixels / MIN_STEP
        self.max_scale, self.t = self.maxScale()
        self.step = float(width_pixels)/float(self.max_scale.val)
        self.step_range = self.getStepRange()
    def getStepRange(self):
        """Builds a range of values that cam be used as indexes to 
        get a time or display ticks on the timeline
        """
        a = []
        for i in range(self.max_scale.val):
            a.append(i * self.step)
        return a
    def getTime(self, i):
        """translates pixel index 'i' into a datetime object
        """
        db_Times = database.utils.getClass('Times')
        kw = {}
        j = int(i / self.step) * self.max_scale.multiplier
        
        mid_delta = timedelta(**{self.max_scale.key:j})
        self.mid_delta = mid_delta
        tmp = self.min_time.date_time + mid_delta
        
        total_years = 0
        for epoch in database.query.classQ('Epochs'):
            epoch_max = epoch.max_year
            total_years = total_years + epoch_max
            lim = self.endOf(total_years)
            if tmp < lim or epoch_max == -1:
                x = db_Times(date_time=tmp, epoch=epoch)
                #printDebug('result={}'.format(x))
                return x
    def getTimeRange(self, index):
        """Returns the range interval represented by the given index
        """
        
        a = self.getTime(index-1)
        b = self.getTime(index)
        
        return a, b
    def disp_step(self, index, time_1, time_2):
        """Displays a single slice of the timeline
        """
        #printDebug('index={}'.format(index))
        
        for key in ORDER:
            time_a = self.parseTime(DATE[key], time_1)
            time_b = self.parseTime(DATE[key], time_2)
            
            t = self.t
            t['months'] = None
            
            #printDebug(self.t)
            #return
            if key in t and time_b != time_a:
                self.__forEachOrder(key, index, time_a, time_b)
    def month(self, time_a, time_b):
        printDebug('{} {}')
        
    def parseTime(self, key, time_obj):
        #printDebug('key={}'.format(key))
        if key == 'epoch':
            return time_obj.epoch
            #return getattr(time_obj, key)
        
        else:
            return getattr(time_obj.date_time, key) 
    def __doDisplay__(self, level_index, max_index, key):
        return level_index == DISP_LEV or (max_index == 2 and key=='months')
    def __forEachOrder(self, key, index, time_1, time_2):
        """Displays the ticks of a given level key.
        """   
        order_id = ORDER.index(key)
        min_key = self.max_scale.key
        max_index = ORDER.index(min_key)
        level_index = order_id - max_index
        
        assert level_index < len(LEVELS)
        
        if level_index < 0:
            return
        
        if key == 'day':
            secondary = ''
        
        
        
        if self.__doDisplay__(level_index, max_index, key):
            text = self.parent_frame.getText(key, time_2)
        elif key:
            text = None
        level = LEVELS[level_index]
        #printDebug(level)
        if index < self.width_pixels:
            self.parent_frame.tick(index, level=level, color=getColors()[key], text=text)
    def isSameEpoch(self):
        """returns true if the stored minimum time and the maximum time
        objects are from the same Epoch.
        """
        return self.min_time.epoch == self.max_time.epoch
    def makeDelta(self):
        """Determines the amount of time between the minimum and maxiumum
        time objects already stored in the instance. Takes Epochs into account.
        """
        max_ = self.max_time.date_time
        min_ = self.min_time.date_time
        #printDebug('min={} max={}'.format(min_, max_))
        
        
        if self.isSameEpoch():
            return self.max_time.date_time - self.min_time.date_time
        else:
            delta = None
            for epoch in database.query.classQ('Epochs'):
                if epoch == self.min_time.epoch:
                    delta = self.endOf(epoch.max_year) - self.min_time.date_time
                elif delta is not None:
                    d = self.endOf(epoch.max_year) - self.beginningOf(1)
                    delta = delta + d
                elif epoch == self.max_time.epoch:
                    return delta + self.max_time.date_time
    def endOf(self, year):
        """Creates a datetime object representing the last day of the given year
        """
        return datetime(day=31, month=12, year=year)
    def beginningOf(self, year):
        """Creates a datetime object representing the first day of the given year 
        """
        return datetime(day=1, month=1, year=year)
    def typeConfirm(self, time):
        """Validates that the time is in fact a database.Times object
        """
        Times = database.utils.getClass('Times')
        if time.__class__ == Times:
            return True
        else:
            s = 'EpochScale object requires database mClass "Times". recieved {}'
            raise TypeError(s.format(time.__class__))
    def maxScale(self):
        """I'm not 100% sure what this one does
        """
        
        t = {}
        max_scale = ScaleObj('None',0,0)
        for key, val in self.totals.iteritems():
            #printDebug('{}: {}'.format(key, val))
            if key != 'months':
                a = self.searchSteps(val, key)
            else:
                a = 0
            if a.val != 0:
                t[key] = a
                if a.val > max_scale.val:
                    max_scale = a
        self.max_scale = max_scale
        assert self.max_scale.val != 0
        return max_scale, t
    def searchSteps(self, s, key):
        if s is None:
            return ScaleObj(key,0,0)
        
        max_o = ScaleObj('None',0,0)
        for i in MULTS:
            t = s / i
            
            mid_o = ScaleObj(key, i, t)
            
            if mid_o.val > max_o.val and mid_o.val < self.num_steps:
                max_o = mid_o
        return max_o
    def total_times(self):
        """Breaks the object's delta attribute into separate scales, each of
        which represents the total number of a different key instead of positional
        numbering 
        """
        delta = self.delta
        #printDebug('delta={}'.format(delta), interrupt_override=True)
        total = dict()
        
        days = delta.days
        total['days'] = days
        total['years'] = total['days']/365
        sec = delta.seconds
        
        total['month'] = None
        
        hours = sec/3600
        hours += days*24
        total['hours'] = hours
        
        minutes = sec/60
        minutes += days*60*24
        total['minutes'] = minutes
        
        seconds = sec
        seconds += days*60*60*24
        total['seconds'] = seconds
        
        return total