#========================library methods============================#
#======================3rd party methods============================#
#======================nevermmore methods===========================#
#==========================constants================================#
class Callback():
    #designed to make a setFrameType function that will allow a button or 
    #menu setFrameType to be customized to fit a particular button.
    
    #designate the destination function and paramter/dict/array and set
    #the widget's command attribute to this.setFrameType
    def __init__(self, return_, *arg, **kw):
        assert return_ is not None
        self.return_ = return_
        self.arg = arg
        self.kw = kw
    def callback(self):
        return self.return_(*self.arg, **self.kw)
    def __call__(self):
        return self.callback()