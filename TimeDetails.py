#===========================standard library imports==========================#
from Tkinter import Frame, Label, Entry, StringVar, Tk, Toplevel
from Tkconstants import LEFT, END, E, W, N, S, GROOVE
from ttk import Button, OptionMenu
import sys
from datetime import datetime
from tktools import lift
#============================3rd party imports================================#
#================================local imports================================#
#import database.query as query
import database.utils as utils
from cli_tools import *
#from entry import fkSelect
#import ntk
DEF_WIDTH = 2
FF_WIDTH = 90
DEF_COLOR = 'red'
#==================================Constants==================================#
TIMELINE_OPTIONS_TITLE = 'Timeline Options'
FILTER_DEF = 'Holidays'

def Calendar(*arg, **kw):
    return sys.modules['Calendar'].Calendar(*arg, **kw)

def __query__(s):
    a = sys.modules['database.query']
    return getattr(a, s)
def scenesByCharAppearance(*arg, **kw):
    q = __query__('scenesByCharAppearance')
    return q(*arg, **kw)
def classQ(*arg, **kw):
    q = __query__('classQ')
    return q(*arg, **kw)
def characterBirthdays(*arg, **kw):
    return getattr(__query__('characterBirthdays')(*arg, **kw))
def entry(at=None):
    e = sys.modules['entry']
    if at is None:
        return e
    else:
        return getattr(e, at)
class TimeDetails(Frame):
    def __init__(self, min_=None, max_=None, return_=None, scene_list=[], *arg, **kw):
        #printDebug('min={}'.format(min_))
        #printDebug('max={}'.format(max_))
        
        root = Tk()
        lift(root)
        Frame.__init__(self, root, *arg, **kw)
        self.return_ = return_
        self.parent = root
        self.parent.title(TIMELINE_OPTIONS_TITLE)
        
        #table = utils.nTables['Times'].sTable
        Label(self, text='Begin:').grid(row=0, column=0, sticky=E)
        ts = TimesEntry(self, set_=min_)
        ts.grid(row=0, column=1)
        self.begin = ts
        
        Label(self, text='End:').grid(row=1, column=0, sticky=E)
        end = TimesEntry(self, set_=max_)
        end.grid(row=1, column=1)
        self.end = end
        
        to = TimeOptionsContainer(self, scene_list=scene_list, height=100, width=850, 
                                  borderwidth=3, relief=GROOVE)
        to.grid(row=2, column=0, columnspan=2, sticky=N+W+E+S)
        self.to = to
        
        b = Button(self, text='Go', command=self.setFrameType)
        b.grid(row=3, column=1, sticky=E)
        
        #for i in scene_list:
        #    self.to.makeSet(i)
        self.pack()
    def proctime(self, el):
        printDebug(el)
    def setFrameType(self):
        begin = self.begin.get()
        end = self.end.get()
        if self.return_(begin, end, self.to.get(begin, end)):
            self.parent.destroy()    
class TimeOptionsContainer(Frame):
    col_def = 1
    def __init__(self, parent, scene_list=[], *arg, **kw):
        Frame.__init__(self, parent, *arg, **kw)
        
        self.row_number = 0
        self.column_number = self.col_def
        self.rows = []
        self.buttons = []
        self.initUI()
        for i in scene_list:
            self.makeSet(i)
    def makeSet(self, sceneList):
        
        if 'obj' in sceneList.__dict__:
            extra = sceneList.obj
        else:
            extra = None
        
        #printDebug('makeSet - ')
        #for key, val in sceneList.__dict__.iteritems():
        #    printDebug('    {}: {}'.format(key, val))
        self.makeRow(color=sceneList.color, 
                     filter_type=sceneList.choice, 
                     extra=extra)
        
    def initUI(self):
        headers = ['Color','Filter']
        #i = 0
        
        for j in range(len(headers)):
            txt = headers[j]
            Label(self, text=txt).grid(row=0, column=j+1, sticky=W)
            
        self.row_number = 1
        #self.makeRow(color='red')
    def setGrid(self, o, row=None, col=None, colreset=None):
        #printDebug(self.column_number)
        
        if row is None:
            row = self.row_number
        if col is None:
            col = self.column_number
        o.grid(row=row, column=self.column_number)
        if not colreset:
            self.column_number = self.column_number + 1
        else:
            self.column_number = 1
    def makeRow(self, color=DEF_COLOR, filter_type=None, **kw):
        #printDebug('makeRow filter type={}'.format(filter_type))
        to = TimeOptions(self, color, filter_type, index=self.row_number, **kw)
        
        plus = PlusButton(self, index=self.row_number+1,
                          pos_command=self.return_,
                          neg_command=self.neg_return)
        plus.grid(row=self.row_number+1, column=0)
        self.row_number = self.row_number+1
        self.column_number = self.col_def
        self.rows.append(to)
        self.buttons.append(plus)
    def return_(self):
        self.makeRow()
    def neg_return(self, index):
        i = self.rows[index]
        self.rows[index-1].grid_forget()
        self.rows.remove(i)
        #self.buttons.delete
    def get(self, *arg):
        res = []
        for row in self.rows:
            a = self.proc(row, *arg)
            res.append(a)
        
        return res
    def proc(self, el, *arg):
        #a = el.get()
        #printDebug(a)
        return el.get(*arg)
class PlusButton(Frame):
    def __init__(self, parent, index=0, *arg, **kw):
        self.index=index
        self.pos_command = kw.pop('pos_command', None)
        self.neg_command = kw.pop('neg_command', None)
        Frame.__init__(self, parent, *arg, **kw)
        self.initUI()
    def initUI(self):
        self.button = Button(self, text='+', width=4, command=self.pos_return)
        self.button.pack()
    def pos_return(self):
        self.pos_command()
        self.button.pack_forget()
        self.button = Button(self, text='-', width=4, command=self.neg_return)
        self.button.pack()
    def neg_return(self):
        self.neg_command(self.index)
        self.grid_forget()
class TimeOptions():
    col_def = ['filter aggregate']
    def __init__(self, parent, color, filter_type, *arg, **kw):
        #printDebug('Timeoptions filter type={}'.format(filter_type))
        self.index = kw.pop('index',0)
        
        #Frame.__init__(self, parent, *arg, **kw)
        self.parent = parent
        self.extra = kw.pop('extra',[])
        self.objects = []
        self.color_menu = self.makeMenu(self.getColors(), default=color)
        self.filter_menu = self.makeFilter(filter_type)
        
    def makeMenu(self, opt, default=None, command=None):
        if default is None:
            default = opt[0]
        
        p = self.parent
        v = StringVar(p)
        o = OptionMenu(p, v, default, *opt, command=command)
        self.objects.append(o)
        self.parent.setGrid(o)
        return v
    def makeFilter(self, filter_type):
        o = FilterFrame(self.parent, default=filter_type)
        o.set(self.extra)
        self.parent.setGrid(o)
        self.objects.append(o)
        return o
    def getColors(self):
        a = sys.modules['timeline'].COLORS.values()
        a.extend(self.col_def)
        return a
    def grid_forget(self):
        for o in self.objects:
            o.grid_forget()
    def get(self, *arg):
        kw = {}
        #TODO: get
        kw['color'] = self.color_menu.get()
        kw['choice'], kw['obj'], kw['query'], kw['list_'] = self.filter_menu.get(*arg)
        return kw
class FilterFrame(Frame):
    #FILTER_DEF = 'All Scenes'
    def filter_opt(self):
        return {
                'All Scenes':self.asc,
                #'Scene Type':self.st,
                'Plotlines':self.plot,
                'Characters who appear':self.ca,
                'Character birthdays':self.cb,
                'Holidays':self.hol
               }
    def __init__(self, parent, *arg, **kw):
        default = kw.pop('default', FILTER_DEF)
        if default is None:
            default = FILTER_DEF
        
        #printDebug(default)
        
        Frame.__init__(self, parent, *arg, **kw)
        self.initUI(default)
        self.q = None
    def initUI(self, default):
        fo = self.filter_opt().keys()
        v = StringVar(self)
        o = OptionMenu(self, v, default, *fo, command=self.filterCom)
        o.pack(side=LEFT)
        self.variable = v
        self.filterCom(default)
    def filterCom(self, el):
        if 'extra' in self.__dict__ and self.extra is not None:
            self.extra.pack_forget()
        a = self.filter_opt()[el]
        if a is not None:
            #session = utils.open_session()
            self.choice = el
            self.extra = a()
    def chrono_order(self, el):
        #TODO: account for epoch
        return el.time.date_time
    def loop(self, el):
        #TODO: Epochs
        
        etd = el.time.date_time
        
        if self.min_time is None:
            self.min_time = el.time
        if self.max_time is None:
            self.max_time = el.time
            
        mintd = self.min_time.date_time
        maxtd = self.max_time.date_time
        
        if self.min_time is None or mintd > etd:
            self.min_time = el.time
        if self.max_time is None or maxtd < etd:
            self.max_time = el.time
        return el
    def st(self):
        assert False
    def plot(self):
        plotlines = utils.getClass('Plotlines')
        b = entry(at='fkSelect')(self, plotlines)
        b.pack(side=LEFT)
        return b
    def asc(self):
        if 'extra' in self.__dict__:
            
            
            classQ('Scenes')
            classQ('Scenes')
    def ca(self):
        #printDebug(arg)
        #printDebug(kw)
        b = self.m2m('Characters')
        b.pack(side=LEFT)
        return b
    def cb(self):
        return self.ca()
    def hol(self):
        b = self.m2m('Holidays')
        b.pack(side=LEFT)
        return b
    def m2m(self, classname):
        mClass = utils.getClass(classname)
        b = entry(at='m2mPlaceholder')(self, mClass, FF_WIDTH)
        b.pack(side=LEFT)
        return b
    def set(self, extra):
        if self.extra is not None:
            self.extra.set(extra)
    
    def get(self, min_, max_):
        s = self.variable.get()
        #printDebug(s)
        #TODO: get
        if self.extra is not None:
            extra = self.extra.get()
        else:
            extra = None
            
        q = None
        list_ = []
        #printDebug('extra={}'.format(extra))
        Epochs = utils.getClass('Epochs')
        epochs = classQ('Epochs')
        classQ('Epochs').order_by(Epochs.id).all()
        
        Plotlines = utils.getClass('Plotlines')
        Scenes = utils.getClass('Scenes')
        
        if s == 'All Scenes':
            q = classQ('Scenes')
        elif s == 'Scene Type':
            assert False
        elif s == 'Plotlines':
            q = classQ('Scenes')
            q = q.filter(Scenes.plotline==extra)
            
            #q.filter(plotline=extra)
        elif s == 'Characters who appear':
            list_ = scenesByCharAppearance(min_, max_, *extra)
        elif s == 'Character birthdays':
            list_ = characterBirthdays(epochs, min_, max_, *extra)
            #query.classQ('Characters')
            #raise NotImplementedError
        elif s == 'Holidays':
            if extra is None:
                list_ = []
            else:
                for i in extra:
                    list_.extend(i.makeScenes(epochs, min_, max_))
        return self.variable.get(), extra, q, list_
class TimesEntry(Frame):
    def __init__(self, parent, label=None, set_=None):
        Frame.__init__(self, parent)
        self.initUI()
        if set is not None:
            self.set(set_)
        #if label is not None:
        #    Label(self, text=label).pack(side=LEFT)
    def initUI(self):
        self.time = TimeSelector(self)
        self.time.pack(side=LEFT)
        Label(self, text='   ').pack(side=LEFT)
        self.date = DateSelector(self)
        self.date.pack(side=LEFT)
        Label(self, text='  Epoch:').pack(side=LEFT)
        fkSelect = sys.modules['entry'].fkSelect
        self.epoch = fkSelect(self, utils.getClass('Epochs'), width=10)
        self.epoch.pack(side=LEFT)
        Label(self, text='   Scene:').pack(side=LEFT)
        self.scene = fkSelect(self, utils.getClass('Scenes'), on_update=self.pullscene)
        self.scene.pack(side=LEFT)
    def onRefresh(self):
        exit('timeDetails.refresh')
    def set(self, obj):
        #exit(obj.__class__)
        self.time.set(obj.date_time.time())
        self.date.set(obj.date_time.date())
        self.epoch.set(obj.epoch)
        self.scene.comp_set('time', obj)
    def get(self):
        kw = {}
        
        for key, val in self.time.get().iteritems():
            kw[key] = int(val)
        for key, val in self.date.get().iteritems():
            kw[key] = int(val)
        
        return utils.getClass('Times')(date_time=datetime(**kw), epoch=self.epoch.get())
    def pullscene(self, el):
        #exit('Dev exit - TimeDetails.TimesEntry.pullscene={}'.format(el.time))
        self.time.set(el.time.date_time)
        self.date.set(el.time.date_time)
        self.epoch.set(el.time.epoch)
class TimeSelector(Frame):
    list_ = ['hour','minute','second']
    separator = ':'
    def __init__(self, parent):
        """A class that generates fields to select a specific time.
        """
        Frame.__init__(self, parent)
        #Label(self, text='Begin:').pack(side=LEFT)
        #self.pack()
        self.entries = dict()
        self.makeEntry(self.list_[0])
        Label(self, text=self.separator).pack(side=LEFT)
        self.makeEntry(self.list_[1])
        Label(self, text=self.separator).pack(side=LEFT)
        self.third()
    def third(self):
        self.makeEntry(self.list_[2])
    def makeEntry(self, key, value='00', width=DEF_WIDTH):
        i = StringVar(self)
        i.set('00')
        e = Entry(self, textvariable=i, width=width)
        e.pack(side=LEFT)
        self.entries[key] = i
    def MakeEntryInsert(self, key, value='00', width=DEF_WIDTH):
        e = Entry(self, width=width)
        e.pack(side=LEFT)
        e.insert(0, value)
        self.entries[key] = e
    def get(self):
        kw = dict()
        for key, val in self.entries.iteritems():
            kw[key] = val.get()
        
        return kw
    def set(self, e):
        for L in self.list_:
            self.entries[L].set(getattr(e, L))
class DateSelector(TimeSelector):
    list_ = ['month','day','year']
    separator = '/'
    def third(self):
        self.makeEntry(self.list_[2], value='0001', width=4)
        b = Button(self, text='Calendar', command=self.calLaunch)
        b.pack(side=LEFT)
    def calLaunch(self):
        root = Toplevel()
        year = self.entries['year'].get()
        month = self.entries['month'].get()
        Calendar(root, return_=self.setFrameType, 
                 year=year, month=month)
    def setFrameType(self, *arg, **kw):
        for k in kw:
            val = kw[k]
            self.entries[k].delete(0, END)
            self.entries[k].insert(0, val)
        #printDict(kw)


        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        