Distinctive scar
Missing tooth
Missing finger
Bad breath
Strong body odor
Perfumed
Sweaty
Hands shake
Unusual eye color
Hacking cough
Sneezes and sniffles
Low voice
High voice
Slurs words
Lisps
Stutters
Enunciates clearly
Speaks loudly
Whispers
Hard of hearing
Tattoo
Birthmark
Unusual skin color
Bald
Long hair
Unusual hair color
limp
Distinctive Jewelery
Outlandish clothes
Underdressed
Overdressed
Nervous eye twitch
Fiddles and Fidgets
Whistles a lot
Sings a lot
Flips a coin
Good posture
bad posture
Tall
Short
Thin
Fat
Visible wounds or sores
Squints
Stares into the distance
Frequently chewing something
Dirty and unkempt
Clean
Distinctive Nose
Selfish
Obsequious
Drowsy
Bookish
Observant
Not very observant
Overly critical
Passionate artist
Passionate Hobbyist
Collector
Skinflint
Spendthrift
Pessimist
Optimist
Drunkard
Teetotaler
Well mannered
Rude
Jumpy
Foppish
Overbearing
Aloof
Proud
Individualist
Conformist
Hot tempered
Even tempered
Neurotic
Jealous
Brave
Cowardly
Careless
Curious
Truthful
Liar
Lazy
Energetic
Reverent or pious
Irreverent
Strongly opinionated
Moody
Cruel
Uses flowery speech
Uses the same phrases over and over
Prejudiced
Fascinated by magic
Distrustful of magic
Prefers members of one class
Jokester
No sense of humor
