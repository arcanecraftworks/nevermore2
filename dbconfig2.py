#===========================standard library imports==========================#
import os
from Tkinter import Frame, Listbox, Label, Entry
from Tkconstants import BOTH, END, LEFT, TOP, N, E, W, FALSE, SE, RIGHT
from ttk import Button
import tkFileDialog
#============================3rd party imports================================#
#================================local imports================================#
import scrollableFrame
import database
#import Nevermore.entry as entry
#from Nevermore.entry import optionField, checkField

from scrollableFrame import ScrolledListBox2
from database.db_dict import DBConfig, StringVarConfig
from database.utils import NT_OVERRIDE
from file_utils import write_config
from cli_tools import *
#==================================Constants==================================#
WINDOW_WIDTH = 725
WINDOW_HEIGHT = 500

DEF_POSTGRES_PORT = 5432
USE_DEFAULT_DBMS = True
DB_LIST_WIDTH = 40
DB_LIST_HEIGHT = 30
LISTBOX_PADDING = 10
DETAIL_ENTRY_WIDTH = 50
DETAIL_PADDING = 5
OPTION_WIDTH = 60
SQL_ENTRY_WIDTH = 50
DEF_OPT = 'Cloud/Local Database'
#=============================================================================#
DATABASE = 'Cloud/Local Database'
SQLITE = 'save file'
ZERO = 'zero'
#class GraphConfig():
#    
#    def __init__(self, *arg, **kw):
#        assert False
#        for i in DEF_SET:
#            self.__dict__[i] = StringVar()
#        self.set(*arg, **kw)
#    def set(self, dbconfig=None, **kw):
#        for key, val in kw.iteritems():
#            if val is 'user':
#                v = 'username'
#            else:
#                v = val
#            self.__dict__[key].set(v)
#    def get(self):
#        #printDictOfObject(self)
#        return Config(**self.__dict__)
#    def __repr__(self):
#        dbc = self.get()
#        return repr(dbc)
#class ListBoxFrame(Frame):
#    def __init__(self, parent, list_=[], command=None):
#        assert False
#        Frame.__init__(self, parent)
#        self.parent = parent
#        self.command = command
#        #===============================#
#        lb = Listbox(self)
#        lb.place(height=DB_LIST_HEIGHT, width=DB_LIST_WIDTH, x=0, y=0)
#        #lb = Listbox(self, )
#        for i in list_:
#            lb.insert(END, i)
#        lb.bind("<<ListboxSelect>>", self.setFrameType)
#        self.box = lb
#        self.box.pack()
#    def setFrameType(self, val):
#        sender = val.widget
#        idx = sender.curselection()
#        try:
#            value = sender.get(idx[0])
#            self.command(value)
#        except IndexError:
#            pass
class ConfigFrame(Frame):
    """Opens a window so the user can select the database that they wish to use.
    """
    def __init__(self, parent, eventhook, callback_obj):
        Frame.__init__(self, parent)
        self.parent = parent
        self.cfg = database.db_dict.StringVarConfig(self)
        self.eventhook = eventhook
        self.callback = callback_obj
        self.initUI()
        self.kwgs = {}
    def initUI(self):
        self.parent.title('Load Database')
        self.pack(fill=BOTH, expand=1)
        if os.name != 'posix' and not database.utils.NT_OVERRIDE:
            a = sys.modules['file_utils'].read_config()
            b = []
            for i in a:
                s = database.db_dict.DBConfig(**i)
                b.append(s)
            sl2 = scrollableFrame.ScrolledListBox2
            self.listbox = sl2(self, *b, callback=self.onListSelect)
        self.listbox.config(height=DB_LIST_HEIGHT, width=DB_LIST_WIDTH)
        #self.listbox.pack(side=LEFT, padx=LISTBOX_PADDING, 
        #                  pady=LISTBOX_PADDING)
        self.listbox.grid(row=0, column=0, rowspan=2, padx=LISTBOX_PADDING, 
                          pady=LISTBOX_PADDING)
        
        
        self.dataframe = DataFrame(self, self.cfg)
        self.dataframe.grid(row=0, column=1, sticky=N, padx=LISTBOX_PADDING, 
                            pady=LISTBOX_PADDING)
        centerWindow(self)
        self.buttons()
        self.parent.resizable(width=FALSE, height=FALSE)
    def buttons(self):
        self.buttonframe = ButtonFrame(self)
        self.buttonframe.grid(row=1, column=1, sticky=SE, 
                           padx=LISTBOX_PADDING, pady=LISTBOX_PADDING+5)
        
        self.buttonframe.makeButton('Go', self.onGo)
        self.buttonframe.makeButton('Save',self.onSave)
        self.buttonframe.makeButton('Save as autologin',self.onSaveAs)
        #self.buttonframe.makeButton('Delete',self.onDelete)
    def onListSelect(self, arg):
        """setFrameType for the listbox. It sets all of the values for the 
        strinval to the argument object
        """
        self.cfg.set(obj=arg)
        #printDebug(repr(arg))
        
        if self.__isArgDetailed__(arg):
            #printDebug('Kirito')
            self.dataframe.setFrameType(DATABASE)
        else:
            #printDebug('Asuna')
            self.dataframe.setFrameType(SQLITE)
            self.cfg.set(obj=arg)
    def isDetailed(self, arg=None):
        if arg is None:
            return self.__isFrameDetailed__()
        else:
            return self.__isArgDetailed__(arg)
    def __isArgDetailed__(self, arg):
        return not 'filename' in arg.__dict__
    def __isFrameDetailed__(self):
        s = self.dataframe.getConnType()
        if s == SQLITE or s == ZERO:
            return False
        elif s == 'Cloud/Local Database':
            return True
    def onSave(self, auto=False):
        dbconfig = self.cfg.build(self.isDetailed())
        
        auto_data = sys.modules['connector'].getAutoConfig()
        if auto_data is not None:
            write_config(auto_data, overwrite=False)
        if auto:
            write_config(dbconfig, auto=True)
        
        #write_config(auto_data)
        
        
        write_config(dbconfig, auto=auto)
        #write_config(dbconfig)
        self.listbox.add(dbconfig)
    def onGo(self):
        """Actions taken when the user presses the "Go" button.
        """
        #printDebug('eventhook={} callback={} cfg={}'.format(self.eventhook,
        #                                                    self.callback,
        #                                                    self.cfg))
        if self.dataframe.getConnType() == SQLITE:
            self.setFrameType.kw['filename'] = self.cfg.filename.get()
        else:
            self.callback.kw['master_config']=self.cfg.build().__dict__
        b = self.callback()
        if b:
            self.parent.destroy()
            return b
    def onSaveAs(self):
        self.onSave(auto=True)
    def onDelete(self):
        raise NotImplementedError

def optionField():
    return sys.modules['entry'].OptionField
def checkField():
    return sys.modules['entry'].CheckField      
class ButtonFrame(Frame):
    def __init__(self, parent, **kw):
        Frame.__init__(self, parent)
        self.parent = parent
        self.buttons = {}
        
    def makeButton(self, name, callback):
        """Builds a button with the given name and setFrameType
        """
        b = Button(self, text=name, command=callback)
        b.pack(side=RIGHT)
        self.buttons[name] = b
class DataFrame(Frame):   
    def opt(self):
        return {
                DATABASE:self.disp_db,
                SQLITE:self.disp_sqlite,
                ZERO:self.clear_info
                }
    db_options = ['postgres']
    def __init__(self, parent, master_config, *arg, **kw):
        
        Frame.__init__(self, parent, *arg, **kw)
        self.cfg = master_config
        self.sel = optionField()(self, self.opt().keys(),
                           command=self.setFrameType, width=OPTION_WIDTH,
                           default=DEF_OPT)
        self.sel.pack(side=TOP)
        self.setFrameType(DEF_OPT, default=True)
    def setFrameType(self, choice, default=False):
        o = self.opt()[choice](default)
        if o is not None:
            o.pack(side=TOP)   
    def disp_db(self, default):
        if 'details' not in self.__dict__:
            self.details = DetailFrame(self)
        if 'sqlite' in self.__dict__:
            self.sqlite.pack_forget()
        return self.details
    def disp_sqlite(self, default):
        if 'sqlite' not in self.__dict__:
            self.sqlite = SQLiteFrame(self, default)
        if 'details' in self.__dict__:
            self.details.pack_forget()
        return self.sqlite
    def clear_info(self, default):
        if 'sqlite' in self.__dict__:
            self.sqlite.pack_forget()
        if 'details' in self.__dict__:
            self.details.pack_forget()
        return None
    def getConnType(self):
        return self.sel.get()
class DetailFrame(Frame):
    fields = ['Alias','Host','Port','Schema','Username','Password']
    def __init__(self, parent, *arg, **kw):
        Frame.__init__(self, parent, *arg, **kw)
        self.initUI(parent.cfg)
    def initUI(self, cfg):
        self.efs = {}
        
        #row = 0
        row = 1
        for el in self.fields:
            self.efs[el.lower()] = self.entryField(el, row, cfg)
            row = row + 1
        self.setDefaultDBMS(cfg)
    def setDefaultDBMS(self, cfg):
        POSTGRES = 'PostgreSQL'
        lab = Label(self, text='DBMS'.format(POSTGRES))
        lab.grid(row=0, column=0, padx=DETAIL_PADDING,
                 pady=DETAIL_PADDING, sticky=E)
        lab2 = Label(self, text=POSTGRES)
        lab2.grid(row=0, column=2, padx=DETAIL_PADDING,
                  pady=DETAIL_PADDING, sticky=W)
        
        cfg.set(dbms=POSTGRES)
    def entryField(self, labeltext, row, cfg, obscure=False):
        lab = Label(self, text=labeltext)
        lab.grid(row=row, column=0, padx=DETAIL_PADDING, 
                 pady=DETAIL_PADDING, sticky=E)
        
        tv = getattr(cfg, labeltext.lower())
        txt = Entry(self, textvariable=tv, width=DETAIL_ENTRY_WIDTH)
        
        if labeltext=='Port' and USE_DEFAULT_DBMS and tv.get() == '':
            tv.set(str(DEF_POSTGRES_PORT))
        
        if labeltext=='Password' or obscure:
            txt.config(show='*')
        
        txt.grid(row=row, column=2)
        return txt
SQL_PADDING = 10
class SQLiteFrame(Frame):
    def __init__(self, parent, *arg, **kw):
        default = kw.pop('default',False)
        Frame.__init__(self, parent)
        self.parent = parent
        self.initUI(default)
    def initUI(self, default):
        tv = self.parent.cfg.filename
        #printDebug('{}={}'.format(tv.__class__, tv))
        open_button = Button(self, text='Open', command=self.onOpen)
        self.setGrid(open_button, 1, 0)
        new_button = Button(self, text='New', command=self.onNew)
        self.setGrid(new_button, 2, 0)
        tv = self.parent.cfg.filename
        tv.set('')
        #printDebug('{} {}'.format(tv.__class__, tv))
        self.textvariable = tv
        sqlite_entry = Entry(self, textvariable=tv, width=SQL_ENTRY_WIDTH)
        self.setGrid(sqlite_entry, 1, 1)   
    def setGrid(self, widget, row, column):
        widget.grid(row=row, column=column,
                               padx=SQL_PADDING, pady=SQL_PADDING)
    def onOpen(self):
        self.proc(tkFileDialog.askopenfilename())
    def onNew(self):
        self.proc(tkFileDialog.asksaveasfilename())
    def proc(self, fl):
        if fl != '':
            #self.filename = fl
            self.textvariable.set(fl)
def buildGeometry(width, height, offset_x, offset_y):
    return '{}x{}+{}+{}'.format(width, height, offset_x, offset_y)
def centerWindow(parent, w=WINDOW_WIDTH, h=WINDOW_HEIGHT):
    #w = DEF_WIDTH
    #h = DEF_HEIGHT
    sh = parent.parent.winfo_screenheight()
    sw = parent.parent.winfo_screenwidth()
    x = (sw - w)/2
    y = (sh - h)/2
    parent.parent.geometry(buildGeometry(w, h, x, y))

            
            
            
            
            
            
        
        
        