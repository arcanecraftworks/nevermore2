#===========================standard library imports==========================#
from Tkinter import PanedWindow, Label, Tk, SUNKEN, BOTH, VERTICAL

#============================3rd party imports================================#

#================================local imports================================#

#==================================Constants==================================#

#=============================================================================#
class WorkPane(PanedWindow):
    def init(self, parent, *args, **kw):
        #PanedWindow.__init__(parent, *args, **kw)
        self.parent = parent
        self.initUI()
    def initUI(self):
        m1 = PanedWindow()
        m1.pack(fill=BOTH, expand=1)
        
        left = Label(m1, text="left pane")
        m1.add(left)
        
        m2 = PanedWindow(m1, orient=VERTICAL)
        m1.add(m2)
        
        top = Label(m2, text="top pane")
        m2.add(top)
        
        bottom = Label(m2, text="bottom pane")
        m2.add(bottom)
        
        self.pack()
        
def label(parent, **kw):
    el = Label(parent, 
          background='gray',
          borderwidth=3,
          relief=SUNKEN,
          **kw)
    return el
if __name__ == '__main__':
    root = Tk()
    WorkPane(root, orient=VERTICAL).pack(fill=BOTH, expand=1)
    #panes = PanedWindow(root)
    #panes.pack(fill="both", expand="yes")
    #left = label(panes, text="Left")
    #left.pack()
    #right = label(panes, text="Right")
    #right.pack()
    
    #panes.add(left)
    #panes.add(right)
    
    root.mainloop()
