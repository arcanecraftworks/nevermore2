#===========================standard library imports==========================#
import os
import sys
from sets import Set
#============================3rd party imports================================#
#================================local imports================================#
#import file_utils as futils
import database
#import database.utils as utils
#import database.cli_utils as cli_utils
import nvm_types
from cli_tools import *

from connector import autoload, connect
from modselector import ModSelector
from Selector import Selector
#from entry import makeCaption as sep_caption
#cli_utils = getattr(database, 'cli_utils')
#utils = getattr(database, 'utils')
#database.cli_utils
#utils = database.utils
#from blockdiag.imagedraw.simplesvg import path
#==================================Constants==================================#
ASSOC_OVERRIDE = True
KEY_ERR_IGNORE = True
ECHO_ELEMENTS = False
auto_override = True
MACRO = True
USE_DEFAULT = False
DEFAULT_FILE = 'tcsmdb_test'
HEAT_END = False
CLIP_LIMIT=80
WRAP_TEXT = 80
#=============================================================================#
OLD_VERSION = 'old version: {}'
#DEF_COM = 'new character'
KEY_ERR = 'Key Error: {}. deferring to manual entry.'
WELCOME = 'Welcome to the Nevermore Command Line Interface!'
SUFFIX = '->'
DUNGEON = "Do you want to add Dungeonmasters' Mod?"
LOAD_MSG = "Cannot change directory to a file." + \
            "Please use the 'load' command to open a file."


FILE_DEFAULT = 'load {}.sql'.format(DEFAULT_FILE)
DECLARATIVE_META = "<class 'sqlalchemy.ext.declarative.api.DeclarativeMeta'>"        
EDIT_MSG = 'live edit not currently supported.' + \
            ' To make edits go through the view command'
NEW_VERSION = 'new version: '
MULTI_EDIT_PROMPT = 'There are {} entries in this selection. ' +\
        'Are you sure you want to override the {} value for all of them?'
INDIVIDUAL_MULTI_EDIT = False
def run_headless():
    print WELCOME
    a = autoload()
    if auto_override or a is None:
        a = browse()
    comm(a)
    
def comm(a):
    assert a is not None
    s = os.path.split(a)[1]
    while True:
        t = prompt_loop('{}{}'.format(s, SUFFIX),
                        'command not recognized',
                        tab=0,
                        f=comm2,
                        override=True,
                        )
        if t == 'exit':
            return
def browse():
    
    cdir = os.path.dirname(__file__)
    while True:
        if USE_DEFAULT:
            s = FILE_DEFAULT
        else:
            p = "{}{}".format(cdir, SUFFIX)
            s = raw_input(p)
        arg = s.split(' ')
        com = arg[0]
        if com == 'load' and arg[1] != '-n':
            b = manual_load(cdir, arg[1])
            #printDebug('b={}'.format(b))
            if b is not None:
                return b
        elif com == 'load' and arg[1] == '-n':
            b = new_load(cdir, arg[2], override=True)
            if b is not None:
                return b
        elif com == 'cd':
            a = absTest(cdir, arg[1])
            if os.path.exists(a) and not os.path.isfile(a):
                cdir = absTest(cdir, arg[1])
            else:
                print LOAD_MSG
        else:
            printDebug('command not recognized', override=True)
    exit('browse')
def comm2(arg):
    a = arg.split(' ')
    pc = a[0]
    if pc == 'exit':
        return 'exit'
    f = '__{}__(a[1:])'.format(pc)
    exec(f)
    try:
        pass
        #exec(f)
    except:
        printDebug('Command not recognized. try either "new x" or "view x"',
                   override=True)
    
def __new__(a):
    n = newMenu(a)
    assert n is not None
        
    session = database.utils.open_session()
    session.add(n)
    session.commit()
        
    if HEAT_END:
        exit('heat end!')
def __check_column__(a):
    printDebug('check column={}'.format(a))
def __manual_filter__(tablename):
    printDebug(tablename)
    col = prompt_loop('Column: ', 'column not found.', __check_column__)
    val = prompt_loop_null('Value:')
    raise NotImplementedError
def __view__(a):
    try:
        nTable = __findNTable__(a[0])
    except IOError:
        printDebug('table "{}" not found.'.format(a[0]))
        return
    
    
    if len(a[1:]) > 0:
        print 'command-level filters not currently supported.'
    
    v = HeadlessViewer(nTable)
    v.mainloop()
class HeadlessViewer(Selector):
    ordinal_menu = ['filter',
                    'reset last filter',
                    'reset all filters',
                    'display details of first',
                    'display specific column',
                    'bulk edit',
                    'delete',
                    'exit']
    def __init__(self, nTable, **kw):
        self.nTable = nTable
        self.headers = database.utils.parseColumns(nTable)
        self.kw = kw
        self.filters = []
        self.tab=0
    def menu_dicts(self):
        self.printq_options = {
                               'filter':self.__filter__,
                               'reset last filter':self.__reset_one__,
                               'reset all filters':self.resetQuery,
                               'display details of first':self.details,
                               'display specific column':self.spec_column,
                               'bulk edit':self.__edit_column,
                               'delete':self.__delete,
                               'exit':self.exit
                               }
        self.no_row_options = self.printq_options
        self.detail_menu = {
                            'edit column':self.__edit_column,
                            'go back':self.__go_back
                            }
    def narrow_query(self, col):
        at = self.get_attrib(col)
        mClass = self.nTable.mClass
        sess = database.utils.open_session()
        return eval('sess.query(mClass.{})'.format(at))
    def spec_column(self):
        prompt = 'column->'
        col = prompt_automatic(prompt)
        query = self.narrow_query(col)
        o = database.cli_utils.PrintQueryObject(query, obj_mode=False, 
                                       nTable=self.nTable, column=col)
        r = o.print_query()
        if r == []:
            print 'Nothing to show'
        junk = str(raw_input('proceed?'))
    #TODO: start here
    def __delete(self):
    
        a = self.q.all()
        s = 'Are you sure you want to permanently delete {} entries?'
        if yn_prompt_loop(s.format(len(a))):
            sess = database.utils.open_session()
            for el in a:
                sess.delete(el)
        sess.commit()
        self.__reset_one__()
    def __go_back(self):
        pass
    def getFirstOnly(self):
        if 'first_only' in self.__dict__:
            return self.first_only
        else:
            return False
    def __edit_column(self):
        
        prompt = 'column->'
        s = prompt_loop_null(prompt)
        col = self.__find_attr(s, self.headers)        
        if col is None:
            printDebug('column not found!', override=True)
        
        if self.getFirstOnly():
            self.edit_one(col)
        else:
            self.multi_edit(col, s)
        database.utils.open_session().commit()
    def edit_one(self, col):
        el = self.q.first()
        at = self.proc(el, col)
        print 'old version: {}'.format(at)
        s = self.new_version()
        exec('el.{} = s'.format(col))
    def old_version(self, at):
        print OLD_VERSION.format(at)
    def new_version(self):
        return str(raw_input(NEW_VERSION))
    def multi_edit(self, col, header):
        a = self.q.all()
        #TODO: afjkld
        ans = yn_prompt_loop(MULTI_EDIT_PROMPT.format(len(a), header))
        if ans and not INDIVIDUAL_MULTI_EDIT:
            val = self.new_version()
            for el in a:
                exec('el.{} = val'.format(col))
        elif ans and INDIVIDUAL_MULTI_EDIT:
            #val = self.new_version()
            for el in a:
                at = self.proc(el, col)
                self.old_version(at)    
                val = self.new_version()
                exec('el.{} = val'.format(col))
    def proc(self, el, col):
        return eval('el.{}'.format(col))
    def __find_attr(self, s, headers):
        for attr, header in self.headers:
            if s.capitalize() == header:
                return attr
    def exit(self, *arg, **kw):
        return 'exit'

    def mainloop(self):
        self.resetQuery()
        self.querySelect()
    def resetQuery(self):
        Selector.resetQuery(self, remote=self.nTable.mClass)
        for i in self.kw:
            pair = [i, self.kw[i]]
            self.q, junk = database.cli_utils.filter_ver(None, query=self.q, pair=pair)
    def clipAttr(self, attr):
        if len(str(attr)) < CLIP_LIMIT:
            return attr
        else:
            printDebug(attr)
            exit()
    def details(self):
        self.first_only = True
        
        el = self.q.first()
        self.headers
        for attr, header in self.headers:
            self.print_wrapped('{}: {}'.format(header, self.searchVal(el, attr)))
        menu_exec(self.detail_menu)
        
        self.first_only = False
    def searchVal(self, el, attr):
        return database.utils.searchVal(self.nTable, el, attr)
    def print_wrapped(self, s):
        """Clipping each element so that the text wraps into WRAP_TEXT characters
           so that the console automation doesn't. 
        """
        line_index = 0
        s_1 = ''
        split = s.split()
        
        for i in split:
            len_i = len(i)
            if (line_index + len_i) >= WRAP_TEXT:
                s_1 = '{}\n{}'.format(s_1, i)
                line_index = 0
            else:
                s_1 = '{} {}'.format(s_1, i)
                #s_1 = s_1 + i + ' '
            line_index = line_index + len_i
        #assert False
        print s_1.lstrip()
    def get_attrib(self, col):
        for el, header in self.headers:
            if col.lower() == header.lower():
                return el
def __edit__(a):
    
    printDebug(EDIT_MSG)

class CommandMenu():
    def __init__(self, prompt, process, options_dict):
        self.prompt = prompt
        self.process = process
        self.options = options_dict
    def loop(self):
        pass

def __menu__(a):
    print 'menu feature not yet implemented'
def __findNTable__(tab):
    for i in frontMenu():
        comp0 = tab.capitalize()
        comp1 = i.mClass.name_singular.capitalize()
        comp2 = i.mClass.__tablename__
        if comp0 == comp1 or comp0 == comp2:
            return i
    raise IOError('{} not found.'.format(tab))
def newMenu(args):
    #printDebug('newMenu={}'.format(args), interrupt_override=True)
    fm = frontMenu()
    if len(args) == 0:
        printDebug(menu(fm, default_value=0))
    else:
        table = __findNTable__(args[0])
        return newEntry(table, args=args[1:])
def newEntry(nTable=None, args=None):
    printDebug('new Entry={}'.format(nTable), interrupt_override=True)
    
    try:
        return HeadlessEntry(nTable, **parseArgs(args)).write()
    except TypeError:
        raise
def parseArgs(args):
    kw = dict()
    
    for i in args:
        try:
            first, second = i.split('=')
        except ValueError:
            #raise
            printDebug('Filtering on columns with spaces in the name are not supported.\n'+
                       'Try using an underscore (_) instead.')
        j = i.split('=')
        printDebug(j)
        first = j[0].replace('_',' ')
        second = j[1].replace('_',' ')
        printDebug(first.capwords())
        kw[first] = second
    return kw
class HeadlessEntry():
    def __init__(self, nTable=None, mapped_object=None, tab=0, first=None, **kw):
        nTable, mClass = self.__setNTable__(nTable)
        self.mapped_object = mapped_object
        self.first = first
        if 'rels' in mClass.__dict__:
            rels = mClass.rels
        else:
            rels = dict()
        self.tab = tab
        self.__set_keywords(nTable, rels, **kw)    
    def __setNTable__(self, nTable):
        #printDebug('tab={}'.format(tab))
        if nTable.__class__ != database.utils.nvmTable:
            raise TypeError('nvmTable object is required.')
        class_ = nTable.__class__
        if class_ == database.utils.nvmTable:
            self.nTable = nTable
            mClass = self.nTable.mClass
        elif str(class_) == DECLARATIVE_META:
            mClass = class_
            self.nTable = database.utils.getNTable(class_=class_)
            #self.nTable = utils.nTables(mClass.__tablename__)
        return nTable, mClass
    
    def __set_keywords(self, nTable, rels, *args, **kw):
        self.assoc = dict()
        self.kw = dict()
        #mClass = nTable.mClass
        for i in kw:
            self.keyErrCheck(nTable, i, rels, *args, **kw)
    def keyErrCheck(self, nTable, i, rels, *args, **kw):
        try:
            return self.__set_keywords2(nTable,
                                        i, rels, 
                                        *args, **kw)
        except KeyError:
            if KEY_ERR_IGNORE or EXCEPT_OVERRIDE:
                raise
            else:
                printDebug(KEY_ERR.format(i), override=True, 
                                   interrupt_override=True)
    
    def __set_keywords2(self, nTable, i, rels, *args, **kw):
        #self.assoc = dict()
        #self.kw = dict()
        mClass = nTable.mClass
        #for i in kw:
        if i in rels:
            self.assoc[i] = kw[i]
            return
        elif 'typeMap' in mClass.__dict__:
            type_ = mClass.typeMap()[i]
        else:
            type_ = nvm_types.convert(nTable)[0]['type']
            #printDebug(type_)[0]['type']
            #exit()
        verify = getattr(nvm_types, type_)().verify
        self.kw[i] = verify(kw[i])
    def write(self):
        """A prompt for writing a row in the given table
        """
        d = nvm_types.convert(self.nTable)
        
        #Collecting elements
        for g in d:
            i = g['key']
            #printDebug(i)
            self.__write_one__(i, g)
        kw2 = dict()
        for i, di in self.kw.iteritems():
            if ECHO_ELEMENTS:
                printDebug('{}: {}={}'.format(i, di.__class__, di), 
                           interrupt_override=True)
            if di is not None:
                kw2[i] = di
        try:
            class_ = self.nTable.mClass
            #printDebug(class_)
            #printDebug(kw2)
            o = class_(**kw2)
            p = self.__assoc(o)
            return p
        except TypeError:
            if EXCEPT_OVERRIDE:
                raise
            else:
                exit('write - TypeError')
    
    def __write_one__(self, i, d):
        #assert i != 'summary'
        a = False
        
        di = d
        if not 'prompt' in di:
            di['prompt'] = '{}:'.format(di['key'].capitalize())
        if 'caption' in di:
            di['prompt'] = di['caption']
        del di['key']
        
        str_type = di.__class__ == ''.__class__
        list_type = di.__class__ == dict().__class__
        if str_type and not i in self.kw:
            #if a:
            #    printDebug('Kirito')
            o = getattr(nvm_types, di)()
            printDebug('write one')
            if self.first is not None:
                printDebug('Kirito')
                self.kw[i] = self.first
                printTabbedOverride(makeCaption(i), tab=self.tab)
                self.first = None
            else:
                self.kw[i] = o.prompt(makeCaption(i), tab=self.tab)
        elif list_type and not i in self.kw:
            #if a:
            #    printDebug('Suguha')
            self.__list_type__(i, di)
        elif not i in self.kw:
            if a:
                printDebug('Asuna')
            self.kw[i] = getattr(nvm_types, di).prompt(tab=self.tab)
    #def __promcapcheck__(self, di):
    #    a = (not 'prompt' in di) or di['prompt'] is None
    #    b = (not 'caption' in di) or di['caption'] is None
    #    return  not(a and b)
    def __list_type__(self, i, di):
        type_ = di['type']
        try:
            o = getattr(nvm_types, type_)
            p = o(self.nTable)
            if '_' in di['prompt']:
                di['prompt'] = makeCaption(di['prompt'], colon=False)
            a = p.prompt(tab=self.tab, **di)
            
        except AttributeError:
            if EXCEPT_OVERRIDE:
                raise
            else:
                exit('nvm_types does not have a(n) "{}" class'.format(type_))
        except TypeError:
            if EXCEPT_OVERRIDE:
                raise
            else:
                exit('error on type {}'.format(o))
        except AssertionError:
            raise
            printDebug('Di=')
            for i, j in di.iteritems():
                printDebug('{}: {}'.format(i, j))
            exit('list type AssertionError')
        self.__get_prompt__(i, di)
            
        if a.__class__ == [].__class__ and a[0] is not None:
            #printDebug('{}=Asuna'.format(i))
            self.assoc[i] = a
        else:
            self.kw[i] = a
    def __get_prompt__(self, i, di):
        if not 'prompt' in di or di['prompt'] is None:
            di['prompt'] = makeCaption(i, di)
    def __assoc(self, a):
        s = str(a.__class__)
        #printDebug(self.assoc)
        #if 'rels' in getMClass(a).__dict__:
        #    printDebug('rels={}'.format(a))
        
        for i, ai in self.assoc.iteritems():
            assert ai is not None
            
            if 'rels' in getMClass(a).__dict__:
                if ai.__class__ == list:
                    for el in ai:
                        #printDebug('utils.assoc({}, {}, {})'.format(a, i, el))
                        database.utils.assoc(a, i, el)
                else:
                    database.utils.assoc(a, i, ai)
                
                
                
                #utils.assoc(a, i, ai)
            #elif ASSOC_OVERRIDE:
            #    printDebug('Asuna')
            #    printDebug('association overriden - {}.{}'.format(a, i))
            #    utils.assoc(a, i, ai)
            else:
                raise AttributeError("No attribute 'rels' in {}".format(a.__class__))
                #printDebug('{}.{} = {}'.format(a, i, ai))
                #exec('a.{} = ai'.format(i))
        return a
def getMClass(a):
    return a._sa_instance_state.class_
def makeCaption(root_key, opt_dict=None, colon=None):
    mc = sys.modules['entry'].makeCaption
    if opt_dict is None:
        return mc(root_key, colon=colon)
    elif 'prompt' in opt_dict:
        return opt_dict['prompt']
    elif 'caption' in opt_dict:
        return opt_dict['caption']
    else:
        return mc(root_key, colon=colon)
def frontMenu():
    a = []
    assert len(database.utils.nTables) != 0
    for i in database.utils.nTables:
        
        nTable = database.utils.nTables[i]
        mClass = nTable.mClass
        if mClass.__tabletype__ == 'front':
            a.append(nTable)
    return a
def new_load(cdir, filepath, new_path=None, override=False):
    if new_path is None:
        path = filepath
    else:
        path = new_path
    
    if override:
        s = connect(filename=path)
        printDebug('s={}'.format(s))
    elif os.path.isfile(path):
        prompt = 'File already exists. Overwrite?'
    elif not os.path.isfile(path):
        prompt = 'No such file found. Create new?'
    
    if yn_prompt_loop(prompt, default_value='Y'):
        t = loadAsk(filename=path)
        printDebug('t={}'.format(t))
    else:
        return None
def manual_load(cdir, filepath):
    try:
        filename = absTest(cdir, filepath, allow_false=True)
    except:
        print 'File not found!'
        return
        
    assert filename is not None    
    if os.path.isfile(filename) and connect(filename=filename): 
        try:
            database.utils.initTables()
            return filename
        except AssertionError, SyntaxError:
            raise
        except SystemExit:
            raise
        except:
            exit('No database found on file {}'.format(filename))
    else:
        nL = new_load(cdir, filepath, new_path=filename)
        if nL is not None:
            database.utils.initTables()
            return filename
def p(el):
    printDebug(el.maps_file)
def loadAsk(*arg, **kw):
    d_o = yn_prompt_loop(DUNGEON, default_value='n')
    if connect(*arg, **kw):
        ModSelector.loadMods(dungeon_override=d_o)
    return kw['filepath']
def absTest(cdir, path, allow_false=False):
    if os.path.isabs(path):
        return path
    else:
        return mergeDir(cdir, path, allow_false=allow_false)
def mergeDir(origDir, arg, allow_false=False):
    
    argDir = arg.split(os.sep)
    imDir = []
    tripoff = False
    for i in argDir:
        if i =='..' and (not tripoff):
            origDir = os.path.split(origDir)[0]
        else:
            tripoff = True
            imDir.append(i)
        
    newDir = os.path.join(origDir, *imDir)
    
    
    if os.path.exists(newDir):
        return newDir
    elif os.path.isdir(os.path.dirname(newDir)) and allow_false:
        return newDir
    else:
        raise IOError    