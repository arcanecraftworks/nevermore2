#===========================standard library imports==========================#
from Tkinter import Frame, LEFT
from ttk import Button
#============================3rd party imports================================#
#================================local imports================================#
from DBConfig import SQLiteFrame
#from database.utils import migrate_all
from stub import stub
from MessageBox import onInfo
#==================================Constants==================================#
MACRO = False

class MigFrame(Frame):
    def __init__(self, parent, *arg, **kw):
        Frame.__init__(self, parent, *arg, **kw)
        self.parent = parent
        self.initUI()
        self.pack()
    def initUI(self):
        ms = SQLiteFrame(self, open=False)
        ms.pack(side=LEFT)
        self.ms = ms
        go = Button(self, text="Migrate", command=self.migrate)
        go.pack(side=LEFT, padx=10)
        #if MACRO:
        #    self.migrate()
            #exit('initUI')
    def migrate(self):
        assert False
        dest = self.ms.sqlite_entry.get()
        if dest == '':
            dest = ':memory:'
        onInfo('Migrating all data to {}'.format(dest))
#        migrate_all(destination=dest)
    def clearData(self):
        s = self.ms.sqlite_entry.get()
        #print s
        