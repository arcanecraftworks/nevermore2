#import sys
#import math
#import main
#sys.path.append('C:\\Python27\\Nevermore2\\SQLAlchemy-1.0.0\\lib')
#sys.path.append('C:\\Python27\\Lib\\site-packages')
#print sys.path
#from sqlalchemy import Sequence, Column, Integer, String, ForeignKey
#from sqlalchemy.orm import relationship

#import main
#import database
#import dbmaps
#Base = dbmaps.Base
# Table 5-2c Power Center Alignment

#dd35_officeholders = {60: 'warrior', 80: 'second-highest-level fighter', 100: 'highest-level-fighter'}
#dd35_isolated_mix = {96: 'human',2: 'halfling', 1: 'elf'}
#dd35_mixed = {79: 'human', 9: 'halfling', 5:'elf', 3: 'dwarf', 2: 'gnome', 1: 'half-elf', 1: 'half-orc'}
#dd35_integrated_mix = {37: 'human', 20: 'halfling', 18: 'elf', 10: 'dwarf', 7:'gnome', 5: 'half-elf', 3: 'half-orc'}
#dd35_traits = open('dd35_descripts.txt', 'r').read().split('\n')

#class poweralign(Base):
#    __tablename__ = 'dd35_poweralign'
#    id = Column(Integer, Sequence('poweralign_seq'), primary_key=True)
#    dCent_upper = Column(Integer)
#    alignment_id = Column(Integer, ForeignKey('Alignments.id'))
#    alignment = relationship('Alignment')
# Table 5-2c Power Center Type
#class powercenter(Base):
#    __tablename__ = 'dd35_powercenters'
#    id = Column(Integer, Sequence('pc_seq'), primary_key=True)
#    d20_upper = Column(Integer)
#    type = Column(String(50))
#    def __repr__(self):
#        return type
#    @staticmethod
#    def build():
#        session = database.open_session()
#        conventional = powercenter(d20_upper=13, type="Conventional")
#        nonstandard = powercenter(d20_upper=18, type="Nonstandard")
#        magical = powercenter(d20_upper=27, type="Magical")
#        together = [conventional, nonstandard, magical]
#        session.add_all(together)
#        session.commit()
#        session.close()
# Tables 5-2 Random Town Generation && Power Centers  
#class cityinfo(Base):
#    __tablename__ = 'dd35_cityinfo'
#    id = Column(Integer, Sequence('dd35_5_2_seq'), primary_key=True)
#    d_upper = Column(Integer)
#    town_size = Column(String(20))
#    pop_upper = Column(Integer)
#    gp_limit = Column(Integer)
#    power_mod = Column(Integer)
#    level_mod = Column(Integer)
#    def __repr__(self):
#        return self.town_size
#    @staticmethod
#    def build():
#        session = database.open_session()
#        thorp = cityinfo(d_upper=10, town_size='Thorp', pop_upper=80, gp_limit=40, power_mod=-1, level_mod=-3)
#        hamlet = cityinfo(d_upper=30, town_size='Hamlet', pop_upper=400, gp_limit=100, power_mod=0, level_mod=-2)
#        village = cityinfo(d_upper=50, town_size='Village', pop_upper=900, gp_limit=200, power_mod=1, level_mod=-1)
#        small_town = cityinfo(d_upper=70, town_size='Small Town', pop_upper=2000, gp_limit=800, power_mod=2, level_mod=0)
#        large_town = cityinfo(d_upper=85, town_size='Large Town', pop_upper=5000, gp_limit=3000, power_mod=3, level_mod=3)
#        small_city = cityinfo(d_upper=95, town_size='Small City', pop_upper=12000, gp_limit=15000, power_mod=4, level_mod=6)
#        large_city = cityinfo(d_upper=99, town_size='Large City', pop_upper=25000, gp_limit=40000, power_mod=5, level_mod=9)
#        metropolis = cityinfo(d_upper=100, town_size='Metropolis', pop_upper=100000, gp_limit=100000, power_mod=6, level_mod=12)
#        entries = [thorp, hamlet, village, small_town, large_town, small_city, large_city, metropolis]
#        session.add_all(entries)
#        session.commit()
#        session.close()
#class dd35_class(Base):
#    __tablename__ = 'dd35_classes'
#    id = Column(Integer, Sequence('class_seq'), primary_key=True)
#    name = Column(String(50))
#    pc_class = Column(Integer)
#    town_char_level_roll = Column(String(50))
#    def __repr__(self):
#        return self.name
#    @staticmethod
#    def build():
#        session = database.open_session()
#        adept = dd35_class(name='Adept', pc_class=0, town_char_level_roll= '1d6')
#        aristocrat = dd35_class(name='Aristocrat', pc_class=0, town_char_level_roll='1d4')
#        barbarian = dd35_class(name='Barbarian', pc_class=1, town_char_level_roll='1d4')
#        bard = dd35_class(name='Bard', pc_class=1, town_char_level_roll='1d6')
#        cleric = dd35_class(name='Cleric', pc_class=1, town_char_level_roll='1d6')
#        commoner = dd35_class(name='Commoner', pc_class=0, town_char_level_roll='4d4')
#        druid = dd35_class(name='Druid', pc_class=1, town_char_level_roll='1d6')
#        expert = dd35_class(name='Expert', pc_class=0, town_char_level_roll='3d4')
#        fighter = dd35_class(name='Fighter', pc_class=1, town_char_level_roll='1d8')
#        monk = dd35_class(name='Monk', pc_class=1, town_char_level_roll='1d4')
#        paladin = dd35_class(name='Paladin', pc_class=1, town_char_level_roll='1d3')
#        ranger = dd35_class(name='Ranger', pc_class=1, town_char_level_roll='1d3')
#        rogue = dd35_class(name='Rogue', pc_class=1, town_char_level_roll='1d8')
#        sorcerer = dd35_class(name='Sorcerer', pc_class=1, town_char_level_roll='1d4')
#        warrior = dd35_class(name='Warrior', pc_class=1, town_char_level_roll='2d4')
#        wizard = dd35_class(name='Wizard', pc_class=1, town_char_level_roll='1d4')
#        classes = [adept, aristocrat, barbarian, bard, cleric, commoner, druid, expert, fighter, monk, paladin, ranger, rogue, sorcerer, warrior, wizard]
#        session.add_all(classes)
#        session.commit()
#        session.close()
#def build_races():
#    session = database.open_session()
#    human = dbmaps.Species(name='Human', name_plural='Humans', size_class='Medium')
#    dwarf = dbmaps.Species(name='Dwarf', name_plural='Dwarves', size_class='Small')
#    elves = dbmaps.Species(name='Elf', name_plural='Elves', size_class='Medium')
#    gnomes = dbmaps.Species(name='Gnome', name_plural='Gnomes', size_class='Small')
#    half_elves = dbmaps.Species(name='Half-Elf', name_plural='Half-Elves', size_class='Small')
#    half_orcs = dbmaps.Species(name='Half-Orc', name_plural='Half-Orcs', size_class='Large')
#    halflings = dbmaps.Species(name='Halfling', name_plural='Halflings', size_class='Small')
#    spex = [human, dwarf, elves, gnomes, half_elves, half_orcs, halflings]
#    session.add_all(spex)
#    session.commit()
#    session.close()
#def dpopulate():
#    cityinfo.build()   
#    dd35_class.build()
#    #dd35_poweralign.build()
#    powercenter.build()
#def town_generate():
#    print("Town Generator protocol.")
#    #roll for town size
#    d = int(raw_input("d%-> "))
#    session = database.open_session()
#    #run query to cityinfo table
#    q = session.query(cityinfo.id, cityinfo.town_size, cityinfo.d_upper, cityinfo.pop_upper)
#    #print results
#    rows = database.print_query(q)
#    #loop over values, find the upper and lower entries
#    cur = 0
#    prev = 0
#    for i in range(len(rows)):
#        row = rows[i]
#        if row['d_upper'] <= d:
#            cur = i
#            prev = i-1
#    
#    if prev < 0:
#        prev = 0
#        pop_lower = 1
#    else:
#        prev_dict = rows[prev]
#        pop_lower = prev_dict['pop_upper']
#    
#    cur_dict = rows[cur]
#    print 'town size: ' + cur_dict['town_size']
#    
#    #roll d% again to determine exact population
#    e = int(raw_input("d%-> "))
#    pop_upper = cur_dict['pop_upper']
#    diff = pop_upper - pop_lower
#    ratio = diff / 100.0
#    pop = math.ceil(pop_lower + (ratio * e))
#    print 'population: ' + str(int(pop))
